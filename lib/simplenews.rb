class SimpleNews < Middleman::Extension
  def initialize(app, options_hash={}, &block)
    super
  end

  helpers do

    ## Parse Markdown <dl>-style news file
    def simplenews_data path = "/all-news.html"
      current_date = ''
      news_hash = {}

      # Load news data
      allnews = sitemap.find_resource_by_path(path).render({layout:false})

      # Demote H1s to H4s
      allnews.gsub!(/(<\/?)h1/,'\\1h4')

      # Parse all news items
      items = Nokogiri(allnews).css('dl').children

      items.each do |item|

        if item.node_name == 'dt'
          current_date = Date.parse item.content

        elsif item.node_name == 'dd'
          news_hash[current_date] ||= ""

          current_title = item.css('h1')

          unless current_title.empty?
            puts current_title.inner_html
          end

          news_hash[current_date] += item.inner_html
        end

      end

      return news_hash.sort_by {|date, content| date}.reverse
    end


    ## Render simplenews
    def simplenews options = {}
      options[:path] ||= "/all-news.html"
      options[:skip] ||= 0
      options[:limit] ||= 999999

      news_html = ""

      previous_date = Time.parse('1970-1-1')

      news = simplenews_data options[:path]

      news.drop(options[:skip]).take(options[:limit]).each do |date, content|
        new_year = (previous_date.year != date.year)

        date_string = "<span class='year-#{new_year ? 'new': 'same'}'>%Y</span> %b %d"

        news_html += "
          <div class='timeline-day year-#{new_year ? 'new' : 'same'}'>
          <h3 class='timeline-date'>#{date.strftime(date_string)}</h3>
          <div class='timeline-day-content'>
          #{content}
          </div>
          </div>
        "

        previous_date = date
      end

      news_html
    end

  end
end

::Middleman::Extensions.register(:simplenews, SimpleNews)
