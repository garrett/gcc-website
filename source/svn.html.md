# GCC: Anonymous read-only SVN access

Our SVN source repository is available read-only to the public at large. That way you can pick up any version (including releases) of GCC that is in our repository.

In addition you can [browse our SVN history online](https://gcc.gnu.org/viewcvs/gcc/).

(Our [web pages are managed via CVS](about.html#cvs).)

## Using the SVN repository

Assuming you have version 1.0.0 and higher of [Subversion](http://subversion.tigris.org/) installed, you can check out the GCC sources using the following command:

> `svn checkout svn://gcc.gnu.org/svn/gcc/trunk SomeLocalDir`

If you are behind a firewall that does not allow the svn protocol through, you can replace `svn://` with `http://`. You may also need to modify your subversion servers file (\~/.subversion/servers) to set `http-proxy-host` and `http-proxy-port`. You should only use the http protocol if the svn protocol does not work; the http protocol has a higher server overhead associated with it and will be slower.

There is more [information about Subversion specifically for GCC](https://gcc.gnu.org/wiki/SvnHelp) in the GCC Wiki.

### Generated files {#generated_files}

Our source tree contains a number of files that are generated from other source files by build tools such as Bison, Autoconf, and Gperf. Bison is now required when using SVN to access our sources, but all other generated files are included in the source tree so that GCC can be built without these build tools. The SVN checkout and update operations do not insure that the timestamps of generated files are later than those of the files they are generated from. The script `contrib/gcc_update` updates the timestamps for all these generated files. See the comments in that script for instructions on running it.

GCC's build system (in particular Make) uses file timestamps to determine if a generated file needs to be updated by running a particular build tool. Because of this, GCC's build system may believe that a generated file needs regenerating even though its source has not changed, and require a particular build tool to rebuild that generated file. If the appropriate build tool is installed on your system, then this will not be a problem. If you do not intend to make changes to the source, you can avoid installing these build tools by running `contrib/gcc_update`.

There has been some discussion of removing these generated files from GCC's SVN source tree (there is no discussion of removing them from the released source tarballs). If that happens then building GCC from the SVN source tree would require installing the above mentioned build tools. Installing these build tools is not particularly difficult, but can be time consuming especially if you only occasionally install GCC on a particular system.

The build tools that GCC uses are all available from the GNU Project (see <http://www.gnu.org>), are often already available on many systems, and can often be found already built for some systems. A partial list of these build tools is: Autoconf, Bison, Xgettext, Automake, and Gperf.

### Conflicts when using `svn update`

It is not uncommon to get svn conflict messages for some generated files when updating your local sources from the SVN repository. Typically such conflicts occur with autoconf generated files.

As long as you haven't been making modifications to the generated files or the generator files, it is safe to delete the offending file, then run `svn update` again to get a new copy.

## Branches and Tags {#tags}

A branch called *branchname* can be checked out with the following command:

> `svn co svn://gcc.gnu.org/svn/gcc/branches/branchname gcc`

(The release branch of the latest version of GCC *x*.*y* is named `gcc-x_y-branch`.)

Similarly a tag called *tagname* can be checked out with the following command:

> `svn co svn://gcc.gnu.org/svn/gcc/tags/tagname gcc`

(For recent releases, the SVN tag for GCC *X*.*Y*.*Z* is of the form `gcc_X_Y_Z_release`.)

The following list provides some representative examples:

-   gcc-4\_3-branch
-   gcc\_4\_3\_4\_release
-   gcc\_4\_3\_3\_release
-   gcc\_4\_3\_2\_release
-   gcc\_4\_3\_1\_release
-   gcc\_4\_3\_0\_release
-   gcc-3\_4-branch
-   gcc\_3\_4\_3\_release
-   gcc\_3\_4\_2\_release
-   gcc\_3\_4\_1\_release
-   gcc\_3\_4\_0\_release

To get a list of available branches, use the command:

> `svn ls svn://gcc.gnu.org/svn/gcc/branches`

To find out the revision/date at which a branch or tag was created, use the command `svn log --stop-on-copy`.

### Active Development Branches {#devbranches}

#### General Infrastructure

struct-reorg-branch
:   This branch is for the development of structure reorganization optimizations, including field reordering, structure splitting for trees. These optimizations are profile information driven. This is a subbranch of tree-profiling. This branch is being maintained by Caroline Tice, Dale Johannesen, Kenneth Zadeck, Stuart Hastings, Mostafa Hagog.
autovect-branch
:   This branch is the successor to the lno-branch. The purpose of this branch is tree-level [autovectorization](projects/tree-ssa/vectorization.html) work, and related work that the autovectorizer could use or benefit from (like data-dependence analysis, [loop nest optimizations](projects/tree-ssa/lno.html)).
[graphite-branch](https://gcc.gnu.org/wiki/Graphite)
:   The purpose of this branch is to develop an infrastructure for loop transforms using the polyhedral model.
[lw-ipo](https://gcc.gnu.org/wiki/LightweightIpo)
:   This branch aims to implement lightweight IPO. Patches and discussion on this branch should be marked with the tag `[lipo]` in the subject line. The branch is maintained by [David Li](mailto:davidxl@google.com).
gimple-front-end
:   This branch implements a front end for GIMPLE. It is maintained by [Diego Novillo](mailto:dnovillo@google.com). Patches should be prefixed with `[gimplefe]` in the subject line. See the [GIMPLE Front End](https://gcc.gnu.org/wiki/GimpleFrontEnd) page for details.
gomp-4\_0-branch
:   The purpose of this branch is to update the OpenMP support to version 4.0. This branch is mantained by Jakub Jelinek. Patches should be marked with the tag `[gomp4]` in the subject line.
ra-improvements
:   This branch aims to implement several improvements to the current register allocator. Examples include implementing a lower-triangular conflict matrix and register coalescing. It is hoped that these improvements will not only help the current allocator, but will be useful to the other register allocation projects such as RABLE and YARA. This branch will be merged with the dataflow-branch from time to time. The patches for this branch should be marked with the tag `[ra-improvements]` in the subject line. The branch is maintained by [Peter Bergner](mailto:bergner@vnet.ibm.com).
sel-sched-branch
:   This branch contains the implementation of the selective scheduling approach. The goal of the branch is to provide more aggressive scheduler implementation with support for instruction cloning, register renaming, and forward substitution. The branch is maintained by Andrey Belevantsev \<<abel@ispras.ru>\> and Maxim Kuvyrkov \<<mkuvyrkov@ispras.ru>\> and will be regularly merged with mainline. Patches will be marked with the tag `[sel-sched]` in the subject line.
incremental-compiler
:   This branch contains change to turn GCC into an incremental compiler. The branch is maintained by Tom Tromey <tromey@redhat.com>. Patches for this branch should be marked with the tag `[incremental]` in the subject line.
gc-improv
:   This branch is for the development of garbage collector improvements. It is the successor to the boehm-gc branch, but without integration with Boehm's GC. The branch is maintained by [Laurynas Biveinis](mailto:laurynas.biveinis@gmail.com). Patches for this branch should be marked with the tag `[gc-improv]` in the subject line.
milepost-branch
:   This branch is for GCC developments done in the Milepost project. (<http://www.milepost.eu>). The branch is maintained by Mircea Namolaru <namolaru@il.ibm.com>. Patches should be marked with the tag `[mpost]` in the subject line.
melt-branch
:   This branch is for a [Middle End Lisp Translator](https://gcc.gnu.org/wiki/MiddleEndLispTranslator) branch, including both the plugin Lisp-like facility and static analyzers developed with it. This branch is maintained by Basile Starynkevitch <basile@starynkevitch.net>. Use the `[MELT]` tag for patches.
stack
:   This branch contains a new stack alignment framework to automatically align stack for local variables with alignment requirement. The branch is maintained by H.J. Lu \<<hjl.tools@gmail.com>\>. Patches should be marked with the tag `[stack]` in the subject line.
rtl-fud-branch
:   This branch is for the development of factored use-def chains as an SSA form for RTL. Patches should be marked with the tag `[rtl-fud]` in the subject line. The branch is maintained by Steven Bosscher and Kenneth Zadeck.
[transactional-memory](https://gcc.gnu.org/wiki/TransactionalMemory)
:   This branch is for the development of transactional memory support for GCC. Patches for this branch should be marked `[trans-mem]` in the subject line. The branch is maintained by Richard Henderson.
[cxx-mem-model](https://gcc.gnu.org/wiki/Atomic/GCCMM)
:   This branch is for the implementation of the C++ memory model. Patches for this branch should be marked `[cxx-mem-model]` in the subject line. The branch is maintained by Aldy Hernandez.
dwarf4
:   This branch is for support of DWARF-4 features. DWARF-4 is currently under development, so changes on this branch will remain experimental until Version 4 is officially finalized.
[plugins](https://gcc.gnu.org/wiki/plugins)
:   This branch adds plugin functionality to GCC. See the [plugins wiki page](https://gcc.gnu.org/wiki/plugins) for details.
no-undefined-overflow
:   This branch is for tracking overflow behavior on expressions rather than on types. Patches should be marked with the tag `[no-undefined-overflow]` in the subject line. The branch is maintained by Richard Guenther.
[debuglocus](https://gcc.gnu.org/wiki/AndrewMacLeod/debuglocus)
:   This branch is an experiment to see whether improved debug information can be maintained throughout the compiler by associating a user decl with an expression, statement, or insn. The name comes from attempting to utilize the ever present source location (locus) field to carry the debug info. Further information can be found on the [debuglocus](https://gcc.gnu.org/wiki/AndrewMacLeod/debuglocus) wiki page .
alias-export
:   This branch contains the alias export and data dependency export patch. It is used to experiment with the propagation process. This branch is maintained by Andrey Belevantsev <abel@ispras.ru>.
vect256
:   This branch is for extending vectorizer to 256bit. The branch is maintained by Richard Guenther and H.J. Lu. Patches should be marked with the tag `[vect256]` in the subject line.
lra
:   This branch contains the Local Register Allocator (LRA). LRA is focused to replace GCC reload pass. The branch is maintained by Vladimir Makarov \< <vmakarov@redhat.com>\> and will be merged with mainline from time to time. Patches will be marked with the tag `[lra]` in the subject line.
libstdcxx\_so\_7-2-branch
:   This branch carries all the C++ Runtime Library (libstdc++-v3) patches that break its abi. It will be merged into the trunk as soon as the decision to move to abi version 7 will have been taken. It is maintained by [François Dumont](mailto:frs.dumont@gmail.com) and the official libstdc++-v3 maintainers Paolo Carlini, Benjamin Kosnik and Jonathan Wakely. Patches will be marked with the tag `[so_7-2]` in the subject line.
cxx-conversion
:   This branch hosts mini-projects that rewrite parts of the existing GCC code into C++. Each conversion project will be proposed for trunk integration independently. The branch is maintained by [Diego Novillo](mailto:dnovillo@google.com). Patches sent to this branch and discussions related to it should be marked with the tag `[cxx-conversion]` in the subject line. For details on working with this branch, see the [C++ conversion](https://gcc.gnu.org/wiki/cxx-conversion) page.
scalar-storage-order
:   This branch hosts the experimental support to specify a reverse storage order (byte/word order, aka endianness) for scalar components of aggregate types. The branch is maintained by [Eric Botcazou](mailto:ebotcazou@adacore.com) and will be merged with mainline from time to time. Patches will be marked with the tag `[sso]` in the subject line.
ubsan
:   This branch contains the Undefined Behavior Sanitizer (ubsan). Ubsan is an undefined behavior detector for the C family of languages. The branch is maintained by Marek Polacek \< <polacek@redhat.com>\> and will be merged with mainline from time to time. Patches will be marked with the tag `[ubsan]` in the subject line.
lto-pressure
:   This branch is for work on adding analysis to inlining (for LTO in particular) so that it can avoid inlining things that cause excessive increases in register pressure. The branch is maintained by Aaron Sawdey \<<acsawdey@linux.vnet.ibm.com>\>.

#### Architecture-specific

avx512
:   The goal of this branch is to implement Intel AVX-512 and SHA Programming Reference ([link](http://download-software.intel.com/sites/default/files/319433-015.pdf)). The branch is maintained by Yukhin Kirill \<<kirill.yukhin@intel.com>\>. Patches should be marked with the tag `[AVX512]` in the subject line.
mpx
:   The goal of this branch is to support Intel MPX technology ([link](http://download-software.intel.com/sites/default/files/319433-015.pdf)). The branch is maintained by Ilya Enkovich \<<ilya.enkovich@intel.com>\> Patches should be marked with the tag `[MPX]` in the subject line.
[st/cli-be](projects/cli.html)
:   The goal of the branch is to develop a back end producing CLI binaries, compliant with ECMA-335 specification. This branch was originally maintained by Roberto Costa \<<robsettantasei@gmail.com>\>. Since May 2007, the current maintainers are Andrea Ornstein \<<andrea.ornstein@st.com>\> and Erven Rohou \<<erven.rohou@st.com>\>.
avx2
:   The goal of this branch is to implement AVX Programming Reference (June, 2011). The branch is maintained by H.J. Lu \<<hjl.tools@gmail.com>\> and Yukhin Kirill \<<kirill.yukhin@intel.com>\>. Patches should be marked with the tag `[AVX2]` in the subject line.
ix86/gcc-4\_5-branch
:   The goal of this branch is to backport support from trunk for newer ix86 processors from AMD and Intel. It will track 4.5 branch with periodic merge from 4.5 branch. The current maintainers are Sebastian Pop \<<sebpop@gmail.com>\> and H.J. Lu \<<hjl.tools@gmail.com>\>.
ix86/gcc-4\_4-branch
:   The goal of this branch is to add support for newer ix86 processors such as AMD's Shanghai and Intel's Atom to GCC 4.4.x. The current maintainers are Dwarakanath Rajagopal \<<dwarak.rajagopal@amd.com>\> and H.J. Lu \<<hjl.tools@gmail.com>\>.
ix86/gcc-4\_3-branch
:   The goal of this branch is to add support for newer ix86 processors such as AMD's Barcelona and Intel's Westmere to GCC 4.3.x. The current maintainers are Dwarakanath Rajagopal \<<dwarak.rajagopal@amd.com>\> and H.J. Lu \<<hjl.tools@gmail.com>\>.
ix86/gcc-4\_2-branch
:   The goal of this branch is to add support for newer ix86 processors such as AMD's Barcelona and Intel's Core 2 to GCC 4.2.x. The current maintainers are Dwarakanath Rajagopal \<<dwarak.rajagopal@amd.com>\> and H.J. Lu \<<hjl.tools@gmail.com>\>.
ix86/gcc-4\_1-branch
:   The goal of this branch is to add support for newer ix86 processors such as AMD's Barcelona and Intel's Core 2 to GCC 4.1.x. The current maintainers are Dwarakanath Rajagopal \<<dwarak.rajagopal@amd.com>\> and H.J. Lu \<<hjl.tools@gmail.com>\>.
cell-4\_3-branch
:   The goal of this branch is to add fixes and additional features required for the Cell/B.E. processor (both PPE and SPE) to GCC 4.3.x. This branch is maintained by Ulrich Weigand.
cell-4\_4-branch
:   The goal of this branch is to back-port from mainline fixes and additional features required for the Cell/B.E. SPE processor to GCC 4.4.x. This branch is maintained by Ulrich Weigand. The branch is merged from gcc-4\_4-branch.
spu-4\_5-branch
:   The goal of this branch is to do development for the Cell/B.E. processor, in particular to support partitioning functions into multiple sections. This branch was created by Michael Meissner and is now maintained by Ulrich Weigand. The branch is merged from mainline.
arc-20081210-branch
:   The goal of this branch is to make the port to the ARCompact architecture available. This branch is maintained by Joern Rennecke during spring 2009, and is expected to be unmaintained thereafter.
microblaze
:   This branch contains support for the Xilinx MicroBlaze architecture. This branch will be used to update MicroBlaze support from gcc-4.1.2 to to the head. It is maintained by Michael Eager \<<eager@eagercon.com>\>.
x32
:   This branch is to implement [x32 psABI](https://sites.google.com/site/x32abi/). The branch is maintained by H.J. Lu. Patches should be marked with the tag `[x32]` in the subject line.

#### Target-specific

cygwin-improvements
:   This branch is intended as a development and proving grounds for fixes and enhancements specifically to the Cygwin port of the compiler, although some of these may touch slightly on MinGW targets as well. It is maintained by Dave Korn \<<dave.korn.cygwin@gmail.com>\> and open to contributions from any interested party; please tag patches with "[cygwin-improvements]" in the title line and post them to the GCC Patches list with a Cc: to that address.

#### Language-specific

fortran-dev
:   This branch is for disruptive changes to the Fortran front end, especially for OOP development and the [array descriptor update](https://gcc.gnu.org/wiki/ArrayDescriptorUpdate). It is maintained by Jerry DeLisle \<<jvdelisle@gcc.gnu.org>\>.
gccgo
:   This branch is for the Go frontend to gcc. For more information about the Go programming language, see [http://golang.org](http://golang.org/). The branch is maintained by Ian Lance Taylor. Patches should be marked with the tag [gccgo] in the Subject line.
[gupc](projects/gupc.html)
:   This branch implements support for UPC (Unified Parallel C). UPC extends the C programming language to provide support for high-performance, parallel systems with access to a single potentially large, global shared address space. Further information can be found on the [GNU UPC](http://gccupc.org) web page.
gcc-4\_4-plugins
:   This branch is for backporting the plugin functionality into a 4.4-based release. There will be no new code or functionality added to this branch. It is maintained by Diego Novillo. Only patches backported from mainline are accepted. They should be marked with the tag [4\_4-plugins] in the Subject line.
pph
:   This branch implements [Pre-Parsed Headers for C++](https://gcc.gnu.org/wiki/pph). It is maintained by [Diego Novillo](mailto:dnovillo@google.com) and [Lawrence Crowl](mailto:crowl@google.com). Patches should be prefixed with `[pph]` in the subject line.
pth-icm
:   This is a sub-branch of the `pph` branch. It implements [Pre-Tokenized Headers for C++](https://gcc.gnu.org/wiki/pph#Pre-Tokenized_Headers_.28PTH.29). Additionally, it contains instrumentation code in the C++ parser that was used in an incremental compiler model (icm) to study the effects of an incremental compiler cache for a compiler server. The branch is maintained by [Diego Novillo](mailto:dnovillo@google.com) and [Lawrence Crowl](mailto:crowl@google.com). Patches should be prefixed with `[pph]` in the subject line.
c++-concepts
:   This is the sandbox for renewed work on *concepts for C++*. It was originally created by Gabriel Dos Reis, with implementation contributed by Andrew Sutton. It is currently maintained by [Jason Merrill](mailto:jason@gcc.gnu.org).
var-template
:   This branch is for implementation work on *variable template for C++*. It was originally created by Gabriel Dos Reis. It is maintained by [Jason Merrill](mailto:jason@gcc.gnu.org).
cilkplus
:   This branch is for the development of [Cilk Plus](http://www.cilkplus.org) language extension support on GCC and G++ compilers. This branch is maintained by [Balaji V. Iyer](mailto:balaji.v.iyer@intel.com). Patches to this branch must be prefixed with `[cilkplus]` in the subject line. It is also highly encouraged to CC the maintainer.

### Distribution Branches {#distrobranches}

These branches are maintained by organizations distributing GCC. No changes should be made to those branches without the explicit permission of the distributing organization. The branch name should be prefixed with the initials of the distributing organization.

apple-local-200502-branch
:   This branch is for various improvements in use at Apple and to coordinate work with others. This branch is maintained by the folks at Apple. Previous branch was apple-ppc-branch.
ARM/embedded-*x\_y*-branch
:   These branches provide bug-fixes, minor enhancements and stability fixes for GCC *x.y* branches when used with ARM's embedded cores, such as the Cortex-R and Cortex-M processors. Most patches will be limited ARM specific or common back-ports from trunk, unlike the current release branches. Very occasionally these branches will hold patches that are waiting for trunk acceptance. Patches for these branches should be marked with the tag `[arm-embedded]` in the subject line. This family of branches is maintained by personnel from ARM.
csl-3\_3\_1-branch
:   CodeSourcery release based on GCC 3.3.1.
csl-arm-2004-q3-branch
:   CodeSourcery ARM 2004-Q3 release
csl-3\_4-linux-branch
:   CodeSourcery GNU/Linux compilers based on GCC 3.4.x.
csl-3\_4\_3-linux-branch
:   CodeSourcery GNU/Linux compilers based on GCC 3.4.3, with patches from the csl-arm-branch.
csl-gxxpro-3\_4-branch
:   CodeSourcery's Sourcery G++ compilers, based on GCC 3.4.x.
csl-sol210-3\_4-branch
:   CodeSourcery branch for developing Solaris 2.10 AMD64 support for GCC 3.4; this branch is used for the system compilers in OpenSolaris. This branch is maintained by CodeSourcery personnel. Patches should be marked with the tag `[csl-sol210-branch]` in the subject line.
google/integration
:   This branch contains some minimal patches that are likely not useful anywhere outside of Google's build environment. These are typically configuration patches. The branch is maintained by Diego Novillo <dnovillo@google.com>.
google/main
:   This branch contains Google local patches that are staged to be contributed to trunk. Some of these patches are either in the process of being reviewed, or have not yet been proposed. The intent of this branch is to serve as a staging platform to allow collaboration with external developers. Patches in this branch are only expected to remain here until they are reviewed and accepted in trunk. This branch is maintained by Diego Novillo <dnovillo@google.com>.
google/gcc-*x\_y*
:   Google compilers based on GCC *x.y* releases. This family of branches is maintained by Diego Novillo <dnovillo@google.com>.
google/gcc-*x\_y[\_z]*-mobile
:   Google compilers based on GCC *x.y.z* releases. These are used to build Android and ChromeOS. This family of branches is maintained by Ahmad Sharif <asharif@google.com>, Han Shen <shenhan@google.com>, and Jing Yu <jingyu@google.com>.
google/gcc-*x\_y[\_z]*-mobile-vtable-security
google/gcc-*x\_y[\_z]*-mobile-vtable-verification
:   Google compilers based on GCC *x.y.z* releases. These are used to build Android and ChromeOS. These branches are for work on function pointer and vtable security. They are maintained by Caroline Tice <cmtice@google.com>.
ibm/gcc-4\_1-branch
:   This branch provides decimal float support backported to GCC 4.1.x. It is expected to be used primarily within IBM for PowerPC-64 GNU/Linux. The branch is maintained by Janis Johnson \<<janis@us.ibm.com>\>.
ibm/gcc-4\_3-branch
:   This branch is expected to be used primarily within IBM for PowerPC-64 GNU/Linux.
ibm/gcc-4\_4-branch
:   This branch is expected to be used primarily within IBM for PowerPC-64 GNU/Linux.
ibm/power7-meissner
:   This branch is a private development branch for Power7 (PowerPC ISA 2.06) development, prior to the patches being submitted to the mainline. The branch is maintained by Michael Meissner, <meissner@linux.vnet.ibm.com>.
linaro/gcc-*x\_y*-branch
:   Linaro compilers based on GCC *x.y* releases. These branches only accept backports of patches which have been accepted to trunk. This family of branches is maintained by personnel from Linaro.
redhat/gcc-3\_2-branch
:   Red Hat GNU/Linux compilers based on GCC 3.2.x.
redhat/gcc-3\_4-branch
:   Red Hat GNU/Linux compilers based on GCC 3.4.x.
redhat/gcc-4\_0-branch
:   Red Hat GNU/Linux compilers based on GCC 4.0.x.
redhat/gcc-4\_1-branch
:   Red Hat GNU/Linux compilers based on GCC 4.1.x.
redhat/gcc-4\_3-branch
:   Red Hat GNU/Linux compilers based on GCC 4.3.x.
suse/gcc-4\_1-branch
:   SUSE GNU/Linux compilers based on GCC 4.1.x.
suse/gcc-4\_2-branch
:   SUSE GNU/Linux compilers based on GCC 4.2.x.
ubuntu/gcc-4\_2-branch
:   This branch follows the gcc-4\_2-branch, except for gcc/java, boehm-gc, libffi, libjava and zlib, which are backported from the trunk (and from gcc-4\_3-branch, once created). The branch is used as the base for the Debian and Ubuntu gcc-4.2 source package.

### Inactive Development Branches {#olddevbranches}

dfp-branch 
 edge-vector-branch 
 cp-parser-branch
 cp-parser-branch-2
 pch-branch
 gcc-3\_4-basic-improvements-branch
 mips-3\_4-rewrite-branch
 dfa-branch
 gcj-abi-2-dev-branch
 gcj-eclipse-branch
 [tree-ssa-20020619-branch](projects/tree-ssa/)
 csl-sol210-branch (Solaris 2.10 AMD64 support)
 gomp-20050608-branch
 gomp-3\_0-branch
 fixed-point
 [gimple-tuples-branch](https://gcc.gnu.org/wiki/tuples/)
 c-4\_5-branch
 alias-improvements
 cond-optab
 [var-tracking-assignments\*-branch](https://gcc.gnu.org/wiki/Var_Tracking_Assignments)
 [mem-ref2](https://gcc.gnu.org/wiki/MemRef)
:   These branches have been merged into the mainline.
apple-ppc-branch
:   This branch was for various improvements in use at Apple and to coordinate work with others. This branch was maintained by the folks at Apple. It has been superseded by apple-local-200502-branch.
thread-annotations
:   This branch contained the implementation of thread safety annotations and analysis (<https://gcc.gnu.org/wiki/ThreadSafetyAnnotation>). It was superseded by the [annotalysis branch.](#annotalysis)
[stree-branch](projects/strees/index.html)
:   This branch was for improving compilation speed and reducing memory use by representing declarations as small flat data structures whenever possible, lazily expanding them into full trees when necessary. This branch was being maintained by Matt Austern, Robert Bowdidge, Geoff Keating, and Mike Stump. Patches were marked with the tag `[stree]` in the subject line.
compile-server-branch
:   This branch was aimed at improving compile speed by caching work done between compilations. The work saved is mainly related to header file processing. This branch was maintained by Mike Stump and Per Bothner. Patches were marked with the tag `[cs]` in the subject line.
libobjc-branch
:   The branch is aimed to clean up libobjc and make it run on Darwin. Patches should be marked with the tag `[libobjc-branch]` in the subject line. Patches can be approved by Andrew Pinski \<<pinskia@gcc.gnu.org>\> or Nicola Pero \<<n.pero@mi.flashnet.it>\>.
cfg-branch
:   This branch was created to develop and test infrastructure for easier writing of new RTL based optimizations. The branch was based on GCC pre-3.3 and has been partially merged into the mainline for GCC 3.4. It is now closed, and work continues on the rtlopt-branch.
pointer\_plus
:   This branch is for the development of POINTER\_PLUS\_EXPR. Which is to be used instead of casting between an integer type and a pointer type before doing pointer addition. This branch is being maintained by Andrew Pinski. Patches for this branch should be marked with the tag `[PTR-PLUS]` in the subject line, and CC'ed to [Andrew Pinski](mailto:pinskia@gmail.com).
tree-ssa-cfg-branch
:   This branch has been merged into the tree-ssa-20020619-branch.
[ast-optimizer-branch](projects/ast-optimizer.html)
:   The purpose of this branch was to improve GCC's tree based optimizations. The patches of this branch have been moved to the tree-ssa-20020619-branch.
faster-compiler-branch
:   This was a temporary branch for compiler speedups for GCC 3.4. See [this thread](https://gcc.gnu.org/ml/gcc/2002-08/msg00498.html) for discussion of possible work still to be done in this area. The branch is unmaintained at present.
gcc-3\_3-e500-branch
:   This branch was for backporting the PowerPC/E500 back end to GCC 3.3. See [this message](https://gcc.gnu.org/ml/gcc/2003-04/msg00733.html) for details.
gcc-3\_4-e500-branch
:   This branch was for stabilization of the powerpc-\*spe architecture, and for adding support for the 8548 chip (e500 v2). This branch was maintained by Aldy Hernandez. All the e500 support was merged to mainline.
gomp-01-branch
gomp-branch
:   These two branches were initial attempts to implement OpenMP support in GCC. They were never properly maintained and have now been superseded by `gomp-20050608-branch`.
[lno-branch](projects/tree-ssa/lno.html)
:   A sub-branch of tree-ssa that aims at implementing a loop nest optimizer at the tree level. Was largely merged into mainline, and is currently unmaintained. This work now continues on the autovect-branch.
java-gui-branch
:   This was a temporary branch for development of java GUI libraries (AWT and Swing) in the libjava directory. It has been superseded by java-gui-20050128-branch
java-gui-20050128-branch
:   This was a temporary branch for development of java GUI libraries (AWT and Swing) in the libjava directory. It has been merged into mainline.
gcjx-branch
:   This branch was used for development of gcjx, a rewrite of the front end for the Java programming language. It has been superseded by gcj-eclipse-branch.
csl-hpux-branch
:   This branch was for changes to G++ to be more compatible with ABI bugs in the HP-UX C++ compiler. It is now unmaintained.
tree-cleanup-branch
:   This branch contained improvements and reorganization to the tree optimizers that were not ready in time for GCC 4.0. The goal was to cleanup the tree optimizers and improve the sequencing of the passes. It has now been merged into mainline for the 4.1 release.
bje-unsw-branch
:   This branch was dedicated to some research work by Ben Elliston at the University of New South Wales (UNSW) on transformation phase ordering. It will never merge with mainline, although a selection of patches may be submitted over time. Deleted by revision 152653.
redhat/gcc-3\_3-branch
:   This branch used to hold Red Hat GNU/Linux compilers based on GCC 3.3.x.
[rtlopt-branch](projects/cfg.html)
:   This branch was the successor to the cfg-branch, with the exception that it was based on GCC pre-3.4. The purpose of the branch was to develop and test infrastructure for CFG based code improving transformations on RTL.
killloop-branch
:   The missing optimizations and optimization improvements necessary for removing the old loop optimizer were developed on this branch. Most of these changes were merged in 4.2.
ssaupdate-branch
:   This branch served to clean up and improve utilities for the SSA form updating, as well as for related changes of the SSA form representation. Most of the changes in this branch were never merged. Part of them were incorporated in Diego Novillo's SSA updating improvement patch.
predcom
:   This branch aimed to implement predictive commoning optimization and to introduce the changes to the representation of Fortran arrays, alias analysis and dependency analysis to make it useful for the common applications (e.g., mgrid). The branch was merged in 4.3.
dataflow-branch
:   This branch has been merged into mainline on June 6, 2007 as svn revision 125624. It used to contain a replacement of back-end dataflow with df.c based dataflow. The branch was maintained by Daniel Berlin \< <dberlin@dberlin.org>\> and Kenneth Zadeck \<<zadeck@naturalbridge.com>\>
cxx0x-branch
:   This branch was for the development of C++0x features, and all features developed on this branch have been merged to mainline. Future C++0x features will be developed against mainline. This branch was deleted at revision 152320.
[cxx0x-concepts-branch](projects/cxx0x.html#concepts)
:   This branch contains the beginnings of a re-implementation of Concepts, a likely future feature of C++, using some of the code from the prototype implementation on conceptgcc-branch. It is not currently maintained.
[cxx0x-lambdas-branch](projects/cxx0x.html#lambdas)
:   This branch was for the development of lambda functions, a coming feature in C++0x. It was merged into the trunk at revision 152318.
[tree-profiling-branch](projects/tree-profiling.html)
:   This branch was for the development of profiling heuristics and profile based optimizations for trees, such as profile driven inline heuristics. Another goal of this branch was to demonstrate that maintaining the CFG and profile information over expanding from GIMPLE trees to RTL is feasible and can bring considerable performance improvements. It is no longer maintained.
new-regalloc-branch
:   Daniel Berlin and Michael Matz were working on an implementation of a graph-coloring register allocator on this branch. It is known to bootstrap under x86-linux-gnu and ppc-linux-gnu. It is no longer maintained.
structure-aliasing-branch
:   This branch contains improvements to the tree optimizers ability to do pointer-to-structure aliasing analysis and optimization. This involves some significant rework of the way our memory information is represented in the tree-ssa form. The branch was maintained by Daniel Berlin. It is no longer maintained.
[cfo-branch](projects/cfo.html)
:   The goal of this branch was to add a new extension for improving the code size optimization of GCC with code factoring methods (code motion and merging algorithms). It is no longer maintained.
reload-branch
:   This branch contains a version of reload in which the tracking of reload register lifetimes and the inheritance code has been rewritten in an attempt to make it more maintainable. It is no longer maintained.
dead/improved-aliasing-branch
:   This branch contains improvements to the tree-based aliasing infrastructure. The branch was maintained by Daniel Berlin \<<dberlin@dberlin.org>\> and Diego Novillo \<<dnovillo@redhat.com>\>. It is no longer maintained.
[sched-treegion-branch](projects/sched-treegion.html)
:   This branch was for the development of a treegion-based instruction scheduler. The branch was maintained by Chad Rosier. It is no longer maintained.
mem-ssa
:   This branch contains the implementation of Memory SSA, a new mechanism for representing memory operations in SSA form (<https://gcc.gnu.org/ml/gcc/2006-02/msg00620.html>). The branch was maintained by Diego Novillo. It is no longer maintained.
yara-branch
:   This branch contains Yet Another Register Allocator (YARA). The branch was maintained by Vladimir Makarov \< <vmakarov@redhat.com>\>. It is no longer maintained; some of the work was used as a basis for the work on the ira branch.
opt-diary
:   This branch contains the implementation of Optimization Diary, a collection of useful log information generated by the optimizers. This branch was maintained by Devang Patel. It is no longer maintained.
insn-select
:   This branch aimed to implement in early instruction selection and register class selection pass, which runs before register allocation and subsumes the current `regclass` pass. In particular the goal is to chose an alternative per instruction, usable as a base during register allocation, which ideally is not changed during reload if registers could be allocated. This will not be possible in all cases, especially when addresses generated during spilling will be invalid on the target machine. But we should be able to do away with fake register classes representing strict unions of other register classes. The branch was maintained by Michael Matz. It is no longer maintained.
addressing-modes
:   This branch aimed to clean up the way base and index registers are handled by target headers. In particular, the strict and non-strict meaning of these registers are unified and a common API is presented to implementations of the target macros. Obsolete target macros will also be removed. The branch was maintained by Paolo Bonzini. It is no longer maintained.
csl-arm-branch
:   CodeSourcery branch for developing ARM back end improvements. The branch was maintained by CodeSourcery personnel. It is no longer maintained; almost all the patches have been merged to mainline.
csl/coldfire-4\_1
:   CodeSourcery branch for developing ColdFire back end improvements. The branch was maintained by CodeSourcery personnel. It is no longer maintained; almost all the patches have been merged to mainline.
ia64-fp-model-branch
:   This branch was a development branch with the goal of implementing the improvements and features discussed at the [ia64 floating point](https://gcc.gnu.org/wiki/ia64_floating_point) page on the [GCC wiki](https://gcc.gnu.org/wiki/). It was maintained by Zack Weinberg \<<zack@codesourcery.com>\>. It is no longer maintained.
ia64-improvements
:   The goal of this branch was to improve the performance of binaries generated with GCC on the Itanium processor. Details can be found at the [IA-64 improvements](projects/ia64.html) page. This branch was maintained by Robert Kidd \<<rkidd@crhc.uiuc.edu>\> and Diego Novillo. It is no longer maintained.
[cxx-reflection-branch](projects/cxx-reflection/)
:   Part of the work on providing support for compile time reflection in C++ was done in this branch. This branch was maintained by Gabriel Dos Reis \<<gdr@integrable-solutions.net>\>. It is no longer maintained.
hammer-3\_3-branch
:   The goal of this branch was to have a stable compiler based on GCC 3.3 with improved performance for AMD's 64-bit Hammer CPUs. The branch was maintained by Jan Hubicka \<<jh@suse.cz>\> and Andreas Jaeger \<<aj@suse.de>\>. Patches added on this branch might not be appropriate for the GCC 3.3 branch due to our policies concerning release branches. All patches were added to mainline GCC (for 3.4).
objc-improvements-branch
:   This branch was originally used to merge Objective-C bug fixes and enhancements from Apple Computer into the FSF tree; this has now been completed. A later purpose of the branch was to implement the Objective-C++ language in the FSF GCC source tree. The message thread starting [here](https://gcc.gnu.org/ml/gcc/2003-07/msg00535.html) describes this at more length. This branch was being maintained by Zem Laski \<<zlaski@apple.com>\>. It is no longer maintained.
libada-gnattools-branch
:   This is the spiritual successor to the libada branch. This branch exists to solve [bug 5911](https://gcc.gnu.org/PR5911) and others, by breaking out the Ada runtime into a libada directory and the Ada tools into a gnattools directory. Work was devoted to cleaning up the configure and make machinery, and separating it as much as possible from the GCC build machinery. Nathanael Nerode \<<neroden@gcc.gnu.org>\> maintained this branch. It is no longer maintained.
libstdcxx\_so\_7-branch
:   This was a branch for experimental work on the C++ Runtime Library (libstdc++-v3) beyond the current version 6 library ABI. Paolo Carlini \<<pcarlini@suse.de>\> and Benjamin Kosnik \<<bkoz@redhat.com>\> were maintaining this branch. It is no longer maintained.
function-specific-branch
:   This branch is for development of adding function specific options to GCC. See the GCC [wiki](https://gcc.gnu.org/wiki/FunctionSpecificOpt) for a more detailed project description. Patches should be marked with the tag `[function-specific]` in the subject line. The branch has been merged into GCC 4.4.
lto-streamer
:   This was a sub-branch of the lto branch. It was intended for unstable work related to the conversion from DWARF encoding to GIMPLE streamer. It is no longer maintained.
plugin
:   This branch contains work for a plugin infrastructure in GCC to enable additional checking work. This branch is maintained by Eric Christopher <echristo@gmail.com> and will be merged with mainline from time to time. Patches will be marked with the tag `[plugin]` in the subject line.
[condate-branch](https://gcc.gnu.org/wiki/Condate)
:   The purpose of this branch is to develop a language for checking control flow graph properties. The code of this branch has not been merged in trunk.
[miro-branch](https://gcc.gnu.org/wiki/MIRO)
:   The purpose of this branch is to develop an improved Mudflap with referent objects. The code of this branch has not been merged in trunk.
var-mappings-branch
:   This branch is for improving debug information based on tracking multiple variables per computed value. The branch is maintained by Richard Guenther and Michael Matz. Patches should be marked with the tag `[varmap]` in the subject line.
mem-ref
:   This branch is for lowering the GIMPLE IL for memory accesses to a flat representation. See the GCC [wiki](https://gcc.gnu.org/wiki/MemRef) for a more detailed project description. The branch is maintained by Richard Guenther. Patches should be marked with the tag `[mem-ref]` in the subject line.
gcc-in-cxx
:   This branch was for converting GCC to be written in C++. The branch was maintained by Ian Lance Taylor.
ibm/power7-tmp
:   This branch was used to stage patches for Power7 (PowerPC ISA 2.06) from the development branch to the mainline. The branch was maintained by Michael Meissner, <meissner@linux.vnet.ibm.com>.
[lto](https://gcc.gnu.org/wiki/LinkTimeOptimization)
:   This branch implemented link-time optimization.
named-addr-spaces-branch
:   This branch was the development branch to add named address space support for architectures that have multiple address spaces. The CELL/spu architecture adds an `__ea` keyword to describe extended memory in the host chip address space instead of the local CELL/spu address space. The branch was created by Ben Elliston, modified by Michael Meissner and eventually maintained by [Ulrich Weigand](mailto:Ulrich.Weigand@de.ibm.com). All changes from this branch were merged into mainline.
named-addr-4\_3-branch
:   The goal of this branch was to backport the changes from the named-addr-spaces-branch to a GCC 4.3 tree. This branch was maintained by Michael Meissner. This branch was merged from gcc-4\_3-branch.
split
:   For development of stack splitting, as described on [the GCC wiki](https://gcc.gnu.org/wiki/SplitStacks). This branch was maintained by Ian Lance Taylor. All changes were merged into mainline.
boehms-gc
:   The goal of this branch was to test Boehm's GC feasibility as the garbage collector for GCC proper. This was a part of Google Summer of Code project, described in detail at <https://gcc.gnu.org/wiki/Garbage_collection_tuning>. The branch was maintained by [Laurynas Biveinis](mailto:laurynas.biveinis@gmail.com).
ARM/aarch64-branch
:   This branch added support for the AArch64 architecture and tracked trunk until the port was merged into mainline.
ARM/hard\_vfp\_4\_4\_branch
:   This branch contains support for the hard-VFP variant of the AAPCS calling standard and tracked gcc-4.4 development. This branch was maintained by Richard Earnshaw.
ix86/avx
:   The goal of this branch is to implement Intel AVX (Intel Advanced Vector Extensions). The branch is maintained by H.J. Lu \<<hjl.tools@gmail.com>\>. Patches should be marked with the tag `[AVX]` in the subject line.
annotalysis
:   This branch contains the implementation of thread safety annotations and analysis (<https://gcc.gnu.org/wiki/ThreadSafetyAnnotation>). The branch was maintained by [Delesley Hutchins](mailto:delesley@google.com).
fortran-exp
:   This branch contained experimental changes to the Fortran front end, initially for array constructor refactoring using splay-tree and other areas of optimization. It was maintained by Jerry DeLisle \<<jvdelisle@gcc.gnu.org>\>.
fortran-caf
:   This branch contained experimental changes to the Fortran front end for implementing the library calls for coarray communication. It was maintained by Tobias Burnus \<<burnus@gcc.gnu.org>\>.

