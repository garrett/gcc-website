# FTensor build and test guide

This page is a guide to running the testing and timing programs for the [FTensor](http://www.gps.caltech.edu/~walter/FTensor/index.html) tensor class library as part of GCC integration testing. FTensor can be built with GCC version 3.1 and later.

## Resource usage

The FTensor distribution, available at [FTensor-1.1pre22.tar.gz](http://www.gps.caltech.edu/~walter/FTensor/FTensor-1.1pre22.tar.gz), is a 169K file. The uncompressed distribution comprises some 2.3 MB of source files. Building the testing and timing programs adds between 20 and 32 MB of object files and executables to this.

On a Pentium III laptop, the conformance tests take 7 minutes and the performance tests take 3 minutes.

## Prepare

To prepare for a build, perform the following:

-   Unpack the file:
    -   `tar zxf FTensor-1.1pre22.tar.gz`, or
    -   `gunzip -c FTensor-1.1pre22.tar.gz | tar xf -`
-   Change directory to the repository thus created:
    -   `cd FTensor-1.1pre22`

## Testing

FTensor tests in directory `tests/conformance` exercise most of the package's possible operations, and the tests in directory `tests/speed` compare how fast the FTensor classes are compared to ordinary arrays. See the file `tests/README` for more information about the two sets of tests.

To build and run the subset of conformance tests meant for compiler testing, do:

` cd tests/conformance  make CXX=<compiler> CXXOPTIMIZE="<flags>" test_compiler  ./test_compiler`

The conformance tests print out `PASS` or `FAIL` followed by a unique test name. There should be no failures.

The path to the C++ compiler is defined in `CXX` and the compiler optimization options are defined in `CXXOPTIMIZE` in the Makefile. If you don't override them as shown above, `CXX` will be `g++` and `CXXOPTIMIZE` will be the flags that are normally used by FTensor developers and users. Testing with the usual flags ensures that FTensor will work for its users on your platform; testing with other optimizations might uncover GCC bugs.

To build and run the performance tests using the optimizations that are normally used for them, simply do:

` cd tests/speed ./one_over_script`

To use different optimizations and/or a different compiler, first do:

` make CXX=<compiler> CXXOPTIMIZE="<flags>" one_over one_over_fast`

In this case when the the script `one_over_script` invokes make, there is nothing for make to do.

In addition to printing timing information, `one_over_script` prints out `PASS` or `FAIL` for each performance test, followed by the unique test name. There should be no failures.

## Timing

The timing information output by the script `tests/speed/one_over_script` can be compared for different versions of GCC, starting with version 3.1.

## Cleanup

Cleanup (to prepare the directory for a fresh run, e.g. with another version of the compiler), is done as follows:

    (cd tests/conformance; make clean)
    (cd tests/speed; make clean)
