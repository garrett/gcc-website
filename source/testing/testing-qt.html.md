# Qt build and test guide

This page is a guide to building the Qt application development toolkit as part of GCC integration testing.

## Resource usage

The Qt distribution, available from Trolltech's site at [qt-x11-2.3.0.tar.gz](http://ftp.gwdg.de/pub/linux/troll/qt/source/qt-x11-2.3.0.tar.gz), is an 8.7 MB file. The uncompressed distribution comprises 40 MB of source files. Building the package adds 40 to 75 MB of object files and executables to this.

Building the entire Qt distribution on a 750 Mhz Pentium III laptop takes about 40 minutes.

## Prepare

To prepare the Qt distribution, perform the following:

-   Unpack the file:
    -   `tar zxf qt-x11-2.3.0.tar.gz`, or
    -   `gunzip -c qt-x11-2.3.0.tar.gz | tar xf -`
-   Rename the directory thus created and move to it:
    -   `mv qt-x11-2.3.0 qt; cd qt`
-   Define and export environment variables:
    -   `PATH` should include the `bin` directory of the GCC installation under test.
    -   `LD_LIBRARY_PATH` should include the `lib` directory of the GCC installation under test.
    -   `QTDIR` should be the full pathname of the top directory of the Qt distribution (the one we just renamed as `qt`).

## Configure

The configuration file, which on a GNU/Linux system is `configs/linux-g++-shared`, the location of the compiler and libraries and the compiler flags used to build Qt. By default it uses compilers found in `PATH`, shared libraries found using `LD_LIBRARY_PATH`, and the compiler flags `-pipe -O2`. You can specify different flags by changing the values of `SYSCONF_CXXFLAGS` and `SYSCONF_CFLAGS` in the configuration file.

Run the configure script before building the first time or after modifying the configuration file:

    ./configure

The configure script waits for input after printing:

    This is the Qt Free Edition.

    You are licensed to use this software under the terms of either
    the Q Public License (QPL) or the GNU General Public License (GPL).

    Type 'Q' to view the Q Public License.
    Type 'G' to view the GNU General Public License.
    Type 'yes' to accept this license offer.
    Type 'no' to decline this license offer.

    Do you accept the terms of the license?

If you invoke `configure` from a script you can simply have an input file with the word "yes".

## Build

To build all of Qt, do:

    make all

The `all` target includes several other targets. You can build them separately if that's more convenient.

## Testing

Qt has no automated tests.

## Interoperability testing

Some of the Qt make targets depend on libraries built for earlier targets. Qt can be used to test interoperability of two compilers by doing the following:

-   Configure to use one compiler by modifying the configuration file or by setting `PATH` and `LD_LIBRARY_PATH` for that compiler and its libraries.
-   `make src-mod src-mt src-sub`
-   Configure to use a second compiler.
-   `make sub-tools sub-tutorial sub-examples`

## Timing

Qt has no performance tests.

## Cleanup

Cleanup (to prepare the directory for a fresh run, e.g. with another version of the compiler), is done as follows.

    make -k clean
