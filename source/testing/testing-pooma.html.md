# FreePOOMA build and test guide

This page is a guide to building the [FreePOOMA](http://www.nongnu.org/freepooma/) parallel object-oriented code and running its test programs.

There is currently no FreePOOMA distribution which is usable with GCC 3.4 and up. You need to get a checkout of CVS HEAD.

## Testing FreePOOMA

To checkout and configure the source code, compile the library, run the testsuite and build example and benchmark programs, use:

-   Checkout the sources:
    -   `export CVS_RSH="ssh"`
    -   `cvs -z3 -d:ext:anoncvs@savannah.nongnu.org:/cvsroot/freepooma co freepooma`
-   Change directory to the repository thus created:
    -   `cd freepooma`
-   Configure for using gcc:
    -   `export POOMASUITE=GCC`
    -   `./configure --arch LINUXgcc --serial --opt --cpp g++-3.4`

    Substitute `g++-3.4` with the compiler to use. Adjust CFLAGS in the `config/arch/LINUXgcc.conf` file.
-   Build the library:
    -   `make`
-   Run the testsuite and compare with baseline:
    -   `make -k check 2>&1 | tee testresults-GCC`
    -   `diff -u testresults testresults-GCC | grep '^[+-]PASSED\|FAILED'`
-   Build examples and benchmarks:
    -   `make examples`
    -   `make benchmarks`

In case of build problems consult the build log files called `test.o_APP.info` for compilation and `test_APP.info` for linking. If unsure ask someone who can diagnose and fix the problem.

## Cleanup

Before rerunning tests be sure to wipe out existing files using `make cleansuite`. Then start over with configuration.
