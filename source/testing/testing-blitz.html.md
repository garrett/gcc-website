# Blitz++ build and test guide

This page is a guide to building the [Blitz++](http://sourceforge.net/projects/blitz/) scientific computing class library as part of GCC integration testing.

## Resource usage

The Blitz++ 0.7 distribution, available via the [Blitz++ download page](http://sourceforge.net/projects/blitz/files/) at SourceForge is a 2.0 MB file. The uncompressed distribution comprises 10.5 MB of source files. Building and running the test suite adds an additional 75 or so MB of object files and executables to this, and building the Blitz++ example programs adds another 50 MB.

Building and running the Blitz++ test suite on a 750 Mhz Pentium III laptop takes 17 minutes. Building the Blitz++ example programs takes another 20 minutes.

All of the make commands below have been tested with `make -j 8`.

## Prepare

To prepare the Blitz++ distribution, perform the following:

-   Unpack the file:
    -   `tar zxf Blitz++-0.7.tar.gz`, or
    -   `gunzip -c Blitz++-0.7.tar.gz | tar xf -`
-   Change to the new directory:
    -   `cd Blitz++-0.7`

Running `configure` and `make` require that the directory containing the C++ libraries to test is in `LD_LIBRARY_PATH`.

## Configure

You can use the simplest form of configuration if the directory containing the compiler under test is in your `PATH` variable and you don't want specific compilation options:

    ./configure --prefix=`pwd` --with-cxx=gcc

To specify optimizations (by default Blitz++ is built using no optimization flags) and to specify the full pathname of the compiler, configure with:

    GCC_INST=directory/where/you/installed/GCC
    ./configure --prefix=`pwd` \
        --with-cxx="gcc:${GCC_INST}/bin/g++ <optimization_flags>" 

## Testing

To build the Blitz++ library, do:

    make lib

To build and run the Blitz++ tests, do:

    make check-testsuite

The output of `make check-testsuite` should include the line "All tests passed". Failure to build a test prevents the rest of the tests from being built and any of the tests from running. To build and run an individual test (see the list for `EXTRA_PROGRAMS` from `testsuite/Makefile`), do:

    cd testsuite
    make <testname>
    ./<testname> || echo failed

The Blitz++ distribution also includes examples. To build the examples as well as building and running the tests, do:

    make check-examples

As with the testsuite, the output should include the line "All tests passed".

## Interoperability testing

Use Blitz++ to test interoperability of two compilers by first configuring to use one compiler, building the library with `make lib`, running `configure` a second time specifying the second compiler, and then running `make check-testsuite` or `make check-examples`.

## Timing

The Blitz++ distribution includes benchmarks, most of which have pieces in C++, Fortran 77, and Fortran 90. These compare the performance of C++ code that uses the Blitz++ library with similar functionality in Fortran. The loop benchmarks don't require Fortran 90 and could be used to compare timings of two versions of GGC.

## Cleanup

Cleanup (to prepare the directory for a fresh run, e.g. with another version of the compiler), is done as follows:

    make -k clean
