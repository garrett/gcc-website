# Boost build and test guide

This page is a guide to running the testing and timing programs for the [Boost](http://www.boost.org/) class libraries as part of GCC integration testing.

## Resource usage

Boost distributions are available from SourceForge, linked from the [Boost web site](http://www.boost.org/). These instructions are for version 1.27.0, whose gzipped tar file is 3.8 MB. The uncompressed distribution comprises some 16 MB of source files. Building the library and tests adds between 80 and 140 MB of object files and executables to this.

On a Pentium III laptop, building the Boost library takes 15 minutes and building and running the regression tests takes an additional 5 minutes.

## Prepare

To prepare for a build, perform the following:

-   Unpack the file:
    -   `tar zxf boost_1_27_0.tar.gz`, or
    -   `gunzip -c boost_all.tar.gz | tar xf -`
-   Change directory to the repository thus created.
    -   `cd boost_1_27_0`
-   Define and export environment variables:
    -   `PATH` should include the `bin` directory of the GCC installation under test.
    -   `LD_LIBRARY_PATH` should include the `lib` directory of the GCC installation under test.
    -   `BOOST_ROOT` should be the full pathname of the top directory of the boost distribution (the one to which we just changed).
-   Change a configuration check to recognize GCC versions later than 3.1; in file `boost/config/compiler/gcc.hpp`, change `__GNUC_MINOR__ > 1` to `__GNUC_MINOR__ > 4`.
-   The test `libs/integer/integer_test.cpp` assumes that types `int` and `long` are the same size. If this isn't true for your target, modify the test file using `sed '/PRIVATE_FIT_TESTS/s/LONG_MAX/INT_MAX/'`.

## Configure

The Boost package gets information about how to compile and link from the file `compiler.cfg`. On a GNU/Linux system you can replace the contents of that file with the following:

    linux
    gcc-3.1
    GCC 3.1
    g++ -c -w -ftemplate-depth-30 -I%include %source
    g++ -o boosttmp.exe -w -ftemplate-depth-30 -I%include %source -lrt
    GNU<br><a href="http://gcc.gnu.org/">GCC</a><br>3.1

To build the package with specific optimizations, add them to the compile line in this file.

## Build

To build the Boost class library, do:

    cd ${BOOST_ROOT}/tools/build/jam_src
    make all
    cd ${BOOST_ROOT}
    tools/build/jam_src/jam0 \
        -sBOOST_ROOT=${BOOST_ROOT} -sTOOLS="gcc"

If you don't have Python installed, there will be build failures for Python support. The `wregex` support fails to build if glibc is older than 2.2.4. These failures do not prevent the build from continuing.

## Test

Build the Boost regression tests with:

    cd ${BOOST_ROOT}/status
    g++ -I${BOOST_ROOT} -o regression regression.cpp

Run the tests with:

    cd ${BOOST_ROOT}/status
    ./regression

"Pass" or "Fail" appears at the end of the output for each test. There should be no failures, although if your glibc is older than 2.2.4 then tests `libs/integer/integer_traits_test.cpp` and `libs/regex/test/regress/wregex_test.cpp` will fail.

## Interoperability testing

Boost can be used to test interoperability of two compilers by building the Boost class library with one compiler and the regression test with a different compiler.

## Time

TBD

## Cleanup

Cleanup (to prepare the directory for a fresh run, e.g. with another version of the compiler), is done as follows:

    cd ${BOOST_ROOT}
    rm -f ${BOOST_ROOT}/build/jam_src/jam0
    rm -f ${BOOST_ROOT}/status/regression
    find . -name \*.o -exec rm -f {} \;
