# LAPACK build and test guide

This page is a guide to building the [LAPACK](http://www.netlib.org/lapack/index.html) linear algebra package and running its testing and timing programs as part of GCC integration testing.

## Resource usage

The LAPACK distribution, available from the netlib repository at [lapack.tgz](http://www.netlib.org/lapack/lapack.tgz), is a 5 MB file. The uncompressed LAPACK distribution comprises some 35 MB of source files. Building the libraries, test and timing programs adds between 80 and 100 MB of objects, libraries and executables to this.

On a 750 Mhz Pentium III laptop, building and testing LAPACK takes about 30 minutes. Building and running the timing programs takes a lot longer.

## Prepare

To prepare the LAPACK distribution, do the following:

-   Unpack the .tgz file:
    -   `tar zxf lapack.tgz`, or
    -   `gunzip -c lapack.tgz | tar xf -`
-   Change directory to the repository thus created:
    -   `cd LAPACK`
-   Edit the include file of the main Makefile (`make.inc`):
    -   `PLAT = G77`
    -   `FORTRAN = <path_to_GCC_installation>/bin/g77`
    -   `OPTS = -g -O2 -funroll-loops`
    -   `NOOPT = -g`
    -   `LOADER = $(FORTRAN)`
    -   `LOADOPTS = -Wl,-rpath=<path_to_GCC_installation>/lib         `
    -   `RANLIB = ranlib`
    -   `BLASLIB = ../../blas$(PLAT).a`

    Note that the value of RANLIB depends on ranlib being available (and necessary) on your operating system.
-   If you want to test LAPACK with an uninstalled compiler, change the following in the above:
    -   Add: `GCC_DIR = /path/to/gcc/build/directory`
    -   Change: `FORTRAN = $(GCC_DIR)/gcc/g77 -B$(GCC_DIR)/gcc/`
    -   Change: `LOADER = $(FORTRAN) -L$(GCC_DIR)/<target>/libf2c`

    where `<target>` is the target triple for your system (e.g. `mips-sgi-irix6.5`)

A final note: On 64-bit targets, LAPACK cannot be built using GCC 2.95.2 without specifying the `-femulate-complex` flag. Building it using GCC 3.x should not need that flag.

On Alphas it is necessary to add the flag `-mieee` to OPTS, NOOPT and LOADOPTS.

## Build

The LAPACK Makefile assumes that you have the current directory in your path. If this is not the case in your shell setup, use the following command line:

`(PATH=$PATH:. export PATH; make install blaslib lapacklib tmglib testing blas_testing)`

If the main memory in your machine is 512 Mbyte or larger, you are able to run the timing programs in addition to the testing programs that are run with the command above. In that case, issue the command:

`(PATH=$PATH:. export PATH; make install blaslib lapacklib tmglib testing blas_testing timing blas_timing)`

## Test

The output of the testing programs is written into files with extension `.out` in the `TESTING` subdirectory of the main LAPACK directory. Use the command `grep -i fail *.out` to find the number of failing tests.

GCC 3.1 should not result in more failures than GCC 3.0.4

## Interoperability testing

The make targets `testing` and `blas_testing` build tests that use libraries built for the previous targets. LAPACK can be used to test interoperability of two compilers by first editing the `make.inc` file to use one compiler and building all of the libraries with that, and then modifying `make.inc` to specify a second compiler and making the testing targets.

## Time

TBD

## Cleanup

Cleanup (to prepare the directory for a fresh run, e.g. with another version of the compiler), has to be done as follows:

`` (PATH=$PATH:. export PATH; make clean; rm `find . -name '*.SUMM' -print`) ``

The reason for this is that the LAPACK programs leave files behind with the extension .SUMM, which are opened with ` STATUS='NEW'` - this will result in an abort if the files already exist.
