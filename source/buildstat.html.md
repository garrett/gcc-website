# Build status for GCC

These pages summarize build reports for GCC.

-   [GCC 4.9.x](gcc-4.9/buildstat.html)
-   [GCC 4.8.x](gcc-4.8/buildstat.html)
-   [GCC 4.7.x](gcc-4.7/buildstat.html)
-   [GCC 4.6.x](gcc-4.6/buildstat.html)
-   [GCC 4.5.x](gcc-4.5/buildstat.html)
-   [GCC 4.4.x](gcc-4.4/buildstat.html)
-   [GCC 4.3.x](gcc-4.3/buildstat.html)
-   [GCC 4.2.x](gcc-4.2/buildstat.html)
-   [GCC 4.1.x](gcc-4.1/buildstat.html)
-   [GCC 4.0.x](gcc-4.0/buildstat.html)
-   [GCC 3.4.x](gcc-3.4/buildstat.html)
-   [GCC 3.3.x](gcc-3.3/buildstat.html)
-   [GCC 3.2.x](gcc-3.2/buildstat.html)
-   [GCC 3.1.x](gcc-3.1/buildstat.html)
-   [GCC 3.0.x](gcc-3.0/buildstat.html)
-   [GCC 2.95.x](gcc-2.95/buildstat.html)

