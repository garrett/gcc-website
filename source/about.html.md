# GCC: About

These pages are maintained by the GCC team and it's easy to [contribute](../contribute.html#webchanges).

The web effort was originally led by Jeff Law. For the last decade or so Gerald Pfeifer has been leading the effort, but there are many [contributors](http://gcc.gnu.org/onlinedocs/gcc/Contributors.html).

The web pages are under [CVS control](#cvs) and you can [browse the repository](http://gcc.gnu.org/cgi-bin/cvsweb.cgi/wwwdocs/) online. The pages on gcc.gnu.org are updated "live" directly after a change has been made; www.gnu.org is updated once a day at 4:00 -0700 (PDT).

Please send feedback, problem reports and patches to our [mailing lists](lists.html).

Want to contribute? Any help concerning the items below is welcome, as are suggestions. Suggestions accompanied by patches have a higher chance of being implemented soon. ;-)

-   Improve navigation, with a consistent (short) menu on every page.
-   Set up a system that automatically checks the mirrors list.

    It should detect mirrors that have gone away, are persistently down, or very out of date (the last being easy to do for those carrying snapshots, harder for those with releases only). DJ Delorie \<<dj@redhat.com>\> has some scripts to do this already.

* * * * *

## Using the CVS repository {#cvs}

Assuming you have both CVS and SSH installed, you can check out the web pages as follows:

1.  Set CVS\_RSH in your environment to `ssh`.
2.  `cvs -q -d :ext:username@gcc.gnu.org:/cvs/gcc checkout -P wwwdocs` where *username* is your user name at gcc.gnu.org

For anonymous access, use `-d :pserver:cvs@gcc.gnu.org:/cvs/gcc` instead.

### Checking in a change

The following is a very quick overview of how to check in a change. We recommend you list files explicitly to avoid accidental checkins and prefer that each checkin be of a complete, single logical change.

1.  Sync your sources with the master repository via "`cvs update`". This will also identify any files in your local tree that you have modified.
2.  We recommend reviewing the output of "`cvs diff`".
3.  Use "`cvs commit`" to check in the patch.
4.  Upon checkin a message will be sent to the gcc-cvs-wwwdocs mailing list.

As changes are checked in, the respective pages are preprocessed via the script `wwwdocs/bin/preprocess` which in turn uses a tool called MetaHTML. Among others, this preprocessing adds CSS style sheets, XML and HTML headers, and our standard footer. The MetaHTML style sheet is in `wwwdocs/htdocs/style.mhtml`.

## The host system {#system}

The setup of the machine running the gcc.gnu.org site is also available, through [cvsweb](http://sourceware.org/cgi-bin/cvsweb.cgi/?cvsroot=sourceware) and anonymous read-only CVS. Use the same procedure as above, but use `:pserver:anoncvs@gcc.gnu.org:/cvs/sourceware` for the repository and `infra` for the module.
