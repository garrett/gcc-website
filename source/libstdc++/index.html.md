## What... is your name?

The GNU Standard C++ Library v3, or libstdc++-v3. Older snapshots of this library were given the name libstdc++-2.9x up until the release of GCC 3. This is a complete rewrite from the previous libstdc++-v2.

## What... is your quest?

This is an ongoing project to implement the ISO 14882 Standard C++ Library as described in chapters 17 through 30 and Annex D. **Participation is welcome!**

## What... is the airspeed velocity of an unladen swallow?

[African or European?](http://www.armory.com/swallowscenes.html)

* * * * *

### Downloading

libstdc++-v3 is developed and released as part of GCC, separate snapshots are no longer made available. The libstdc++-v3 sources are included with the GCC sources and can be downloaded from the GCC FTP area or from any of the [GCC mirror sites](../mirrors.html).

The SVN source repository is part of the GCC repository. Instructions for anonymous SVN access are the same as [those for the rest of the compiler](../svn.html). Checking out the GCC `trunk` or one of the branches will also get the library sources; to retrieve only the library, check out `trunk/libstdc++-v3` instead.
