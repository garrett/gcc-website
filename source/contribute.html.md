# Contributing to GCC

We strongly encourage individuals as well as organizations to contribute to the development of GCC in the form of new features, new or improved optimizations, bug fixes, documentation updates, web page improvements, etc....

There are certain legal requirements and style issues which all contributions must meet:

-   [Legal Prerequisites](#legal)
-   [Coding Standards](#standards)
-   [Testing Patches](#testing)
-   [Documentation Changes](#docchanges)
-   [Web Site Changes](#webchanges)
-   [Submitting Patches](#patches)
-   [Announcing Changes (to our Users)](#announce)

* * * * *

## Legal Prerequisites
{:#legal}

Before we can incorporate significant contributions, certain legal requirements must be met.

The FSF prefers that a contributor files a copyright assignment for large contributions. [See some documentation by the FSF](https://www.gnu.org/prep/maintain/maintain.html#Legal-Matters) for details and contact us (either via the <gcc@gcc.gnu.org> list or the GCC maintainer that is taking care of your contributions) to obtain the relevant forms. The most common forms are an assignment for a specific change, an assignment for all future changes, and an employer disclaimer, if an employer or school owns work created by the developer. It's a good idea to send <assign@gnu.org> a copy of your request.

If a contributor is reluctant to sign a copyright assignment for a change, a copyright disclaimer to put the change in the public domain is acceptable as well. The copyright disclaimer form is different than an employer disclaimer form. A copyright assignment is more convenient if a contributor plans to make several separate contributions.

Small changes can be accepted without a copyright disclaimer or a copyright assignment on file.

## Coding Standards
{:#standards}

All contributions must conform to the [GNU Coding Standards](http://www.gnu.org/prep/standards_toc.html). There are also some [additional coding conventions for GCC](codingconventions.html); these include documentation and testsuite requirements as well as requirements on code formatting.

Submissions which do not conform to the standards will be returned with a request to address any such problems. To help with the preparation of patches respecting these rules, one can use the script [contrib/check\_GNU\_style.sh](https://gcc.gnu.org/viewcvs/gcc/trunk/contrib/check_GNU_style.sh) to detect some of the common mistakes.

## Testing Patches
{:#testing}

All patches must be thoroughly tested. We encourage you to test changes with as many host and target combinations as is practical. In addition to using real hardware, you can [use simulators](simtest-howto.html) to increase your test coverage.

Much of GCC's code is used only by some targets, or used in quite different ways by different targets. When choosing targets to test a patch with, make sure that your selections exercise all aspects of the code you are changing. For instance, a change to conditional branch handling should be tested both with targets that use `cc0`, and targets that don't.

You will of course have tested that your change does what you expected it to do: fix a bug, improve an optimization, add a new feature. If the test framework permits, you should automate these tests and add them to GCC's testsuite. You must also perform regression tests to ensure that your patch does not break anything else. Typically, this means comparing post-patch test results to pre-patch results by testing twice or comparing with recent posts to the [gcc-testresults list](https://gcc.gnu.org/ml/gcc-testresults/).

### Which tests to perform

If your change is to code that is not in a front end, or is to the C front end, you must perform a complete build of GCC and the runtime libraries included with it, on at least one target. You must bootstrap all default languages, not just C. You must also run all of the testsuites included with GCC and its runtime libraries. For a normal native configuration, running

>     make bootstrap
>     make -k check

from the top level of the GCC tree (**not** the `gcc` subdirectory) will accomplish this.

If your change is to the C++ front end, you should rebuild the compiler, `libstdc++`, `libjava` and run the C++ testsuite. If you already did a complete C,C++,Java bootstrap from your build directory before, you can use the following:

>     make clean-target-libstdc++-v3                    # clean libstdc++ and ...
>     test -d */libjava && make -C */libjava clean-nat  # ... parts of libjava
>     make all-target-libstdc++-v3 all-target-libjava   # rebuild compiler and libraries
>     make -k check-c++                                 # run C++/libstdc++ testsuite

If your change is to a front end other than the C or C++ front end, or a runtime library other than `libgcc`, you need to verify only that the runtime library for that language still builds and the tests for that language have not regressed. (Most languages have tests stored both in the `gcc` subdirectory, and in the directory for the runtime library.) You need not bootstrap, or test other languages, since there is no way you could have affected them.

Since the Ada front end is written in Ada, if you change it you must perform a complete bootstrap; however, running other language testsuites is not necessary.

In all cases you must test exactly the change that you intend to submit; it's not good enough to test an earlier variant. The tree where you perform this test should not have any other changes applied to it, because otherwise you cannot be sure that your patch will work correctly on its own. Include all your new testcases in your testsuite run.

## Documentation Changes
{:#docchanges}

Documentation changes do not require a new bootstrap (a working bootstrap is necessary to get the build environment correct), but you must perform `make info` and `make dvi` and correct any errors. You should investigate complaints about overfull or underfull hboxes from `make dvi`, as these can be the only indication of serious markup problems, but do not feel obliged to eliminate them all.

## Web Site Changes
{:#webchanges}

Changes to the web site must [validate](http://validator.w3.org/) as XHTML 1.0 Transitional. The web pages as checked into CVS do not include DOCTYPE declarations; they are automatically added when the web server checks out its copy. To validate your changes, temporarily insert this header in place of the \<html\> tag, then use the "upload file" mode of the validator.

>     <?xml version="1.0" encoding="ISO-8859-1"?>
>     <!DOCTYPE html 
>               PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
>               "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
>     <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

Please mark patches with the tag [wwwdocs] in the subject line.

Web site sources are [in CVS](about.html#cvs).

## Submitting Patches
{:#patches}

Every patch must have several pieces of information, *before* we can properly evaluate it:

A description of the problem/bug and how your patch addresses it.
:   For new features a description of the feature and your implementation. For bugs a description of what was wrong with the existing code, and a reference to any previous bug report (in the [GCC bug tracker](https://gcc.gnu.org/bugzilla/) or the [gcc-bugs archives](http://gcc.gnu.org/ml/gcc-bugs/)) and any existing testcases for the problem in the GCC testsuite.

Testcases
:   If you cannot follow the recommendations of the [GCC coding conventions](codingconventions.html) about testcases, you should include a justification for why adequate testcases cannot be added.

ChangeLog
:   A ChangeLog entry as plaintext; see the various ChangeLog files for format and content, and the [GCC coding conventions](codingconventions.html) and [GNU Coding Standards](http://www.gnu.org/prep/standards_toc.html) for further information. The ChangeLog entries should be plaintext rather than part of the patch since the top of the ChangeLog changes rapidly and a patch to the ChangeLog would probably no longer apply by the time your patch is reviewed. If your change fixes a PR, put text in the ChangeLog entry mentioning the PR. The `svn commit` machinery understands how to extract this information and automatically append the commit log to the PR. In order to be recognized, the text must fit a particular form. It must start with "PR", and then must include the category and PR number. For instance, `PR java/2369` is valid. Multiple PRs can be mentioned in a single message.

Bootstrapping and testing
:   State the host and target combinations you used to do [proper testing](#testing) as described above, and the results of your testing.

The patch itself
:   If you are accessing the [SVN repository](svn.html) at gcc.gnu.org, please configure your svn to use GNU diff and generate patches using the "`-up`" or "`-cp`" options. See [SVN setup instructions](https://gcc.gnu.org/wiki/SvnSetup) for more details.
     Do not include generated files as part of the patch, just mention them in the ChangeLog (e.g., "\* configure: Regenerate.").

Don't mix together changes made for different reasons. Send them *individually*. Ideally, each change you send should be impossible to subdivide into parts that we might want to consider separately, because each of its parts gets its motivation from the other parts. In particular, changes to code formatting to conform to coding standards are best not mixed with substantive changes, because that makes it difficult to see what the real changes are. (In the case of a very large reorganization of code, it may make sense to separate changes even further to make it clearer what has changed; for example, by first sending structural changes that make subsequent changes easier but do not change GCC's behavior, then new code, then the changes that actually make use of the new code and change GCC's behavior.)

We prefer patches posted as plain text or as MIME parts of type `text/x-patch` or `text/plain`, disposition `inline`, encoded as `7bit` or `8bit`. It is strongly discouraged to post patches as MIME parts of type `application/`*whatever*, disposition `attachment` or encoded as `base64` or `quoted-printable`. Avoid MIME large-message splitting (`message/partial`) at all costs.

If the patch is too big or too mechanical, posting it gzipped or bzip2ed and uuencoded or encoded as a `base64` MIME part is acceptable, as long as the ChangeLog is still posted as plain text.

When you have all these pieces, bundle them up in a mail message and send it to [the appropriate mailing list(s)](lists.html). (Patches will go to one or more lists depending on what you are changing.) For further information on the GCC SVN repository, see the [Anonymous read-only SVN access](svn.html) and [Read-write SVN access](svnwrite.html) pages.

(Everything listed here still applies if you can check in the patch without further approval under the [GCC write access policies](svnwrite.html#policies), except that ChangeLog entries may be included as part of the patch and diffs representing new files may be omitted, especially if large, since they can be accessed directly from the repository.)

### Pinging patches, Getting patches applied

If you do not receive a response to a patch that you have submitted within two weeks or so, it may be a good idea to chase it by sending a follow-up email to the same list(s). Patches can occasionally fall through the cracks. Please be sure to include a brief summary of the patch and the URL of the entry in the mailing list archive of the original submission.

If you do not have write access and a patch of yours has been approved, but not committed, please advise the approver of that fact. You may want to point out lack of write access in your initial submission, too.

## Announcing Changes (to our Users)
{:#announce}

Everything that requires a user to edit his Makefiles or his source code is a good candidate for being mentioned in the release notes.

Larger accomplishments, either as part of a specific project, or long term commitment, merit mention on the front page. Examples include projects like tree-ssa, new back ends, major advances in optimization or standards compliance.

The gcc-announce mailing list serves to announce new releases and changes like front ends or back ends being dropped.
