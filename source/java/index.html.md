# The GNU Compiler for the Java^[TM](tm.html)^ Programming Language

GCJ is a portable, optimizing, ahead-of-time compiler for the Java Programming Language. It can compile Java source code to Java bytecode (class files) or directly to native machine code, and Java bytecode to native machine code.

Compiled applications are linked with the GCJ runtime, `libgcj`, which provides the core class libraries, a garbage collector, and a bytecode interpreter. `libgcj` can dynamically load and interpret class files, resulting in mixed compiled/interpreted applications. It has been merged with [GNU Classpath](http://www.gnu.org/software/classpath/) and supports most of the 1.4 libraries plus some 1.5 additions.

GCJ can also be configured as a cross-compiler, suitable for embedded systems programming. Recent versions of the [GNU Debugger](http://www.gnu.org/software/gdb/) support GCJ ([short tutorial](gdb.html)).

## GCJ News

September 22, 2009
:   GCJ support on Windows (Cygwin and MinGW) targets has been enhanced with a number of bugfixes, and the option to build libgcj in DLL form for dynamic runtime linking.
March 30, 2007
:   Keith Seitz and Kyle Galloway have made considerable progress on GCJ's implementation of the JDWP. It is now possible to use eclipse to debug interpreted Java code using libgcj as the VM.
February 20, 2007
:   Jakub Jelinek checked in a patch to make `java.util.TimeZone` read system timezone files. Now libgcj does not require updating when a timezone change is published.
February 15, 2007
:   David Daney and Johannes Schmidt of Avtrex checked in a new `gc-analyze` tool. This is a valuable debugging tool which can be used to analyze heap dumps from libgcj.
January 8, 2007
:   We've merged the `gcj-eclipse` branch to svn trunk. The merge changes gcj to use the Eclipse compiler as a front end, enabling all 1.5 language features. This merge also brings in a new, generics-enabled version of Classpath, including some new tools. This new code will appear in GCC 4.3.
September 11, 2006
:   Joel Dice has [announced](http://gcc.gnu.org/ml/java/2006-09/msg00020.html) [micro-libgcj 0.1](http://ulibgcj.sourceforge.net/status.html), a lightweight version of libgcj intended to provide a usable subset of Java's features while remaining small and self-contained. This release fixes compatibility issues present in the initial 0.0 release and adds optional [SWT support](http://ulibgcj.sourceforge.net/uswt.html).
June 13, 2006
:   The May 2006 issue of Linux Journal contains the article "[Embedded Java with GCJ](http://www.linuxjournal.com/article/8757)" by Gene Sally.
June 6, 2006
:   RMS approved the plan to use the Eclipse compiler as the new gcj front end. Work is being done on the `gcj-eclipse` branch; it can already build libgcj. This project will allow us to ship a 1.5 compiler in the relatively near future. The old `gcjx` branch and project is now dead.
May 20, 2006
:   John David Anglin and Andreas Tobler checked in a [patch](http://gcc.gnu.org/ml/gcc-patches/2006-05/msg01004.html) to enable libjava to be built by default on the HP-UX PA 32-bit platform.
May 19, 2006
:   Mark Wielaard has imported [GNU Classpath](http://www.gnu.org/software/classpath/) 0.91 ([release notes](http://www.gnu.org/software/classpath/announce/20060515.html)) into GCJ.
April 26, 2006
:   Andrew Haley has merged in a [patch](http://gcc.gnu.org/ml/java-patches/2006-q2/msg00100.html) that eliminates conservative scanning of static data and considerably speeds up garbage collection in many cases (for example, halving the time spent in garbage collection when running [Eclipse](http://www.eclipse.org/)).
April 17, 2006
:   Tom Tromey has [announced](http://gcc.gnu.org/ml/java/2006-04/msg00046.html) the availability of two experimental Just-In-Time (JIT) interpreters for GCJ based on [libjit](http://freecode.com/projects/libjit) and [LLVM](http://llvm.org/) respectively. The source code for these JITs is available via the "gcj-jit" module of the [rhug](http://sourceware.org/rhug/) repository.
April 4, 2006
:   `fastjar` has been removed from the GCC source tree and made into [a separate project](http://savannah.nongnu.org/projects/fastjar). The GCJ build process now requires an external tool for creating JARs - it searches for a tool named either `jar` or `fastjar` during configuration and reports an error if it is unable to find such a tool.
March 15, 2006
:   David Daney has merged in a [patch](http://gcc.gnu.org/ml/java-patches/2006-q1/msg00289.html) that adds the new `-freduced-reflection` option to GCJ. This option prevents GCJ from generating most of the reflection metadata for classes resulting in smaller binaries. There are some caveats to using this option though - please read the GCJ manual for the implications of using this option.
March 9, 2006
:   Mark Wielaard has imported [GNU Classpath](http://www.gnu.org/software/classpath/) 0.90 ([release notes](http://www.gnu.org/software/classpath/announce/20060306.html)) into GCJ.
February 8, 2006
:   Mark Wielaard has written "[A look at GCJ 4.1](http://lwn.net/Articles/171139/)" for LWN.net.

[Less recent GCJ news](news.html)
