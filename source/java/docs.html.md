# GCJ Documentation

## General documentation

-   [GCJ Manual](http://gcc.gnu.org/onlinedocs/gcj/).
-   [API Documentation](http://developer.classpath.org/doc/). (This is actually the API documentation for GNU Classpath, which is essentially identical to libgcj. However, there may be differences between the libgcj release you have and the version of Classpath used to generate this page.)
-   General [gcj](gcj2.html) information.
-   General [runtime](libgcj2.html) information.
-   How to [compile](compile.html) a Java program.
-   How to [debug](gdb.html) a Java program using GDB.
-   [Frequently Asked Questions](faq.html).
-   How to [contribute](contrib.html).
-   [CNI Documentation](http://gcc.gnu.org/onlinedocs/gcj/About-CNI.html).

## Porting the GCJ runtime

We've written three documents on how to port various components of GCJ runtime, libgcj, to different target systems.

-   How to [port](port-threads.html) the thread layer.
-   How to [port](port-files.html) the file handling layer.
-   How to [port](port-signals.html) the signal handling layer.

## Papers describing our work

The engineers on this project have written a number of papers for conferences and magazines.

-   **January 2003**: "Compiling Java with GCJ" is an overview article that appeared in the Linux Journal.
    [[HTML](http://www.linuxjournal.com/article/4860)]
    Author: [Per Bothner](http://www.bothner.com/).
-   **February 1999**: "The Cygnus Native Interface for C++/Java Integration".
    [[HTML](papers/cni/t1.html)] [[SGML/Docbook source](papers/cni.sgml)]
    This is a programmer's manual which explains how to use a Java-aware C++ compiler (G++) to write native methods for Gcj.
    The CNI design and ideas were proposed in "Writing native Java methods in natural C++" (November 1997).
-   **August 1998**: "No Silver Bullet - Garbage Collection for Java in Embedded Systems".
    [[HTML](papers/nosb.html)] [[PS for presentation slides](papers/nosb.ps.gz)]
    Author: Alex Petit-Bianco.
-   **November 1997**: "Writing native Java methods in natural C++" discusses interaction between Java and C++. This is an early design document; see "The Cygnus Native Interface for C++/Java Integration" (February 1998) for the programmer's manual.
    [[HTML](papers/native++.html)] [[SGML/Docbook source](papers/native++.sgml)]
    Author: [Per Bothner](http://www.bothner.com/).
-   **October 1997**: "Compiling Java for Embedded Systems" was presented by Per Bothner at the Embedded Systems Conference West in San Jose. It provides a technical overview of our strategy and plans at the time. This is fairly out of date now.
    [[PDF](papers/compjava.pdf)] [[gzip'd Postscript](papers/compjava.ps.gz)] [[SGML/Docbook source](papers/esc97.sgml)] [[PS for presentation slides](papers/esc97w-slides.ps.gz)].
    Author: [Per Bothner](http://www.bothner.com/).
-   **February 1997**: "A Gcc-based Java Implementation" is an older paper presented by Per Bothner at IEEE Compcon.
    [[extended abstract (HTML)](papers/gcc-java.html)] [[complete paper (Postscript)](papers/gcc-java.ps.gz)] [([slides for presentation](papers/compcon97.ps.gz))]
    Author: [Per Bothner](http://www.bothner.com/).

