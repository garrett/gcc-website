# GCJ Status

Status of GCJ as of GCC 3.2. Improvements that are only in current development versions are marked as "in CVS".

## Core Features {#features}

-   Compile Java source code ("ahead-of-time") to native (machine) code,
-   Compile Java bytecode (`.class` files) to native (machine) code,
-   Compile Java source code to `.class` files (`javac` replacement).
-   A byte-code interpreter, allowing support for `ClassLoader`s, and dynamically loaded classes. Corresponds to JDK's `java` command.
-   Support for JNI, as well as CNI, a more efficient and easier-to-use (though non-standard) API for writing Java methods in C++.
-   Verification, both at compile time (compiling classes to native) and run-time (loading classes). Both miss some tests, and you should not (yet) rely on their correctness for security.
-   A "conservative" garbage collector.
-   Replacements for the `jar`, `javah`, `rmic`, and `rmiregistry` programs.
-   An extensive class library - see below.

## Implemented Packages {#packages}

java.applet
:   Believed to be complete, but note that without a functional AWT it isn't very useful.
java.awt
:   A lot of code exists, but not enough for use in real applications.
java.beans
:   Believed to be functional and complete, should be compatible with JDK 1.4.
java.io
:   Ok.
java.lang
:   Ok.
java.lang.ref
:   Ok.
java.lang.reflect
:   Ok. Does not check access permissions.
java.math
:   Ok.
java.net
:   Ok.
java.nio
:   The public interface is ready, but the implementation is not working yet.
java.rmi
:   Ok.
java.security
:   Code exist; completeness unknown.
java.sql
:   Ok, should be compatible with JDK 1.4.
java.util
:   Ok.
java.util.jar
:   Ok.
java.util.regex
:   Ok.
java.util.zip
:   Ok.
java.text
:   Ok, but most localization data not available.
javax.accessibility
:   Some code; status unknown.
javax.crypto
:   We recommend using [GNU Crypto](http://www.gnu.org/software/gnu-crypto/).
javax.naming
:   Complete, but no providers written.
javax.sql
:   Some code; status unknown.
javax.swing
:   Some code, but not enough for real applications.
javax.transaction
:   Complete, but no providers written.

You can also see [a comparison of our classes with Classpath's](http://gcc.gnu.org/java/libgcj-classpath-compare.html). Differences here are merged from time to time. You can also see [a comparison of the GUI branch with Classpath](http://gcc.gnu.org/java/gui-compare/libgcj-classpath-compare.html).

## Supported Targets {#targets}

GNU/Linux on the Pentium-compatible PCs (`i[56]86-pc-linux-gnu`)
:   Ok.
FreeBSD on the Pentium-compatible PCs (`i[56]86-pc-freebsd*`)
:   Ok.
GNU/Linux on Alpha (`alpha*-*-linux-gnu`)
:   Ok.
GNU/Linux on the Itanium (ia64) architecture (`ia64-*-linux-gnu`)
:   Ok.
GNU/Linux on PowerPC
:   Ok.
GNU/Linux on AMD x86-64 ("Hammer") architecture (`x86_64-*-linux-gnu`)
:   Ok, in CVS (but building with multilibs enabled needs libtool patch).
Solaris 2.5.1, 2.6, 2.7, 2.8 on SPARC (`sparc*-sun-solaris2.[5678]`)
:   Ok for 32- and 64-bit ABIs. Byte-code interpreter not currently supported.
SGI Irix 6.5 on Mips (`mips-sgi-irix6.5`)
:   Some tricks required to build. Byte-code interpreter not currently supported. No SEGV handler available.
Windows on the Intel PC platform, using the MingW32 compiler (`i[56]86-pc-mingw32`)
:   Works for target; works for host (in CVS). Is incomplete.
PPC Darwin (including MacOS X)
:   Some kludges needed.
DEC OSF 4.0f and OSF 5.1 on Alpha (`alpha-dec-osf4.0f`, `alpha-dec-osf5.1`)
:   Ok.
Bare metal ARM ELF using newlib (`arm-elf`)
:   Ok, in CVS. No threads, file I/O or networking.
Bare metal XScale ELF using newlib (`xscale-elf`)
:   Ok. No threads, file I/O or networking.
IBM s390x
:   Ok, in CVS.
GNU/Linux on Hitachi SH-3/4 micro-controller (`sh[34]-*-linux`)
:   Ok, in CVS.

