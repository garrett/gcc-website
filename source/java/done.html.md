# Done with GCJ

This page provides descriptions and links on projects that are using the GCJ runtime and compiler. If you would like your project to be listed here or know about a particular project, please write to the <java@gcc.gnu.org> list.

Entries are listed in chronological order.

Project

Description

Contact

**iRATE radio**

iRATE radio is a popular client/server mp3 player/downloader featuring collaborative filtering: you rate the tracks and it uses your ratings and other people's to guess what you'll like. iRATE uses GCJ together with the SWT toolkit to provide native binaries on multiple platforms, including Windows and GNU/Linux. iRATE has thousands of users, many of which use iRATE daily, making it perhaps the most widely distributed GCJ-compiled program to date.

[Taras](mailto:taras.judge@shaw.ca)

**[XWT](http://www.xwt.org)**

XWT is the XML Windowing Toolkit. It lets you write remote applications -- applications that run on a server, yet can "project" their user interface onto any computer, anywhere on the Internet. When built with gcj, it calls directly into the host's graphic routines and can be turned into, for example, an ActiveX control.

[Adam Megacz](mailto:gcj@lists.megacz.com)

**Autonomous Haulage**

Modular Mining System is using gcj on an embedded StrongARM processor to control autonomous trucks in mining haulage systems. Note that they're not using libgcj, but rather an in-house runtime that they wrote.

**[jUSB](http://jusb.sourceforge.net)/[jPhoto](http://jphoto.sourceforge.net)**

jUSB is an open source Java API for USB, with an implementation that runs on recent GNU/Linux OS distributions. jPhoto provides basic command line and library support for the digital camera protocol on top of jUSB. The protocol is the new USB standard for talking to such devices called PTP.

[David Brownell](mailto:david-b@pacbell.net)

**PAL**

PAL is a Java library for use in molecular evolution and phylogenetics. Basically, PAL includes classes and methods for computationally intensive statistics that are useful for the analysis of DNA sequences.
 PAL 1.0 consists of 111 classes in 12 packages with more than 20,000 lines of code, all of which compiles nicely into native code using GCJ.

[Korbinian Strimmer](mailto:strimmer@stat.uni-muenchen.de)

**[Swarm](http://www.swarm.org)**

Swarm is a fine-grained distributed discrete event simulator. Swarm is mainly used for agent-based (bottom-up) modeling. Swarm is not written in Java, but it has a JNI layer. It was done with JNI at first for portability. And, since the simulator is not implemented in C++, it wasn't expected to see major benefits from CNI... The tested configuration relies on Kaffe as equipped with gcj support. On Intel Debian 2.2, up to 30% performance gains over Kaffe's jit3 were witnessed.

[Marcus G. Daniel](mailto:mgd@swarm.org).

**[Tamanoir](http://www.ens-lyon.fr/LIP/RESO/Tamanoir/)**

Tamanoir system is a complete framework that allows users to easily deploy and maintain distributed active routers on wide area networks. In the rapidly expanding field of active and programmable networks, Tamanoir is designed around a new kind of architecture dedicated to high performance active networking. Its implementation features a multi-threading approach to combine performance and portability of services. It's written in Java and standardly built using gcj.

[Jean Patrick Gelas](mailto:jpgelas@lhpcm.univ-lyon1.fr)

**Fluid Dynamics**

Alejandro Rodríguez Gallego, at ICAI University (Spain) has spent two years developing a Java program to solve fluid dynamics. He says: I have recently tested my program with GCJ and I have got an incredible performance increase over HotSpot v2 (JDK 1.3 for linux). -50% RAM were needed and execution time reduced in -15%.
 GCJ will be the main compiler for my program.

**[rhug](http://sourceware.org/rhug/)**

Anthony Green and friends have set up rhug, a collection of free software Java packages set up to build with gcj. Both sources and RPMs are available.

[Anthony Green](mailto:green@redhat.com)

**WebMiel**

Christophe Roux has been working on software for community communications over http. This program, WebMiel, uses Tomcat and has been working with gcj since mid-2002. The documentation is in French.

[Christophe Roux](mailto:ch_roux@club-internet.fr)

**Freestyler Toolkit**

The Freestyler Toolkit facilitates building with GCJ. It focuses on simplifying the construction of SWT Graphical User Interfaces and XmlRpc servants that sit before MySQL database backends.

[Erik Poupaert](mailto:erik.poupaert@skynet.be)

**Rimfaxe Web Server**

Rimfaxe is a web server featuring compilation of jsp pages to .so libraries with gcj, SEDA-based architecture with non-blocking I/O, servlet 2.3 / JSP 1.2 implementation including filters and taglibs, and caching of static content.

[Lars Andersen](mailto:lars@rimfaxe.com)

**[JGachine](http://jgachine.berlios.de)**

JGachine is a networked game engine for 2D multi-player (networked) games. JGachine is written in Java and C++. The games themselves are written in pure Java and can be loaded via network.

[Jens Thiele](mailto:karme@berlios.de)

**[pdftk](http://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/)**

If PDF is electronic paper, then pdftk is an electronic staple-remover, hole-punch, binder, secret-decoder-ring, and X-Ray-glasses. Pdftk is a useful tool for handling PDF documents. Every PDF user should have one in the top drawer of his/her desktop.

Pdftk is also an example of how to use a library of Java classes in a stand-alone C++ program. Specifically, it demonstrates how GCJ and CNI allows C++ code to use iText's (itext-paulo) Java classes.

[Sid Steward](mailto:ssteward@accesspdf.com)

**[gcjwebplugin](http://www.nongnu.org/gcjwebplugin/)**

gcjwebplugin is a web browser plugin to execute Java (tm) applets. It supports Firefox, Galeon, Mozilla, Opera and many more browsers.

[Michael Koch](mailto:konqueror@gmx.de)

**[H2 Database Engine](http://www.h2database.com/)**

H2 is a Java database engine. Apart from the usual features such as SQL, transactions, JDBC API, referential integrity, the database also supports clustering, views, subqueries, encryption, trigger and stored procedures. As HSQLDB, database can be operated in the embedded and server mode, with data kept in-memory or on disk. It is available as a native executable (using GCJ) and as a Java library.

[H2 maintainers](mailto:dbsupport@h2database.com)
