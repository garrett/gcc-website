# Contributing to the GCJ Project

The [main GCC contribute page](../contribute.html) contains general information; below you will find GCJ-specific details.

## Project Ideas

We've started maintaining a list of [useful projects](projects.html).

## Coding Standards

We follow the [GCC Coding Conventions](../codingconventions.html) with some extrapolations concerning Java programming style:

-   Open braces go on a new line (like GNU C style).
-   Two space indent for methods and inner classes
-   `if`, `for`, `try`, and `synchronized` blocks are indented four spaces (two for the braces, and an additional two for the code).
-   The open brace for a method body is indented only as far as the method header.

## Submitting Patches

Send libgcj patches to <java-patches@gcc.gnu.org>. This list is [archived](http://gcc.gnu.org/ml/java-patches/).

Patches to the GCJ front end — the `gcc/java` directory — should be sent to the [gcc-patches](mailto:gcc-patches@gcc.gnu.org) list.

## Testing

We're accepting test case patches.

-   Put the `.java` file in `libjava/testsuite/libjava.compile` (if it is an expected compiler failure or other compile-time test -- run-time tests usually go in `libjava.lang`)
-   If the file is "`foo.java`" make a "`foo.xfail`" file. For an expected failure the only thing in there should be "`shouldfail`"
-   If the test case matches a Java PR, it is customary to name it after the PR number.
-   Send the patch to java-patches and then commit of you're allowed to.

More detailed documentation on [how to run tests](http://gcc.gnu.org/install/test.html) is included in the GCC installation notes.
