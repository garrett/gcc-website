# The libgcj home page

## What is it?

\`\`libgcj'' is the runtime that goes along with the [gcj](gcj2.html) front end to GCC. libgcj includes parts of the Java Class Libraries, plus glue to connect the libraries to the compiler and the underlying OS.

## What you get

libgcj eventually builds a couple of libraries (one for the runtime and one for the garbage collector), a \`\`zip'' version of the class libraries, and a program called \`\`jv-convert'' which can be used to do character encoding transformations.

## What is missing?

The runtime is not yet fully complete. Parts of the standard class libraries are missing (most notably AWT), though much of the equivalent of JDK 1.2 has been supported. Also, the bytecode interpreter available only on certain platforms.

## How to build it

Just follow the directions for [building](build-snapshot.html) GCJ.

There are a few options you might consider passing to \`\`configure'':

--enable-java-gc=TYPE
:   Enables the garbage collector. The default type is \`\`boehm'', for the Boehm conservative GC.
--enable-fast-character
:   libgcj includes two implementations of java.lang.Character. The default implementation is small, but slow. This option will enable the faster, larger code.
--enable-threads=TYPE
:   libgcj includes a retargetable thread layer. The value of this option must be the same as the value used when building gcj itself. By default, no threads will be used (this is a crummy default, but we just follow GCC here). The only supported values are \`\`none'' (for no threads) and \`\`posix'' (for POSIX threads).

