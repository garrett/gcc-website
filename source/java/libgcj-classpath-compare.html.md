# libgcj -vs- Classpath (No GUI Classes)

This page compares the "current" cvs libgcj against the "current" cvs Classpath. It was generated using the `gen-classpath-compare` script (available in gcc cvs repository in `wwwdocs/bin/`) on Wed Dec 1 15:26:19 UTC 2004

Only lists non-gui files (NO AWT, Swing, or gtk-peer)

This table intentionally omits certain classes which are not of interest. If the second column shows a "Diff" link, then that means the script believes that the class has been merged, but a difference has been reintroduced. *Note that such differences cannot be automatically merged.* Any merging must be done manually; some differences are currently required.

|Class|Merge Status|
|:----|:-----------|
|gnu.java.beans.IntrospectionIncubator|[Diff](compare/gnu.java.beans.IntrospectionIncubator.diff)|
|gnu.java.io.Base64InputStream|[Diff](compare/gnu.java.io.Base64InputStream.diff)|
|gnu.java.io.EncodingManager|Classpath-only|
|gnu.java.io.PlatformHelper|Classpath-only|
|gnu.java.lang.CharData|[Diff](compare/gnu.java.lang.CharData.diff)|
|gnu.java.lang.MainThread|[Diff](compare/gnu.java.lang.MainThread.diff)|
|gnu.java.lang.SystemClassLoader|Classpath-only|
|gnu.java.locale.LocaleInformation\_de|[Diff](compare/gnu.java.locale.LocaleInformation_de.diff)|
|gnu.java.net.BASE64|Classpath-only|
|gnu.java.net.CRLFInputStream|Classpath-only|
|gnu.java.net.CRLFOutputStream|Classpath-only|
|gnu.java.net.DefaultContentHandlerFactory|GCJ-only|
|gnu.java.net.EmptyX509TrustManager|Classpath-only|
|gnu.java.net.GetLocalHostAction|Classpath-only|
|gnu.java.net.GetSystemPropertyAction|Classpath-only|
|gnu.java.net.LineInputStream|Classpath-only|
|gnu.java.net.PlainDatagramSocketImpl|[Diff](compare/gnu.java.net.PlainDatagramSocketImpl.diff)|
|gnu.java.net.PlainSocketImpl|[Diff](compare/gnu.java.net.PlainSocketImpl.diff)|
|gnu.java.net.protocol.core.Connection|GCJ-specific|
|gnu.java.net.protocol.core.CoreInputStream|GCJ-specific|
|gnu.java.net.protocol.core.Handler|GCJ-specific|
|gnu.java.net.protocol.ftp.ActiveModeDTP|Classpath-only|
|gnu.java.net.protocol.ftp.BlockInputStream|Classpath-only|
|gnu.java.net.protocol.ftp.BlockOutputStream|Classpath-only|
|gnu.java.net.protocol.ftp.CompressedInputStream|Classpath-only|
|gnu.java.net.protocol.ftp.CompressedOutputStream|Classpath-only|
|gnu.java.net.protocol.ftp.DTP|Classpath-only|
|gnu.java.net.protocol.ftp.DTPInputStream|Classpath-only|
|gnu.java.net.protocol.ftp.DTPOutputStream|Classpath-only|
|gnu.java.net.protocol.ftp.FTPConnection|Classpath-only|
|gnu.java.net.protocol.ftp.FTPException|Classpath-only|
|gnu.java.net.protocol.ftp.FTPResponse|Classpath-only|
|gnu.java.net.protocol.ftp.FTPURLConnection|Classpath-only|
|gnu.java.net.protocol.ftp.Handler|Classpath-only|
|gnu.java.net.protocol.ftp.PassiveModeDTP|Classpath-only|
|gnu.java.net.protocol.ftp.StreamInputStream|Classpath-only|
|gnu.java.net.protocol.ftp.StreamOutputStream|Classpath-only|
|gnu.java.net.protocol.gcjlib.Connection|GCJ-specific|
|gnu.java.net.protocol.gcjlib.Handler|GCJ-specific|
|gnu.java.net.protocol.http.Authenticator|Classpath-only|
|gnu.java.net.protocol.http.ByteArrayRequestBodyWriter|Classpath-only|
|gnu.java.net.protocol.http.ByteArrayResponseBodyReader|Classpath-only|
|gnu.java.net.protocol.http.ChunkedInputStream|Classpath-only|
|gnu.java.net.protocol.http.Connection|GCJ-only|
|gnu.java.net.protocol.http.Cookie|Classpath-only|
|gnu.java.net.protocol.http.CookieManager|Classpath-only|
|gnu.java.net.protocol.http.Credentials|Classpath-only|
|gnu.java.net.protocol.http.HTTPConnection|Classpath-only|
|gnu.java.net.protocol.http.HTTPDateFormat|Classpath-only|
|gnu.java.net.protocol.http.HTTPURLConnection|Classpath-only|
|gnu.java.net.protocol.http.Handler|[Diff](compare/gnu.java.net.protocol.http.Handler.diff)|
|gnu.java.net.protocol.http.Headers|Classpath-only|
|gnu.java.net.protocol.http.Request|Classpath-only|
|gnu.java.net.protocol.http.RequestBodyWriter|Classpath-only|
|gnu.java.net.protocol.http.Response|Classpath-only|
|gnu.java.net.protocol.http.ResponseBodyReader|Classpath-only|
|gnu.java.net.protocol.http.ResponseHeaderHandler|Classpath-only|
|gnu.java.net.protocol.http.SimpleCookieManager|Classpath-only|
|gnu.java.net.protocol.http.event.ConnectionEvent|Classpath-only|
|gnu.java.net.protocol.http.event.ConnectionListener|Classpath-only|
|gnu.java.net.protocol.http.event.RequestEvent|Classpath-only|
|gnu.java.net.protocol.http.event.RequestListener|Classpath-only|
|gnu.java.net.protocol.jar.Connection|[Diff](compare/gnu.java.net.protocol.jar.Connection.diff)|
|gnu.java.nio.NIOServerSocket|[Diff](compare/gnu.java.nio.NIOServerSocket.diff)|
|gnu.java.nio.VMPipe|VM-specific|
|gnu.java.nio.VMSelector|VM-specific|
|gnu.java.nio.channels.FileChannelImpl|[Diff](compare/gnu.java.nio.channels.FileChannelImpl.diff)|
|gnu.java.security.PolicyFile|No|
|gnu.java.security.ber.BER|Classpath-only|
|gnu.java.security.ber.BEREncodingException|Classpath-only|
|gnu.java.security.ber.BERReader|Classpath-only|
|gnu.java.security.ber.BERValue|Classpath-only|
|gnu.java.security.der.DERReader|[Diff](compare/gnu.java.security.der.DERReader.diff)|
|gnu.java.security.pkcs.PKCS7SignedData|Classpath-only|
|gnu.java.security.pkcs.SignerInfo|Classpath-only|
|gnu.java.security.provider.GnuDSAPrivateKey|[Diff](compare/gnu.java.security.provider.GnuDSAPrivateKey.diff)|
|gnu.java.security.provider.GnuDSAPublicKey|[Diff](compare/gnu.java.security.provider.GnuDSAPublicKey.diff)|
|gnu.javax.rmi.CORBA.DelegateFactory|Classpath-only|
|gnu.javax.rmi.CORBA.GetDelegateInstanceException|Classpath-only|
|gnu.javax.rmi.CORBA.PortableRemoteObjectDelegateImpl|Classpath-only|
|gnu.javax.rmi.CORBA.StubDelegateImpl|Classpath-only|
|gnu.javax.rmi.CORBA.UtilDelegateImpl|Classpath-only|
|gnu.javax.rmi.CORBA.ValueHandlerImpl|Classpath-only|
|gnu.javax.rmi.PortableServer|Classpath-only|
|java.applet.Applet|[Diff](compare/java.applet.Applet.diff)|
|java.io.BufferedInputStream|[Diff](compare/java.io.BufferedInputStream.diff)|
|java.io.DeleteFileHelper|Classpath-only|
|java.io.File|[Diff](compare/java.io.File.diff)|
|java.io.InputStreamReader|[Diff](compare/java.io.InputStreamReader.diff)|
|java.io.ObjectInputStream|[Diff](compare/java.io.ObjectInputStream.diff)|
|java.io.ObjectOutputStream|[Diff](compare/java.io.ObjectOutputStream.diff)|
|java.io.ObjectStreamField|[Diff](compare/java.io.ObjectStreamField.diff)|
|java.io.OutputStreamWriter|[Diff](compare/java.io.OutputStreamWriter.diff)|
|java.io.PrintStream|[Diff](compare/java.io.PrintStream.diff)|
|java.io.VMObjectStreamClass|VM-specific|
|java.lang.Character|[Diff](compare/java.lang.Character.diff)|
|java.lang.Class|[Diff](compare/java.lang.Class.diff)|
|java.lang.ClassLoader|[Diff](compare/java.lang.ClassLoader.diff)|
|java.lang.Double|[Diff](compare/java.lang.Double.diff)|
|java.lang.EcosProcess|GCJ-specific|
|java.lang.Float|[Diff](compare/java.lang.Float.diff)|
|java.lang.Object|[Diff](compare/java.lang.Object.diff)|
|java.lang.PosixProcess|GCJ-specific|
|java.lang.Runtime|[Diff](compare/java.lang.Runtime.diff)|
|java.lang.SecurityManager|[Diff](compare/java.lang.SecurityManager.diff)|
|java.lang.String|[Diff](compare/java.lang.String.diff)|
|java.lang.StringBuffer|[Expected diff changed](compare/java.lang.StringBuffer.diff)|
|java.lang.System|[Diff](compare/java.lang.System.diff)|
|java.lang.Thread|[Diff](compare/java.lang.Thread.diff)|
|java.lang.VMClassLoader|VM-specific|
|java.lang.VMCompiler|VM-specific|
|java.lang.VMSecurityManager|VM-specific|
|java.lang.VMThrowable|VM-specific|
|java.lang.Win32Process|GCJ-specific|
|java.lang.ref.Reference|[Expected diff changed](compare/java.lang.ref.Reference.diff)|
|java.lang.reflect.Array|[Expected diff](compare/java.lang.reflect.Array.diff)|
|java.lang.reflect.Constructor|VM-specific|
|java.lang.reflect.Field|VM-specific|
|java.lang.reflect.Method|VM-specific|
|java.lang.reflect.Modifier|[Expected diff](compare/java.lang.reflect.Modifier.diff)|
|java.net.InetAddress|[Diff](compare/java.net.InetAddress.diff)|
|java.net.JarURLConnection|[Diff](compare/java.net.JarURLConnection.diff)|
|java.net.MimeTypeMapper|Classpath-only|
|java.net.URL|[Expected diff changed](compare/java.net.URL.diff)|
|java.net.URLClassLoader|[Diff](compare/java.net.URLClassLoader.diff)|
|java.net.URLConnection|[Diff](compare/java.net.URLConnection.diff)|
|java.nio.DirectByteBufferImpl|[Expected diff changed](compare/java.nio.DirectByteBufferImpl.diff)|
|java.nio.MappedByteBuffer|[Diff](compare/java.nio.MappedByteBuffer.diff)|
|java.nio.MappedByteBufferImpl|[Expected diff changed](compare/java.nio.MappedByteBufferImpl.diff)|
|java.nio.VMDirectByteBuffer|VM-specific|
|java.nio.charset.Charset|[Diff](compare/java.nio.charset.Charset.diff)|
|java.security.AccessControlContext|[Diff](compare/java.security.AccessControlContext.diff)|
|java.security.AccessController|[Diff](compare/java.security.AccessController.diff)|
|java.security.BasicPermission|[Diff](compare/java.security.BasicPermission.diff)|
|java.security.IntersectingDomainCombiner|Classpath-only|
|java.security.Security|[Diff](compare/java.security.Security.diff)|
|java.security.cert.X509Certificate|[Diff](compare/java.security.cert.X509Certificate.diff)|
|java.text.Collator|[Diff](compare/java.text.Collator.diff)|
|java.util.Calendar|[Diff](compare/java.util.Calendar.diff)|
|java.util.Date|[Diff](compare/java.util.Date.diff)|
|java.util.GregorianCalendar|[Diff](compare/java.util.GregorianCalendar.diff)|
|java.util.ResourceBundle|[Diff](compare/java.util.ResourceBundle.diff)|
|java.util.VMTimeZone|VM-specific|
|java.util.jar.JarFile|[Diff](compare/java.util.jar.JarFile.diff)|
|java.util.zip.Deflater|[Diff](compare/java.util.zip.Deflater.diff)|
|java.util.zip.DeflaterConstants|Classpath-only|
|java.util.zip.DeflaterEngine|Classpath-only|
|java.util.zip.DeflaterHuffman|Classpath-only|
|java.util.zip.DeflaterOutputStream|[Diff](compare/java.util.zip.DeflaterOutputStream.diff)|
|java.util.zip.DeflaterPending|Classpath-only|
|java.util.zip.GZIPInputStream|[Diff](compare/java.util.zip.GZIPInputStream.diff)|
|java.util.zip.GZIPOutputStream|[Diff](compare/java.util.zip.GZIPOutputStream.diff)|
|java.util.zip.Inflater|[Diff](compare/java.util.zip.Inflater.diff)|
|java.util.zip.InflaterDynHeader|Classpath-only|
|java.util.zip.InflaterHuffmanTree|Classpath-only|
|java.util.zip.InflaterInputStream|[Diff](compare/java.util.zip.InflaterInputStream.diff)|
|java.util.zip.OutputWindow|Classpath-only|
|java.util.zip.PendingBuffer|Classpath-only|
|java.util.zip.StreamManipulator|Classpath-only|
|javax.rmi.BAD\_OPERATION|Classpath-only|
|javax.rmi.CORBA.ClassDesc|Classpath-only|
|javax.rmi.CORBA.ObjectImpl|Classpath-only|
|javax.rmi.CORBA.PortableRemoteObjectDelegate|Classpath-only|
|javax.rmi.CORBA.Stub|Classpath-only|
|javax.rmi.CORBA.StubDelegate|Classpath-only|
|javax.rmi.CORBA.SystemException|Classpath-only|
|javax.rmi.CORBA.Tie|Classpath-only|
|javax.rmi.CORBA.Util|Classpath-only|
|javax.rmi.CORBA.UtilDelegate|Classpath-only|
|javax.rmi.CORBA.ValueHandler|Classpath-only|
|javax.rmi.ORB|Classpath-only|
|javax.rmi.PortableRemoteObject|Classpath-only|


