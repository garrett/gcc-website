# libgcj -vs- Classpath (GUI Classes only)

This page compares the "current" cvs libgcj against the "current" cvs Classpath. It was generated using the `gen-classpath-compare` script (available in gcc cvs repository in `wwwdocs/bin/`) on Wed Dec 1 15:26:55 UTC 2004

Only lists gui files (AWT, Swing, and gtk-peer)

This table intentionally omits certain classes which are not of interest. If the second column shows a "Diff" link, then that means the script believes that the class has been merged, but a difference has been reintroduced. *Note that such differences cannot be automatically merged.* Any merging must be done manually; some differences are currently required.

|Class|Merge Status|
|:----|:-----------|
|gnu.java.awt.peer.gtk.GdkGraphics|[Diff](compare/gnu.java.awt.peer.gtk.GdkGraphics.diff)|
|gnu.java.awt.peer.gtk.GdkGraphics2D|[Diff](compare/gnu.java.awt.peer.gtk.GdkGraphics2D.diff)|
|gnu.java.awt.peer.gtk.GtkComponentPeer|[Diff](compare/gnu.java.awt.peer.gtk.GtkComponentPeer.diff)|
|gnu.java.awt.peer.gtk.GtkFramePeer|[Diff](compare/gnu.java.awt.peer.gtk.GtkFramePeer.diff)|
|gnu.java.awt.peer.gtk.GtkImage|[Diff](compare/gnu.java.awt.peer.gtk.GtkImage.diff)|
|gnu.java.awt.peer.gtk.GtkImagePainter|[Diff](compare/gnu.java.awt.peer.gtk.GtkImagePainter.diff)|
|gnu.java.awt.peer.gtk.Test|GCJ-only|
|gnu.java.awt.peer.gtk.TestAWT|GCJ-only|
|java.awt.Component|[Diff](compare/java.awt.Component.diff)|
|java.awt.Window|[Diff](compare/java.awt.Window.diff)|
|java.awt.image.MemoryImageSource|[Diff](compare/java.awt.image.MemoryImageSource.diff)|
|java.awt.image.PixelGrabber|[Diff](compare/java.awt.image.PixelGrabber.diff)|
|jni/gtk-peer/gnu\_java\_awt\_peer\_gtk\_GdkGraphics.c|[Diff](compare/jni.gtk-peer.gnu_java_awt_peer_gtk_GdkGraphics.c.diff)|
|jni/gtk-peer/gnu\_java\_awt\_peer\_gtk\_GtkImagePainter.c|[Diff](compare/jni.gtk-peer.gnu_java_awt_peer_gtk_GtkImagePainter.c.diff)|


