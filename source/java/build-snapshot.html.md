# How to build and run libgcj/gcj snapshots or checkouts

<table>
<col width="100%" />
<tbody>
<tr class="odd">
<td align="left"><pre><code>1. Get a GCC snapshot or check out the sources.</code></pre></td>
</tr>
<tr class="even">
<td align="left"><pre><code>2. Make a compile directory

  $ mkdir compile</code></pre></td>
</tr>
<tr class="odd">
<td align="left"><pre><code>3. Move the snapshot into the compile dir, e.g.

  $ cd compile
  $ mv ../gcc-20001211.tar.gz .
  $ gunzip *.gz
  $ tar xfv *.tar
  $ ln -s gcc-20001211 gcc</code></pre></td>
</tr>
<tr class="even">
<td align="left"><pre><code>4. Tell the build system you want to build libgcj

  Have a look at the toplevel configure.in (gcc/configure.in) and
  make sure that the variable `noconfigdirs&#39; isn&#39;t assigned to
  something (like target-libjava or ${libgcj}.)

  Also, check for platform-specific assignments of `noconfigdirs&#39; with
  ${libgcj}; if ${libgcj} is listed for your platform, remove it before
  configuring.</code></pre></td>
</tr>
<tr class="odd">
<td align="left"><pre><code>5. Compile and install gcc/gcj/libgcj

  $ cd compile
  $ mkdir objdir
  $ cd objdir
  $ ../gcc/configure --enable-threads=posix --prefix=/home/joerg/gcc \
    --enable-shared --enable-languages=c++,java \
    --with-as=/opt/gnu/bin/as --with-ld=/opt/gnu/bin/ld
  $ make bootstrap
  $ make
  $ make install

The Boehm GC is the collector picked up by default.

If you compile under GNU/Linux you could omit the last two options. Under
Solaris you&#39;ll need them. If you omit &#39;--prefix&#39; the compiled source
will be installed under /usr/local. For more information about
installing gcc and/or configuration options see:

  http://gcc.gnu.org/install/
</code></pre></td>
</tr>
<tr class="even">
<td align="left"><pre><code>6. Adjust your environment

Reflect your choice of --prefix value to your environment. For csh
compatible shells, this would be something like the following:

setenv PATH /home/joerg/gcc/bin:$PATH
setenv LD_LIBRARY_PATH /home/joerg/gcc/lib</code></pre></td>
</tr>
<tr class="odd">
<td align="left"><pre><code>7. Edit a file HelloWorld.java

  public class HelloWorld {
    public static void main(String [] args) {
      System.out.println(&quot;Hello&quot;);
    }
  }</code></pre></td>
</tr>
<tr class="even">
<td align="left"><pre><code>8. Compile and run HelloWorld

 $ gcj --main=HelloWorld -o HelloWorld HelloWorld.java
 $ ./HelloWorld</code></pre></td>
</tr>
</tbody>
</table>


