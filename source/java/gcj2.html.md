# The GCJ home page

## What is it?

We've written a front end to the GCC compiler which can natively compile both Java^[tm](#javatm)^ source and bytecode files. The compiler can also generate class files. This new front end is integrated into the [GCC](http://gcc.gnu.org/) project.

## What you get

The currently available code consists of several programs:

`gcj`
:   A front end to gcc which is able to read Java \`\`.class'' files and generate assembly code. gcj is also a convenient front end to jvgenmain. Finally, gcj can read \`\`.java'' files and generate assembly code or Java bytecode.
`jvgenmain`
:   A small program to generate an appropriate \`\`main'' for a Java class.
`gcjh`
:   A program to generate C++ header files corresponding to Java .class files.
`jcf-dump`
:   Reads a \`\`.class'' file and prints out all sorts of useful information.
`jv-scan`
:   Reads a \`\`.java'' file and prints some useful information. For instance, it can tell you which classes are defined in that file.

## Making executables

In order to make full executables, you'll need to link the output of gcj with the appropriate runtime code. See [the libgcj page](libgcj2.html) for details on the runtime. Note that you'll want to configure GCC to build libjava; see [configuration and build instructions for GCJ](build-snapshot.html).

There are also [more detailed instructions](compile.html) on compiling Java programs.

## How to get it

The new Java front end is very easy to download and install. Since it is it fully integrated into GCC, you can simply follow the GCC download and build instructions. Note that you'll want to configure GCC to use the appropriate threads system; see [the libgcj page](libgcj2.html) for details.

## How to try it

Once you've downloaded and installed gcj and libgcj, it is very easy to try a small Java program of your own:

        gcj --main=HelloWorld -o HelloWorld HelloWorld.java
        ./HelloWorld
          

[**Return to main page**]()

* * * * *

> Java and all Java-based marks are trademarks or registered trademarks of Sun Microsystems, Inc. in the United States and other countries. The Free Software Foundation, Cygnus Solutions, and Red Hat are independent of Sun Microsystems, Inc.
