# GCJ - Less Recent News

January 16, 2006
:   Mark Wielaard has imported [GNU Classpath](http://www.gnu.org/software/classpath/) 0.20 ([release notes](http://www.gnu.org/software/classpath/announce/20060113.html)) into GCJ.
November 17, 2005
:   Mark Wielaard has imported [GNU Classpath](http://www.gnu.org/software/classpath/) 0.19 ([release notes](http://www.gnu.org/software/classpath/announce/20051102.html)) into GCJ.
October 12, 2005
:   The October 2005 issue of Red Hat Magazine contains the article "[The state of Java on Linux](https://www.redhat.com/magazine/012oct05/features/java/)" by Tom Tromey.
September 23, 2005
:   Tom Tromey has imported [GNU Classpath](http://www.gnu.org/software/classpath/) 0.18 ([release notes](http://www.gnu.org/software/classpath/announce/20050906.html)) into GCJ.
July 15, 2005
:   Tom Tromey has [integrated](http://gcc.gnu.org/ml/java-patches/2005-q3/msg00093.html) [GNU Classpath](http://www.gnu.org/software/classpath/) into the GCJ build in such a way that it becomes [much easier](http://gcc.gnu.org/ml/java/2005-05/msg00202.html) to import the latest GNU Classpath into GCJ. This particular import brings in GNU Classpath 0.17 ([release notes](http://www.gnu.org/software/classpath/announce/20050715.html)).
April 6, 2005
:   Mark Wielaard has written "[GCJ - past, present, and future](http://lwn.net/Articles/130796/)" for LWN.net.
March 10, 2005
:   Bryce McKinlay has merged in [a new stack trace mechanism](http://gcc.gnu.org/ml/java-patches/2005-q1/msg00712.html) that is based on GCC's unwind support and provides cleaner and faster stack traces.
February 15, 2005
:   Thomas Fitzsimmons has checked in an implementation of libjawt, the AWT Native interface. Among other things, this enables the JOGL (OpenGL for Java) bindings to work.
February 1, 2005
:   We've merged GNU JAXP into the core. This includes many classes in `javax.xml`, plus updated versions of `org.xml.sax` and `org.w3c.dom`.
November 25, 2004
:   John David Anglin checked in [a patch](http://gcc.gnu.org/ml/gcc-patches/2004-11/msg02214.html) to enable libjava to be built by default on hppa-unknown-linux-gnu.
November 22, 2004
:   We're pleased to announce that the GCJ binary compatibility branch has been merged to the trunk. This work includes a new ABI which allows precompiled code to follow the binary compatibility rules of the Java programming language.
September 21, 2004
:   Andreas Tobler imported the new `javax.crypto`, `javax.crypto.interfaces`, `javax.crypto.spec`, `javax.net`, `javax.net.ssl`, `javax.security.auth`, `javax.security.auth.callback`, `javax.security.auth.login`, `javax.security.auth.x500`, `javax.security.sasl` and `org.ietf.jgss` packages from the latest [GNU Classpath](http://www.gnu.org/software/classpath/) 0.11 developer snapshot release. These packages will be an official part of then next major release. Extra crypto algorithms can be obtained from the [GNU Crypto](http://www.gnu.org/software/gnu-crypto/) project, a full TLS implementation is provided by the Jessie project.
July 16, 2004
:   AWT and Swing support continues to improve rapidly. Thomas Fitzsimmons of Red Hat added support for the AWT 1.0 event model, still used by many web applets. This means that Slime Volleyball now runs on GCJ and gcjwebplugin.
July 16, 2004
:   GCJ in the press! The July issue of Linux Journal features an article on building the Eclipse IDE to native code using GCJ: [Eclipse goes native](http://www.linuxjournal.com/article/7413). The July issue of Doctor Dobbs Journal also features an article (not available online) on GCJ and the Compiled Native Interface ([CNI](http://gcc.gnu.org/onlinedocs/gcj/About-CNI.html)).
March 9, 2004
:   Thanks to Wes Biggs and the other GNU Regexp authors, Mark Wielaard (for merging into Classpath) and Anthony Green (for merging into libgcj), we now have support for `java.util.regex`. This arrives a little too late for gcc 3.4, but it will appear in the next major release.
January 22, 2004
:   Graydon Hoare has checked in [a patch to implement Swing buttons](http://gcc.gnu.org/ml/java-patches/2004-q1/msg00241.html). This is the first working Swing code, a major improvement. See the [screen shot](swingshot.png).
January 9, 2004
:   Andrew Haley has checked in [a large reorganization](http://gcc.gnu.org/ml/gcc-patches/2004-01/msg00362.html) of `-findirect-dispatch`. This is an important step toward the new binary compatibility ABI.
September 3, 2003
:   Jeff Sturm has adapted Jan Hubicka's call graph optimization code to gcj. This code improves gcj's method inlining abilities with support for out-of-order methods and inter-class method calls, along with improved code size heuristics. It is enabled by default when compiling at -O3.
August 4, 2003
:   Gary Benson from Red Hat has released [Naoko](http://people.redhat.com/gbenson/naoko/): a subset of the [rhug](http://sourceware.org/rhug/) packages that have been repackaged for eventual inclusion in Red Hat Linux. Naoko basically comprises binary RPMS of Ant, Tomcat, and their dependencies built with gcj.
August 2, 2003
:   Tom Tromey, Andrew Haley and others from Red Hat win the [Fast Free Eclipse Prize](http://lists.gnu.org/archive/html/classpath/2003-08/msg00004.html).
August 1, 2003
:   A team of hackers from Red Hat has released RPMS for a version of [Eclipse](http://www.eclipse.org/), a free software IDE written in Java, that has been compiled with a modified gcj. You can find more information [here](http://sourceware.org/eclipse/). We'll be integrating the required gcj patches in the near future.
July 31, 2003
:   Andrew Haley has checked in a major patch to the [tree-ssa branch](http://gcc.gnu.org/projects/tree-ssa/) that allows us to convert entire functions to trees when reading from `.class` files. This is an important step towards better high-level optimizations for Java.
July 30, 2003
:   Thanks to Andreas Tobler and Jeff Sturm, libgcj is now built by default for Darwin. This work is available on the cvs trunk, and will show up in GCJ 3.4.
July 28, 2003
:   Michael Koch has announced the first (0.0.1) release of [gcjwebplugin](http://www.nongnu.org/gcjwebplugin/). This is a plugin to allow execution of applets in web browsers like Mozilla, Konqueror, and Opera. Currently it is still a proof-of-concept.
May 14, 2003
:   [GCC 3.3 has been released](http://gcc.gnu.org/gcc-3.3/). This release includes many bug fixes, support for `assert`, and JDBC 3.0 support in `java.sql` and `javax.sql`, among other things.
January 3, 2003
:   Jeff Sturm implemented libffi closures for SPARC so that libgcj's bytecode interpreter is now available on SPARC Solaris hosts.
January 1, 2003
:   The January 2003 issue of Linux Journal contains the article [Compiling Java with GCJ](http://www.linuxjournal.com/article/4860) by Per Bothner.
December 27, 2002
:   It is now possible to run the [Eclipse](http://www.eclipse.org) IDE using the GCJ runtime, thanks to the work of Tom Tromey, Mark Wielaard, and others. Mark has a web page with further information and screenshots [here](http://www.klomp.org/mark/gij_eclipse/index.html).
September 29, 2002
:   Ulrich Weigand has implemented the necessary support in libffi to get libgcj running on the s390x.
September 29, 2002
:   Anthony Green merged the java.lang.reflect.Proxy implementation in from GNU Classpath.
August 16, 2002
:   Andrew Haley updated the gcc tree-based inliner to work for gcj.
July 25, 2002
:   Kaz Kojima has implemented the necessary support in libffi and libjava to get libgcj running on SH-3/4.
July 19, 2002
:   Bo Thorsen, SuSE Labs, has implemented the necessary support in libffi, boehm-gc and libjava to get libgcj running on x86-64. This is a big step towards getting libgcj fully supported on x86-64.
June 24, 2002
:   Tom Tromey has checked in a patch that makes the bytecode interpreter use direct threading. This gives the interpreter a performance boost.
June 21, 2002
:   The `java.sql` and `javax.sql` packages were updated by Bryce McKinlay to implement the JDBC 3.0 (JDK 1.4) API.
June 11, 2002
:   Tom Tromey has implemented the JDK 1.4 `assert` facility.
March 10, 2002
:   Adam Megacz has contributed a mingw32 port of libgcj. This port is still a little bit incomplete (e.g., `java.lang.Process` doesn't work), and it currently only works as a target, not a host. Still, it works, and he is using it for [XWT](http://www.xwt.org/). This work will appear in the upcoming 3.1 release.
February 28, 2002
:   Bryce McKinlay has contributed a patch to optimize some of the array access and added a new compiler flag, `--no-store-check`, to disable assignability checks for stores into object arrays. With code that is known not to throw `ArrayStoreException`, this flag can be used to disable the check operations. In which case it can provide a reasonable performance boost and slight code size reduction.
January 22, 2002
:   Tom Tromey has contributed a patch to allow certain Java method calls to be inlined by gcj. For instance, calls to `Math.min` are now inlined when optimization is enabled. This patch includes some infrastructure to make it easy to add more such inlines when desired.
January 14, 2002
:   Richard Stallman has changed the licensing of the Classpath AWT implementation to match the licensing of the rest of Classpath. This means that the only remaining barrier to AWT for libgcj is manpower. Work has already begun to merge the Classpath and libgcj AWT implementations.
January 14, 2002
:   Adam Megacz announced that [XWT](http://www.xwt.org) is an ActiveX control which is written in Java and compiled with gcj. We hope to be checking in his Windows patches in the near future.
December 14, 2001
:   Hans Boehm has checked in changes which once again allow gcj and libgcj to work on IA-64. His changes also speed up allocation of objects which don't require a finalizer.
November 5, 2001
:   Tom Tromey contributed a bytecode verifier. It hasn't yet been extensively tested, so it is still disabled by default.
October 24, 2001
:   Tom Tromey of Red Hat checked in the `javax.naming` and `javax.transaction` packages, started by Anthony Green finished by Warren Levy and Tom.
October 2, 2001
:   Tom Tromey of Red Hat has written the support code necessary to implement `java.lang.ref.*`.
September 7, 2001
:   Anthony Green of Red Hat has added a facility for compiling property files and other system resources into libraries and executables. These files are accessible to the gcj runtime via the new "core" protocol handler. The gcj runtime places "core:/" at the end of the java.class.path, ensuring that compiled-in resource files are discovered and loaded when required. This is a convenient feature for building and deploying libraries and executables with no external file dependencies. See the `--resource` option in the gcj manual for details.
August 27, 2001
:   Tom Tromey has imported RMI into libgcj. This RMI implementation was implemented and donated to the Free Software Foundation by Transvirtual Technologies; many thanks to them for their important contribution.
June 18, 2001
:   GCC 3.0 has been released! Everything you need to build and run GCJ is now included in a single source distribution. Download it from one of our [mirror sites](../mirrors.html).
June 4, 2001
:   Per Bothner of Brainfood Inc has implemented an invocation interface allowing an existing non-Java thread to start and connect to a Java environment. The GCJ start-up code now uses this interface. Brainfood is using this to attach an Apache module to a pre-compiled servlet-like class.
May 30, 2001
:   We now have a new hash-table based lightweight lock implementation which was contributed by Hans Boehm. The new code should reduce memory consumption and improve performance for applications which do a lot of synchronization. Currently, this code is enabled for IA-64 and X86 Linux.
April 25, 2001
:   Bryce McKinlay merged `java.security` with Classpath. The new package is now JDK 1.2 compliant, and much more complete.
March 25, 2001
:   It is now possible to call methods on Java interface references from C++ code via [CNI](papers/cni/t1.html), thanks to Bryce McKinlay's compiler work. [Details.](http://gcc.gnu.org/ml/gcc-patches/2001-03/msg01483.html)
March 25, 2001
:   Kevin B. Hendricks implemented the necessary FFI support to get the libgcj interpreter running on PowerPC. This is now one of our fully supported targets!
March 7, 2001
:   The first snapshot from the code branch that will become GCC 3.0 is now available. Snapshots from this branch will be made weekly. These include everything you need to build and run GCJ, and should be much more stable than the main development tree -- so try them out and help us find bugs!
February 8, 2001
:   Made use of Warren Levy's change to the [Mauve test suite](http://sourceware.org/mauve/) to handle regressions. Modifications have been made to `mauve.exp` to copy the newly created `xfails` file of known library failures from the source tree to the directory where the libjava `'make check'` is being run. This allows the testsuite to ignore `XFAIL`s and thus highlight true regressions in the library. The Mauve tests are automatically run as part of a libjava `make check` as long as the Mauve suite is accessible and the environment variable `MAUVEDIR` is set to point to the top-level of the Mauve sources.
January 28, 2001
:   The various gcj mailing lists have moved to gcc.gnu.org. Unused lists have been removed, and java-discuss was renamed to java. See [the announcement](http://gcc.gnu.org/ml/java/2001-01/msg00539.html) for all the gory details. With this change, the gcj project is now fully merged into GCC.
January 17, 2001
:   GCJ is now compatible with the V3 C++ ABI, thanks to the efforts of Alexandre Petit-Bianco.
December 20, 2000
:   The Java GNATS database has been merged into the GCC GNATS database. The old database continues to exist but only for reference purposes; it is now read-only.
December 9, 2000
:   Thanks to Richard Henderson's recent `libffi` changes, the `libgcj` bytecode interpreter is now enabled for Alpha systems.
December 9, 2000
:   The libgcj sources have [migrated](http://gcc.gnu.org/ml/java/2000-12/msg00102.html) to the gcc repository. We've imported [fastjar](http://sourceforge.net/projects/fastjar/) in our [tree](http://gcc.gnu.org/cgi-bin/cvsweb.cgi/gcc/fastjar/) and use it as a replacement to zip.
December 8, 2000
:   The libgcj repository is now [closed](http://gcc.gnu.org/ml/java/2000-12/msg00095.html): We're moving our sources over to the gcc [tree](http://gcc.gnu.org/ml/java-announce/2000/msg00003.html). Read [here](http://gcc.gnu.org/svn.html) how you will soon check your sources out.
November 2, 2000
:   Warren Levy has checked in code for a serialized object dumper. It is enabled by configuring with `--enable-libgcj-debug` and calling `System.setProperty("gcj.dumpobjects", "true");` in your test program. The output will be generated as the object is deserialized (i.e. the `readObject()` method is executed).
October 29, 2000
:   Bryce McKinlay checked in a large patch merging parts of the Java Collections code from Classpath into libgcj. He's also been working on reformatting code to fit the coding standards, and tuning the Collections code for performance.
October 27, 2000
:   Warren Levy has checked in even more serialization work -- he's been working steadily on making object serialization compatible with Sun's implementation for a while now, and I believe we are quite close to complete compatibility.
October 22, 2000
:   Rolf Rasmussen has checked in a large piece of code implementing most of Xlib-based AWT peers as well as parts of J2D. Bryce McKinlay has also been working on AWT.
September 30, 2000
:   Bryce McKinlay checked in a patch that enables bitmap-descriptor based marking in the garbage collector. This code includes work by Tom Tromey and Hans Boehm, and results in some significant [performance gains](http://gcc.gnu.org/ml/java-patches/2000-q3/msg00161.html) in memory-intensive code.
September 8, 2000
:   Warren Levy checked in various patches that provide much a more compatible serialization implementation. Still some special case classes to go, but 90% of the serializable classes should now be compatible with Sun's JDK.
September 6, 2000
:   Anthony Green checked in a patch to make gcj read compressed zip files. He also moved zlib into the gcc tree -- the first concrete work towards merging the gcc and libgcj source trees.
August 20, 2000
:   We haven't had any news in a while, but things have still been happening: Anthony Green recently merged in a lot of Classpath code, in particular the `java.util.jar` and `java.security.cert` packages. Also, Bryce McKinlay and Rolf Rasmussen have done a fair amount of work on `java.awt` (not ready for a screen shot yet, though).
August 3, 2000
:   Oskar Liljeblad has made the first release of [gnome-gcj](http://gnome-gcj.sourceforge.net). This is a project to create a complete set of [Gtk+](http://www.gtk.org/) and [Gnome](http://www.gnome.org/) bindings for Java using gcj.
June 27, 2000
:   A new [Done with GCJ](done.html) section was added. Send your GCJ stories [here](mailto:java@gcc.gnu.org).
May 19, 2000
:   Today we merged some major work done at Red Hat. This included:
    -   Substantial work for the IA-64 port.
    -   `java.beans` from [Classpath](http://www.gnu.org/software/classpath/).
    -   A somewhat modified initial implementation of serialization. (This is known not to interoperate with other implementations yet.)
    -   Miscellaneous bug fixes.

May 2, 2000
:   Tom Tromey checked in the JNI stubs patch for gcj. This patch makes it possible to use compiled Java code where the native methods are implemented using JNI.
April 22, 2000
:   The new [GCJ Projects](projects.html) page is brought online.
April 10, 2000
:   Warren Levy checked in a JDK 1.1 compatible version of [Classpath's](http://www.gnu.org/software/classpath/) `java.sql`. Once we have merged the necessary JDK 1.2 support, we'll update this to directly track Classpath.
April 8, 2000
:   Tom Tromey wrote most of `java.awt.event`.
April 3, 2000
:   Anthony Green made RPMs of gcj and libgcj. See his original [message](http://gcc.gnu.org/ml/java/2000-04/msg00022.html) for some caveats.
March 26, 2000
:   Hans Boehm contributed a port of libgcj to the IA-64. This port includes the interpreter.
March 15, 2000
:   Jon Beniston contributed the beginnings of a Windows port of libgcj.
March 13, 2000
:   Alexandre Petit-Bianco has checked in a change to the compiler to add support for most JDK 1.1 language features, such as inner classes.
March 9, 2000
:   Warren Levy checked in the basics of the `java.security.*` packages that he and Tom Tromey have been putting together. It is just the basics of `java.security` and some of it is indeed stubbed. It has been tried with the 20000120 snapshot of Cryptix JCE, so it is usable.
     Please note that it is mostly JDK 1.1 based but there are some JDK 1.2 things included.
March 7, 2000
:   Bryce McKinlay checked in a change to both the runtime and the compiler which implements constant-time interface method lookup and type checking. This should provide a huge speed increase for programs that use interfaces or things like \`\`instanceof''.
March 6, 2000
:   We are relicensing libgcj to a more liberal license, and we're assigning copyright to the [FSF](http://www.gnu.org/). This will let us share code with the [Classpath](http://www.gnu.org/software/classpath/) project.
March 2, 2000
:   Tom Tromey changed g++ to allow CNI code to catch and throw exceptions Java-style. This makes it much easier to write CNI code which calls Java methods that throw exceptions.
February 25, 2000
:   Anthony Green added an important optimization to gcj. His optimization eliminates redundant class initializations in some cases. It should speed up code which uses static methods or fields.
February 25, 2000
:   Hans Boehm has contributed an IA-64 libffi port. This is an important step towards getting libgcj to run on the IA-64.
February 16, 2000
:   We've done a lot since the previous announcement, but we haven't mentioned it here. Oops! Here are some of the things we've added: complete reflection support, Runtime.loadLibrary, Throwable.printStackTrace (this one is *very* cool), a Java implementation of java.math.BigInteger, JNI (believed complete but still in testing), and, of course, many bug fixes and more minor enhancements.
September 6, 1999
:   [One year later](gcj-announce.txt)... new web pages.
August 26, 1999
:   [Libgcj 2.95.1 is released](http://gcc.gnu.org/ml/java-announce/1999/msg00005.html)! This version corresponds to the GCC 2.95.1 release.

