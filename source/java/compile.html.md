# Compiling with GCJ {align="center"}

## Introduction

GCJ works in three ways: First GCJ can take .java files as input and compile architecture-specific object files. Second, by using the -C option, GCJ can take .java files as input, and generate .class files. Third, GCJ can take .class files as input to create architecture-specific object files.

GCJ offers two run-time support libraries, libgcj and libgcjgc. libgcj is a clean-room implementation of the core Java libraries. libgcjgc is the garbage collector, which is responsible for automatic memory management. This collector is based on the Boehm-Weiser conservative collector, but it scans Java objects precisely and has changes to work with the cooperative threads-package. It uses a basic mark-sweep algorithm to perform the actual collections, stopping all threads as it works.

## Compiling Java Programs

If you are working with Java, we recommend you use the gcj program instead of using GCC directly.

### Java-specific file extensions

GCJ (GNU Compiler for Java) deals with the following Java-specific file extensions, and you can specify any of these as an input file:

.java
:   A source file in the Java language, consisting of one or more class definitions.
.class
:   A binary file containing bytecode instructions and information pertaining to a single class. It can be loaded into and executed by a Java Virtual Machine. Compiling a .java source file results in one or more .class files, one for each class definition in the source file. The .class file format was designed as a portable and secure representation of Java classes, but there also exist tools for compiling program written in other languages (such as Ada, Scheme, and ML) into .class files.
.zip
:   The .zip file format is a file archival and compression format popular in the PC world. In the Java world it is mainly used to bundle a collection of .class files. When specified on the gcj command line, gcj compiles all the .class files in the .zip archive. The result is a single assembly file, object file, or executable, depending on the options you specify.
.jar
:   A .jar (Java ARchive) file is in .zip format, but following certainly extra conventions. (Certain extra files should also be included.) GCJ treats it the same as a .zip file.

### GCJ command-line options

In addition to the normal GCC options, GCJ recognizes the following options:

-C
:   The input file(s) must all be .java source files. They are compiled into portable .class files. No machine code (.o files or executable) is generated.
--output-class-dir=OUTPUTDIR
:   When used with -C, specifies which directory the generated .class should be written to. A class A.B (whose input file is usually A/B.java) would be written to OUTPUTDIR/A/B.class.
-d OUTPUTDIR
:   Synonym for --output-class-directory, for compatibility with Suns javac.
--bounds-check
:   When compiling to machine code, emit instructions to check that array indexes are within bounds, and to throw an exception if they are not. This is the default.
--no-bounds-check
:   When compiling to machine code, do not emit instructions to check that array indexes are within bounds.
-M
-MM
-MD
-MMD
:   These options work as with the C compiler. For GCJs purposes, a system header is any .zip file installed as part of the compiler system.
--main=CLASSNAME
:   When linking an application, generate a stub so the application starts executing with the main method of the class named. (This option is ignored if you are only compiling and not linking.)

### Path searching options

At compile time, GCJ uses a list of paths to search for classes and packages that it needs to find. This list is called the classpath. Each element of the classpath can be either a directory or the name of a .zip or .jar file. In the latter case, GCJ searches the contents of the file for the required information. GCJ has a built-in classpath, which includes the directory ., and the system libgcj.zip file, which holds classes from the standard Java class libraries, such as java.lang. There are several ways to set or augment the classpath.

-I directory
:   A directory (or file) specified using -I are prepended to the classpath. -I options are never overridden by the other options listed below.
--classpath=path
-classpath path
:   If specified, the option to --classpath (or -classpath; the two spellings are synonymous) overrides the built-in classpath, and suppresses recognition of --CLASSPATH and the CLASSPATH environment variable.
--CLASSPATH=path -CLASSPATH path
:   If specified, the option to --CLASSPATH (or -CLASSPATH; the two spellings are synonymous) is appended to the built-in classpath, but suppresses recognition of the CLASSPATH environment variable.
CLASSPATH
:   The CLASSPATH environment-variable can be set to a path. This path is appended to the compiler-supplied default path. In the above, a path is a colon-separated (on Windows, semicolon-separated) list of directories or file names.

Here are some other points worth noting:

-   If there is no -g or -O option (and no options starting with those letters), the default is -g1. This is different from gcc, where the default is -g0. Making -g1 the default causes line number information to be generated, but not the other information necessary for source-level debugging. The reason for this change is partly for compatibility with Sun's tools, and partly because it is helpful when printing an exceptions stack trace.
-   When an application is linked, gcj will also link in the standard Java run-time libraries (libgcj, and possibly others).

