The Cygnus Native Interface for C++/Java Integration

[Prev](t1405.html)

The Cygnus Native Interface for C++/Java Integration

[Next](t1431.html)

* * * * *

# Exception Handling

While C++ and Java share a common exception handling framework, things are not yet perfectly integrated. The main issue is that the “run-time type information” facilities of the two languages are not integrated.

Still, things work fairly well. You can throw a Java exception from C++ using the ordinary `throw`{.LITERAL} construct, and this exception can be caught by Java code. Similarly, you can catch an exception thrown from Java using the C++ `catch`{.LITERAL} construct.

Note that currently you cannot mix C++ catches and Java catches in a single C++ translation unit. We do intend to fix this eventually.

Here is an example:

``` {.PROGRAMLISTING}
if (i >= count)
   throw new java::lang::IndexOutOfBoundsException();
```

* * * * *

||
|[Prev](t1405.html)|[Home](t1.html)|[Next](t1431.html)|
|Class Initialization| |Synchronization|


