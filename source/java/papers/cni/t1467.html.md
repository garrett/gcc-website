The Cygnus Native Interface for C++/Java Integration

[Prev](t1457.html)

The Cygnus Native Interface for C++/Java Integration

 

* * * * *

# Using gcjh

The **gcjh** is used to generate C++ header files from Java class files. By default, **gcjh** generates a relatively straightforward C++ header file. However, there are a few caveats to its use, and a few options which can be used to change how it operates:

`--classpath`{.LITERAL} `path`{.REPLACEABLE}, `--CLASSPATH`{.LITERAL} `path`{.REPLACEABLE}, `-I`{.LITERAL} `dir`{.REPLACEABLE}
:   These options can be used to set the class path for gcjh. Gcjh searches the class path the same way the compiler does; these options have their familiar meanings.

`-d directory`{.LITERAL}
:   Puts the generated `.h`{.LITERAL} files beneath `directory`{.REPLACEABLE}.

`-o file`{.LITERAL}
:   Sets the name of the `.h`{.LITERAL} file to be generated. By default the `.h`{.LITERAL} file is named after the class. This option only really makes sense if just a single class file is specified.

`--verbose`{.LITERAL}
:   gcjh will print information to stderr as it works.

`-M`{.LITERAL}, `-MM`{.LITERAL}, `-MD`{.LITERAL}, `-MMD`{.LITERAL}
:   These options can be used to generate dependency information for the generated header file. They work the same way as the corresponding compiler options.

`-prepend text`{.LITERAL}
:   This causes the `text`{.REPLACEABLE} to be put into the generated header just after class declarations (but before declaration of the current class). This option should be used with caution.

`-friend text`{.LITERAL}
:   This causes the `text`{.REPLACEABLE} to be put into the class declaration after a `friend`{.LITERAL} keyword. This can be used to declare some other class or function to be a friend of this class. This option should be used with caution.

`-add text`{.LITERAL}
:   The `text`{.REPLACEABLE} is inserted into the class declaration. This option should be used with caution.

`-append text`{.LITERAL}
:   The `text`{.REPLACEABLE} is inserted into the header file after the class declaration. One use for this is to generate inline functions. This option should be used with caution.

All other options not beginning with a `-`{.LITERAL} are treated as the names of classes for which headers should be generated.

gcjh will generate all the required namespace declarations and `#include`{.LITERAL}'s for the header file. In some situations, gcjh will generate simple inline member functions. Note that, while gcjh puts `#pragma interface`{.LITERAL} in the generated header file, you should *not* put `#pragma implementation`{.LITERAL} into your C++ source file. If you do, duplicate definitions of inline functions will sometimes be created, leading to link-time errors.

There are a few cases where gcjh will fail to work properly:

gcjh assumes that all the methods and fields of a class have ASCII names. The C++ compiler cannot correctly handle non-ASCII identifiers. gcjh does not currently diagnose this problem.

gcjh also cannot fully handle classes where a field and a method have the same name. If the field is static, an error will result. Otherwise, the field will be renamed in the generated header; \`\_\_' will be appended to the field name.

Eventually we hope to change the C++ compiler so that these restrictions can be lifted.

* * * * *

||
|[Prev](t1457.html)|[Home](t1.html)| |
|Reflection| | |


