# GCC mailing lists

The GCC project has many mailing lists, which are archived on the web (and [searchable](#searchbox)). Please make yourself familiar with [our policies](#policies) before [subscribing](#subscribe) and posting to these lists.

Announcement lists:

-   **[gcc-announce](http://gcc.gnu.org/ml/gcc-announce/)** is a read-only low volume list where we post announcements about releases or other important events.
-   **[java-announce](http://gcc.gnu.org/ml/java-announce/)** is a low-volume, moderated, announcements-only list. Only announcements related to the Java language front end or runtime library are posted here.

Open lists:

-   **[gcc-help](http://gcc.gnu.org/ml/gcc-help/)** is a relatively high volume list for people searching for help in building or using GCC.
-   **[gcc](http://gcc.gnu.org/ml/gcc/)** is a high volume list for general development discussions about GCC. Anything relevant to the development or testing of GCC and not covered by other mailing lists is suitable for discussion here.

    Recruiting postings, including recruiting for GCC or other free software jobs, are not permitted on any of our mailing lists. If you are interested in hiring a GCC developer, please visit the [FSF jobs page](http://www.fsf.org/resources/jobs/).

    All major decisions and changes, like abandoning ports or front ends, should be announced and discussed here. Ideally, this list should be sufficient to follow the major trends and important news in GCC's development process.

-   **[gcc-bugs](http://gcc.gnu.org/ml/gcc-bugs/)** is a relatively high volume list with mails from our bug-tracking system.
-   **[gcc-patches](http://gcc.gnu.org/ml/gcc-patches/)** is a relatively high volume list for patch submissions and discussion of particular patches. All patches (including those for front ends and web pages) and all discussion for a particular patch should be sent to this list.
-   **[gcc-testresults](http://gcc.gnu.org/ml/gcc-testresults/)** is a moderate volume list where test results for the GCC compilers are posted.
-   **[gcc-regression](http://gcc.gnu.org/ml/gcc-regression/)** is a moderate volume list where regression results for the GCC compilers are posted.
-   **[libstdc++](http://gcc.gnu.org/ml/libstdc++/)** is the main discussion and development list for the standard C++ library (libstdc++-v3). Patches to libstdc++-v3 should be sent to both this list and **gcc-patches**.
-   **[java](http://gcc.gnu.org/ml/java/)** is the main discussion and development list for the Java language front end of GCC, and the corresponding runtime library.
-   **[java-patches](http://gcc.gnu.org/ml/java-patches/)** is a list for submission and discussion of patches to libgcj, the Java runtime. Patches to GCJ, the Java language front end, should go to both this list and **gcc-patches**.
-   **[fortran](http://gcc.gnu.org/ml/fortran/)** is the main discussion and development list for the Fortran language front end of GCC, and the corresponding runtime library. Patches to gfortran and libgfortran should go to both this list and **gcc-patches**.
-   **[jit](http://gcc.gnu.org/ml/jit/)** is for discussion and development of [libgccjit](http://gcc.gnu.org/wiki/JIT), an experimental library for implementing Just-In-Time compilation using GCC as a back end. The list is intended for both users and developers of the library. Patches for the jit branch should go to both this list and **gcc-patches**.

Read only lists:

-   **[gcc-prs](http://gcc.gnu.org/ml/gcc-prs/)** is a read-only, relatively high volume list which tracks problem reports as they are entered into our database.
-   **[gcc-cvs](http://gcc.gnu.org/ml/gcc-cvs/)** is a read-only, relatively high volume list which tracks checkins to the GCC SVN repository.
-   **[libstdc++-cvs](http://gcc.gnu.org/ml/libstdc++-cvs/)** is a read-only, relatively low volume list which tracks checkins to the libstdc++-v3 part of the GCC SVN repository. This is a subset of the messages to **gcc-cvs**.
-   **[gcc-cvs-wwwdocs](http://gcc.gnu.org/ml/gcc-cvs-wwwdocs/)** is a read-only, relatively low volume list which tracks checkins to the GCC webpages repository.
-   **[gccadmin](http://gcc.gnu.org/ml/gccadmin/)** is a read-only moderate volume list where output from `cron` jobs run by the `gccadmin` account on gcc.gnu.org is posted.
-   **[java-prs](http://gcc.gnu.org/ml/java-prs/)** is a read-only list which tracks Java-related problem reports as they are entered into our database. Messages sent here are also sent to **gcc-prs**.
-   **java-cvs** tracks checkins to the Java language compiler and runtime. It is not archived. Messages sent here are also sent to **gcc-cvs**.

Historical lists (archives only, no longer in use):

-   **[libstdc++-prs](http://gcc.gnu.org/ml/libstdc++-prs/)** was formerly used by the libstdc++-v3 problem report database. libstdc++-v3 now uses the main GCC database and the **gcc-prs** list.

To post a message, just send mail to *listname*`@gcc.gnu.org`.

## Policies

Our lists have a maximum message size of 100KB, only the gcc-prs list allows for a larger maximum message size of 2MB. If your message exceeds this size, you should compress any attachments before sending it to the list.

We have a strong policy of not editing the web archives.

When posting messages, please select an appropriate list for the message and try to avoid cross posting a message to several lists.

Please refrain from sending messages in HTML, RTF or similar formats.

Please do not include or reference confidentiality notices, like:

> The referring document contains privileged and confidential information. If you are not the intended recipient you must not disseminate, copy or take any action in reliance on it, and we request that you notify companyname immediately.

Such disclaimers are inappropriate for mail sent to public lists. If your company automatically adds something like this to outgoing mail, and you can't convince them to stop, you might consider using a free web-based e-mail account.

Notices like:

> Any views expressed in this message are those of the individual sender, except where they are specifically stated to be the views of companyname.

are acceptable, although they are redundant; unless explicitly stated, it's assumed that no-one on these lists means to speak for their company.

## Subscribing/unsubscribing {#subscribe}

You will be able to subscribe or unsubscribe from any of the GCC mailing lists via this form:

> Mailing list: gcc-announce gcc gcc-help gcc-bugs gcc-patches gcc-testresults gcc-prs gcc-cvs gcc-cvs-wwwdocs gcc-regression libstdc++ libstdc++-cvs java-announce java java-patches java-prs java-cvs fortran jit
>  Your e-mail address:
>  Digest version   subscribe unsubscribe  

If you're having trouble getting off a list, look at the **`List-Unsubscribe:`** header on a message sent to that list. It has your subscribed address included in it. Send mail to that address (omit the `mailto:` part) and you'll be unsubscribing yourself from that mailing list. You'll need to confirm the unsubscription, of course.

Please trust in the `List-Unsubscribe:` header. Every person who has said "I can't get off this list! Unsubscribe me!" has found, with enough prodding, that sending mail to the address listed in `List-Unsubscribe:` does the trick.

## Filtering

If you want to use procmail or similar tools to process the GCC mailing lists, you can filter using the Sender: header, as well as all of the RFC2369 headers (List-Subscribe, List-Unsubscribe, List-Post, List-Archive, etc.).

For example, the following procmail rule will sort all mail from our lists into a single folder named INLIST.gcc:

> ` :0 * ^Sender: .*-owner@gcc.gnu.org INLIST.gcc`

To filter duplicate messages due to cross-posts to multiple lists, you can use the following recipe (Use at your own risk!):

> ` :0 Wh: msgid.lock * ^Sender: .*-owner@gcc.gnu.org | formail -D 8192 msgid.cache`

## Help

For further information on using the lists, send a blank mail to `listname-help@gcc.gnu.org` (but note that this won't currently work for the *gcc* list).

## Spam blocking

To reduce spam sent to the GCC mailing lists, a handful of Realtime Blackhole Lists (RBLs) are consulted. If you're sending mail from a site that is listed in one of these RBLs, contact your site administrator about fixing your mail setup. Spammers are taking advantage of your site to relay their spam.

Note that, if you are *subscribed* to a mailing list at gcc.gnu.org, you will not be subject to any kind of spam blocking for that mailing list. However, if you are subscribed from one account and post from *another* then the posting account will be subject to spam block checking. To avoid this, you can put yourself on the "global allow" list for gcc.gnu.org by sending mail to

> ` global-allow-subscribe-you=yourdomain.com@gcc.gnu.org`

(where you=yourdomain.com translates to your email address with an "=" substituted for the "@"). This will bypass all spam checking for future submissions to the gcc.gnu.org mailing lists.

You can use this technique if you just want to be able to send email to a list without receiving any email from the list. You can also give yourself posting privileges just for an individual list by replacing "global" above with the name of the specific list.

To complicate the harvesting of e-mail addresses from the web archives of the GCC mailing lists, some simple transformations are done on the e-mail addresses.

See also [information about dealing with spam on the lists](spam.html).

* * * * *

This search will allow you to search the contents gcc.gnu.org.
