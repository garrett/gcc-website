# Read-write SVN access

We have read/write access to the SVN repository available for significant developers. Maintainers are also encouraged to [edit our bugs database](bugs/management.html). Our [web pages are managed via CVS](about.html#cvs).

* * * * *

## Contents

1.  [Authenticated access](#authenticated)
2.  [Setting up your local SVN tree](#setup)
3.  [Write access policies](#policies)
4.  [Testing changes](#testing)
5.  [Checking in a change](#checkin)
6.  [Example check-in session](#example)
7.  [Creating and using branches](#branches)
8.  [Tips&Tricks around your account](#account)

* * * * *

## Authenticated access

We provide authenticated access via the SSH protocol.

If you already have an account on sourceware.org, please send email to `overseers(at)gcc.gnu.org` with a request for access to the GCC repository. Include the name of an existing maintainer who is sponsoring your access and CC: your sponsor. Else use [this form](http://sourceware.org/cgi-bin/pdw/ps_form.cgi) to supply your SSH public key (which you can generate via the `ssh-keygen` program) and other details.

Once we have this information we will set up an account on `gcc.gnu.org` and inform you by mail. At this point, check out a tree using the instructions below and add yourself to the MAINTAINERS file. Note: Your first and last names *must* be exactly the same between your account on gcc.gnu.org and the MAINTAINERS file. Place your name in the correct section following the conventions specified in the file (e.g. "Write After Approval" is "last name alphabetical order").

Then produce a diff to that file and circulate it to the `gcc-patches` list, whilst also checking in your change to test write access (approval from the mailing list is not needed in this one case). For all other changes, please be sure to follow the write access policies below.

* * * * *

## Setting up your local SVN tree

Once your account has been set up, check out the GCC sources by issuing the command:

> ` svn co svn+ssh://username@gcc.gnu.org/svn/gcc/trunk gcc`

where *username* is your user name at gcc.gnu.org.

It is also possible to convert an existing SVN tree to use SSH by using `svn switch --relocate`:

> ` svn switch --relocate svn://gcc.gnu.org/svn/gcc svn+ssh://username@gcc.gnu.org/svn/gcc`

To avoid the nuisance of having to supply your passphrase for each operation, you may want to use `ssh-agent`(1) and `ssh-add`(1).

To avoid messages about (lack of) X11 forwarding, put in your \$HOME/.ssh/config an entry like:

> ` Host gcc.gnu.org ForwardX11 no`

* * * * *

## Write access policies

The GCC project grants developers various levels of write access to and review authority over the GCC master sources. We have not put any technical enforcement in place, rather we rely on everyone to follow the appropriate policies.

Global reviewers.
:   A limited number of developers have global review permission and can approve other people's changes to any part of the compiler.

Localized write permission.
:   This is for people who have primary responsibility for ports, front ends, or other specific aspects of the compiler. These folks are allowed to make changes to areas they maintain and related documentation, web pages, and test cases without approval from anyone else, and approve other people's changes in those areas. They must get approval for changes elsewhere in the compiler.

    Maintainers of a port maintain the relevant files in `gcc/config`, documentation, web pages, and test cases and aspects of these relevant to that port. Port maintainers do not have approval rights beyond this.

Localized review permission.
:   This is similar to localized write permission, except that reviewers required approval for their own changes.

Write after approval.
:   This is folks that make regular contributions, but do not fall into one of the previous categories. People with write after approval need to submit their patches to the list; once the patches have been approved by the appropriate maintainers the patches may be checked in. The steering committee or a well-established GCC maintainer (including reviewers) can [approve for write access](#authenticated) any person with GNU copyright assignment papers in place and known to submit good patches.

The list of folks with write access to the repository can be found in the MAINTAINERS file in the GCC distribution.

When you have checked in a patch exactly as it has been approved, you do not need to tell that to people -- including the approver. People interested in when a particular patch is committed can check SVN or the [gcc-cvs](http://gcc.gnu.org/ml/gcc-cvs/) list.

### Free for all

The following changes can be made by everyone with SVN write access:

Obvious fixes can be committed without prior approval. Just check in the fix and copy it to `gcc-patches`. A good test to determine whether a fix is obvious: “will the person who objects to my work the most be able to find a fault with my fix?” If the fix is later found to be faulty, it can always be rolled back. We don't want to get overly restrictive about checkin policies.

Similarly, no outside approval is needed to revert a patch that you checked in.

[Importing files maintained outside the tree from their official versions](codingconventions.html#upstream).

[Creating and using a branch](#branches) for development, including outside the parts of the compiler one maintains, provided that changes on the branch have copyright assignments on file. Merging such developments back to the mainline still needs approval in the usual way.

* * * * *

## Testing changes

All changes must be tested according to the [instructions for testing patches](contribute.html#testing) before they are checked in. If you wrote the patch yourself, you should test it yourself, unless there is a reason why someone else must do it for you (for instance, if you are fixing a problem on a system you do not have access to). If you are checking in a patch for someone else, you only need to test it if they did not.

You must test exactly the change you intend to check in; it is not good enough to have tested an earlier variant. (Unless the only changes from the earlier variant are formatting and comment changes; if there are **any** changes to the code itself you should re-bootstrap.) It is a good idea to re-test patches which were submitted a long time ago before applying them, even if nothing appears to have changed.

When you post your change to `gcc-patches`, state the canonical name(s) of the platform(s) you used for testing.

These rules are designed to ensure that checked-in code does not contain bugs that prevent other people from continuing to get their work done. There will always be bugs, but these rules help to minimize the amount of time where the tree does not build at all. Repeated failure to adhere to these rules could result in the revocation of check-in privileges by the Steering Committee.

* * * * *

## Checking in a change

The following is meant to provide a very quick overview of how to check in a change. It is not meant to be a replacement for the SVN manual but instead a supplement. The SVN manual is available both as part of the SVN source distribution, as well as at the book [Version Control with Subversion](http://svnbook.red-bean.com/).

In all the commands listed below, you can give an explicit list of filenames to the SVN command. We recommend you list files explicitly when performing checkins to avoid accidental checkins of local code.

We prefer that each SVN checkin be of a complete, single logical change, which may affect multiple files. The log message for that checkin should be the complete ChangeLog entry for the change. This makes it easier to correlate changes across files, and minimizes the time the repository is inconsistent. If you have several unrelated changes, you should check them in with separate SVN commit commands.

1.  Sync your sources with the master repository via "`svn update`" before attempting a checkin; this will save you a little time if someone else has modified that file since the last time you synced your sources. It will also identify any files in your local tree that you have modified.
2.  Apply the patch to your local tree and update the ChangeLog file. Use the current date/time for the ChangeLog entry, not the time that the patch was submitted.
3.  Make sure to rebuild any generated files that would be affected by the patch. Make sure to check them in along with the files explicitly modified by the patch.
4.  We recommend using "`svn diff`" after applying a patch to a local tree. Review the output to make sure that only the changes you wanted to check in will be checked in. Also check to see if the copyright dates need to be updated.
5.  Use "`svn commit`" to check in the patch. You can enter the log message via the "`-m`" argument to commit, or wait for the editor window to appear and enter the log message in the editor window.
6.  After exiting the editor, SVN will connect to the GCC SVN server and check in your changes. When your prompt returns the checkin is finished. A message will be sent to the gcc-cvs mailing list indicating that a change was made. SVN will provide a message if an error occurs and it will not check in any files.

* * * * *

## Example check-in session

Here's an actual check-in session for a patch John Carr recently sent to the GCC list. This was the ChangeLog for his change:

>     Sun Feb  8 08:02:13 1998  John Carr  <jfc@mit.edu>
>
>        * bitmap.c (bitmap_debug_file): HOST_PTR_PRINTF converts a pointer,
>        not a HOST_WIDE_INT.
>
>        * calls.c (expand_call): Change test of expand_inline_function
>        return value to stop compiler warning.
>
>        * genattrtab.c (RTL_HASH): Cast pointer to long, not HOST_WIDE_INT.

### First, I sync my local repository.

>     [/law/gcc] svn update
>     ? libobjc
>     ? gcc/.ada
>     ? gcc/jump.c.SAVE
>     ? gcc/loop.c.SAVE
>     M MAINTAINERS
>     M Makefile.in
>     M gcc/loop.c
>     M gcc/cp/parse.c
>     M gcc/objc/Make-lang.in
>     M gcc/objc/Makefile.in
>     At revision 105932

The question marks indicate files in my local repository that are not part of the official sources. The "M" indicates files I've changed locally for some unrelated work -- thus I have to be careful to avoid checking them in. A "U" would have indicated a file that SVN updated because my local copy was out of date relative to the master sources.

The local repository is now up to date.

### Apply the patch to the local source

>     [/law/gcc/gcc] patch < ~/Mail/gcc/pendingpatches/42
>     Hmm...  Looks like a new-style context diff to me...
>     The text leading up to this was:
>     [ uninteresting text deleted ]
>     |*** bitmap.c.egcs      Sat Dec 20 06:31:11 1997
>     |--- bitmap.c   Sun Feb  8 08:01:32 1998
>     --------------------------
>     Patching file bitmap.c using Plan A...
>     Hunk #1 succeeded at 563.
>     Hunk #2 succeeded at 573.
>     Hmm...  The next patch looks like a new-style context diff to me...
>     The text leading up to this was:
>     --------------------------
>     |*** calls.c.egcs       Sun Feb  8 07:44:02 1998
>     |--- calls.c    Sun Feb  8 08:00:08 1998
>     --------------------------
>     Patching file calls.c using Plan A...
>     Hunk #1 succeeded at 730.
>     Hmm...  The next patch looks like a new-style context diff to me...
>     The text leading up to this was:
>     --------------------------
>     |*** genattrtab.c.egcs  Sun Feb  8 07:44:04 1998
>     |--- genattrtab.c       Sun Feb  8 08:05:36 1998
>     --------------------------
>     Patching file genattrtab.c using Plan A...
>     Hunk #1 succeeded at 506.
>     done

### Add ChangeLog entry by hand

ChangeLog entries should be handled as straight text; patches against ChangeLogs rarely apply correctly.

### Review changes for correctness

The patch and its associated `ChangeLog` entry are in my local tree; now I run `svn diff` on the modified files and review the output, verifying that it only contains the changes we want.

>     [/law/gcc/gcc] svn diff bitmap.c calls.c genattrtab.c

### Update Copyrights

Review the changed files to see if any copyrights need updating, in this particular case all three needed their copyrights updated.

>     [/law/gcc/gcc] vi bitmap.c calls.c genattrtab.c

### Commit the changes to the central repository

>     [/law/gcc/gcc] svn commit ChangeLog bitmap.c calls.c genattrtab.c

My editor starts and I enter the log message; the lines starting with `This line, and those below, will be ignored` are automatically added by SVN and will be automatically removed:

>             * bitmap.c (bitmap_debug_file): HOST_PTR_PRINTF converts a pointer,
>             not a HOST_WIDE_INT.
>
>             * calls.c (expand_call): Change test of expand_inline_function
>             return value to stop compiler warning.
>
>             * genattrtab.c (RTL_HASH): Cast pointer to long, not HOST_WIDE_INT.
>
>     --This line, and those below, will be ignored--
>     M    ChangeLog
>     M    bitmap.c
>     M    calls.c
>     M    genattrtab.c

Now write & quit from the editor, and SVN will start the actual checkin process....

>     Sending     ChangeLog
>     Sending     bitmap.c
>     Sending     calls.c
>     Sending     genattrtab.c
>     Transmitting file data .
>     Committed revision 105933.
>     [/law/gcc/gcc]

And that's it!

* * * * *

## Creating and using branches {#branches}

To create a branch for development, simply copy the trunk directory to a directory in /branches as follows:

>     svn cp svn+ssh://username@gcc.gnu.org/svn/gcc/trunk \
>         svn+ssh://username@gcc.gnu.org/svn/gcc/branches/$BRANCH

Also, please document such branches at the [list of development branches](svn.html#devbranches).

When merging changes from mainline (or another branch) to a branch, do not copy the entire set of ChangeLog entries. Just use something like "Merge from mainline" or similar.

* * * * *

## Tips&Tricks around your account

Your gcc.gnu.org account also receives e-mail (and is what you use for Bugzilla). If you ever need to change the address e-mail to *username*@gcc.gnu.org is forwarded to, you can easily do so using

>     ssh username@gcc.gnu.org email mynewaddress@example.com
