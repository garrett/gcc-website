# GCC and the Translation Project

This page documents how GCC development interacts with the [Translation Project](http://translationproject.org/html/welcome.html).

## `gcc.pot` regeneration {#regen}

`gcc/po/gcc.pot` and `libcpp/po/cpplib.pot` (the template files containing all the messages translators need to translate) are currently regenerated manually.

To regenerate `gcc.pot`:

-   configure and build GCC with `--enable-generated-files-in-srcdir` (this option is only needed for 4.3);
-   `make gcc.pot` in `objdir/gcc/`;
-   move `objdir/gcc/po/gcc.pot` to `srcdir/gcc/po/`.

Similarly, to regenerate `cpplib.pot`:

-   `make cpplib.pot` in `objdir/libcpp/`;
-   move `objdir/libcpp/po/cpplib.pot` to `srcdir/libcpp/po/`.

This is done at the following times on mainline and active release branches:

1.  Just before each release (so the file is current in releases).
2.  At some time during development stage 3 or 4 (so that a snapshot can be submitted to the Translation Project and translators have a chance to catch up before the release).

## `gcc.pot` submission to Translation Project {#submit}

An i18n maintainer submits the `.pot` files from releases to the Translation Project.

In preparation for the first release from a new release branch, a `.pot` file for a snapshot should be [manually submitted](http://translationproject.org/html/maintainers.html). Send a message to <coordinator@translationproject.org>, whose subject is "gcc-*version*-b*date*.pot" (the "b" format gets it ordered correctly before the release proper) and whose body contains the URL of the snapshot `.tar.bz2` file. You need to regenerate the `pot` files on the branch, then wait for a new snapshot to be run containing it (or run one manually, using the command in the crontab, if you have access to the `gccadmin` account), before submitting the snapshot.

## `.po` file collection {#collect}

`.po` files (translated messages) from the translators need to be added to GCC (mainline and active release branch). They are automatically sent (gzipped) to gcc-patches, and should be committed by the i18n maintainer as and when they appear on gcc-patches. But sometimes the i18n maintainer may be busy or otherwise engaged, or a `.po` file may be missed, so, before each release, a check should be made for updated `.po` files that haven't been committed already and the most recent `.po` file for each language should be committed to mainline and branch. The current `.po` files for GCC may be found [at the Translation Project site](http://translationproject.org/domain/gcc.html). cpplib `.po` files may also be found [there](http://translationproject.org/domain/cpplib.html).

## `.po` file maintenance {#maintain}

`.po` files are maintained by the translators. Patches to them must not go directly into GCC; they need to be sent to the respective language team, indicated by the Language-Team entry in the `.po` file or [the list of language teams](http://translationproject.org/team/index.html). Bug reports about a translation should be forwarded to the translation team.
