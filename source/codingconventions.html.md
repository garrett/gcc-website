<html><head><title>GCC Coding Conventions</title></head><body markdown="1">
# GCC Coding Conventions

There are some additional coding conventions for code in GCC, beyond
those in the [GNU Coding Standards][1]. Some existing code may not
follow these conventions, but they must be used for new code. If
changing existing code to follow these conventions, it is best to send
changes to follow the conventions separately from any other changes to
the code.

* [Documentation](#Documentation)
* [ChangeLogs](#ChangeLogs)
* [Portability](#Portability)
* [Makefiles](#Makefiles)
* [Testsuite Conventions](#Testsuite)
* [Diagnostics Conventions](#Diagnostics)
* [Spelling, terminology and markup](#Spelling)
* [C and C++ Language Conventions](#CandCxx)
  * [Compiler Options](#C_Options)
  * [Language Use](#C_Language)
    * [Assertions](#Assertions)
    * [Character Testing](#Character)
    * [Error Node Testing](#Error)
    * [Parameters Affecting Generated Code](#Generated)
    * [Inlining Functions](#C_Inlining)
  
  * [Formatting Conventions](#C_Formatting)
    * [Line Length](#Line)
    * [Names](#C_Names)
    * [Expressions](#Expressions)

* [C++ Language Conventions](#Cxx_Conventions)
  * [Language Use](#Cxx_Language)
    * [Variable Definitions](#Variable)
    * [Struct Definitions](#Struct_Use)
    * [Class Definitions](#Class_Use)
    * [Constructors and Destructors](#Constructors)
    * [Conversions](#Conversions)
    * [Overloading Functions](#Over_Func)
    * [Overloading Operators](#Over_Oper)
    * [Default Arguments](#Default)
    * [Inlining Functions](#Cxx_Inlining)
    * [Templates](#Template_Use)
    * [Namespaces](#Namespace_Use)
    * [RTTI and `dynamic_cast`](#RTTI)
    * [Other Casts](#Casts)
    * [Exceptions](#Exceptions)
    * [The Standard Library](#Standard_Library)
  
  * [Formatting Conventions](#Cxx_Formatting)
    * [Names](#Cxx_Names)
    * [Struct Definitions](#Struct_Form)
    * [Class Definitions](#Class_Form)
    * [Class Member Definitions](#Member_Form)
    * [Templates](#Template_Form)
    * [Extern "C"](#ExternC)
    * [Namespaces](#Namespace_Form)

## <a name="Documentation">Documentation</a>

Documentation, both of user interfaces and of internals, must be
maintained and kept up to date. In particular:

* All command-line options (including all `--param` arguments) must be
  documented in the GCC manual.
* Any change to documented behavior (for example, the behavior of a
  command-line option or a GNU language extension) must include the
  necessary changes to the manual.
* All target macros must be documented in the GCC manual.
* The documentation of the `tree` and RTL data structures and interfaces
  must be kept complete and up to date.
* In general, the documentation of all documented aspects of the
  front-end and back-end interfaces must be kept up to date, and the
  opportunity should be taken where possible to remedy gaps in or
  limitations of the documentation.

## <a name="ChangeLogs">ChangeLogs</a>

GCC requires ChangeLog entries for documentation changes; for the web
pages (apart from `java/` and `libstdc++/`) the CVS commit logs are
sufficient.

See also what the [GNU Coding Standards][1] have to say about what goes
in ChangeLogs; in particular, descriptions of the purpose of code and
changes should go in comments rather than the ChangeLog, though a single
line overall description of the changes may be useful above the
ChangeLog entry for a large batch of changes.

For changes that are ported from another branch, we recommend to use a
single entry whose body contains a verbatim copy of the original entries
describing the changes on that branch, possibly preceded by a
single-line overall description of the changes.

There is no established convention on when ChangeLog entries are to be
made for testsuite changes; see messages [1][2] and [2][3].

If your change fixes a PR, put `PR java/58` (where `java/58` is the
actual number of the PR) at the top of the ChangeLog entry.

## <a name="Portability">Portability</a>

There are strict requirements for portability of code in GCC to older
systems whose compilers do not implement all of the latest ISO C and C++
standards.

The directories `gcc`, `libcpp` and `fixincludes` may use C++03. They
may also use the `long long` type if the host C++ compiler supports it.
These directories should use reasonably portable parts of C++03, so that
it is possible to build GCC with C++ compilers other than GCC itself. If
testing reveals that reasonably recent versions of non-GCC C++ compilers
cannot compile GCC, then GCC code should be adjusted accordingly.
(Avoiding unusual language constructs helps immensely.) Furthermore,
these directories *should* also be compatible with C++11.

The directories libiberty and libdecnumber must use C and require at
least an ANSI C89 or ISO C90 host compiler. C code should avoid
pre-standard style function definitions, unnecessary function prototypes
and use of the now deprecated `PARAMS` macro. See
[README.Portability][4] for details of some of the portability problems
that may arise. Some of these problems are warned about by `gcc
-Wtraditional`, which is included in the default warning options in a
bootstrap.

The programs included in GCC are linked with the libiberty library,
which will replace some standard library functions if not present on the
system used, so those functions may be freely used in GCC. In
particular, the ISO C string functions `memcmp`, `memcpy`, `memmove`,
`memset`, `strchr` and `strrchr` are preferred to the old functions
`bcmp`, `bcopy`, `bzero`, `index` and `rindex`; see messages [1][5] and
[2][6]. The older functions must no longer be used in GCC; apart from
`index`, these identifiers are poisoned to prevent their use.

Machine-independent files may contain conditionals on features of a
particular system, but should never contain conditionals such as `#ifdef
__hpux__` on the name or version of a particular system. Exceptions may
be made to this on a release branch late in the release cycle, to reduce
the risk involved in fixing a problem that only shows up on one
particular system.

Function prototypes for extern functions should only occur in header
files. Functions should be ordered within source files to minimize the
number of function prototypes, by defining them before their first use.
Function prototypes should only be used when necessary, to break
mutually recursive cycles.

## <a name="Makefiles">Makefiles</a>

`touch` should never be used in GCC Makefiles. Instead of `touch foo`
always use `$(STAMP) foo`.

## <a name="Testsuite">Testsuite Conventions</a>

Every language or library feature, whether standard or a GNU extension,
and every warning GCC can give, should have testcases thoroughly
covering both its specification and its implementation. Every bug fixed
should have a testcase to detect if the bug recurs.

The testsuite READMEs discuss the requirement to use `abort
()` for runtime failures and `exit (0)` for success. For compile-time
tests, a trick taken from autoconf may be used to evaluate expressions:
a declaration `extern char x[(EXPR) ? 1 :
-1];` will compile successfully if and only if `EXPR` is nonzero.

Where appropriate, testsuite entries should include comments giving
their origin: the people who added them or submitted the bug report they
relate to, possibly with a reference to a PR in our bug tracking system.
There are [some copyright guidelines][7] on what can be included in the
testsuite.

If a testcase itself is incorrect, but there's a possibility that an
improved testcase might fail on some platform where the incorrect
testcase passed, the old testcase should be removed and a new testcase
(with a different name) should be added. This helps automated
regression-checkers distinguish a true regression from an improvement to
the test suite.

## <a name="Diagnostics">Diagnostics Conventions</a>

* Use of the `input_location` global, and of the diagnostic functions
  that implicitly use `input_location`, is deprecated; the preferred
  technique is to pass around locations ultimately derived from the
  location of some explicitly chosen source code token.
* Diagnostics using the GCC diagnostic functions should generally use
  the GCC-specific formats such as `%qs` or `%<` and `%>` for quoting
  and `%m` for `errno` numbers.
* Identifiers should generally be formatted with `%E` or `%qE`; use of
  `identifier_to_locale` is needed if the identifier text is used
  directly.
* Formats such as `%wd` should be used with types such as
  `HOST_WIDE_INT` (`HOST_WIDE_INT_PRINT_DEC` is a format for the host
  `printf` functions, not for the GCC diagnostic functions).
* `error` is for defects in the user's code.
* `sorry` is for correct user input programs but unimplemented
  functionalities.
* `warning` is for advisory diagnostics; it may be used for diagnostics
  that have severity less than an error.
* `inform` is for adding additional explanatory information to a
  diagnostic.
* `internal_error` is used for conditions that should not be triggered
  by any user input whether valid or invalid and including invalid asms
  and LTO binary data (sometimes, as an exception, there is a call to
  `error` before further information is printed and an ICE is
  triggered). Assertion failures should not be triggered by invalid
  input.
* `inform` is for informative notes accompanying errors and warnings.
* All diagnostics should be full sentences without English fragments
  substituted in them, to facilitate translation.

## <a name="Spelling">Spelling, terminology and markup</a>

The following conventions of spelling and terminology apply throughout
GCC, including the manuals, web pages, diagnostics, comments, and
(except where they require spaces or hyphens to be used) function and
variable names, although consistency in user-visible documentation and
diagnostics is more important than that in comments and code. The
following table lists some simple cases:

<table cellpadding="4">
  <tr>
    <th>Use...</th><th>...instead of</th><th>Rationale</th>
  </tr>
  <tr>
    <td>American spelling (in particular -ize, -or)</td>
    <td>British spelling (in particular -ise, -our)</td>
    <td />
  </tr>
  <tr>
    <td>"32-bit" (adjective)</td>
    <td>"32 bit"</td>
    <td />
  </tr>
  <tr>
    <td>"alphanumeric"</td>
    <td>"alpha numeric"</td>
    <td />
  </tr>
  <tr>
    <td>"back end" (noun)</td>
    <td>"back-end" or "backend"</td>
    <td />
  </tr>
  <tr>
    <td>"back-end" (adjective)</td>
    <td>"back end" or "backend"</td>
    <td />
  </tr>
  <tr>
    <td>"bit-field"</td>
    <td>"bit field" or "bitfield"</td>
    <td>Spelling used in C and C++ standards</td>
  </tr>
  <tr>
    <td>"built-in" as an adjective ("built-in function") or "built in"</td>
    <td>"builtin"</td>
    <td>"builtin" isn't a word</td>
  </tr>
  <tr>
    <td>"bug fix" (noun) or "bug-fix" (adjective)</td>
    <td>"bugfix" or "bug-fix"</td>
    <td>"bugfix" isn't a word</td>
  </tr>
  <tr>
    <td>"ColdFire"</td>
    <td>"coldfire" or "Coldfire"</td>
    <td />
  </tr>
  <tr>
    <td>"command-line option"</td>
    <td>"command line option"</td>
    <td />
  </tr>
  <tr>
    <td>"compilation time" (noun);
      how long it takes to compile the program</td>
    <td>"compile time"</td>
    <td />
  </tr>
  <tr>
    <td>"compile time" (noun), "compile-time" (adjective);
      the time at which the program is compiled</td>
    <td />
    <td />
  </tr>
  <tr>
    <td>"dependent" (adjective), "dependence", "dependency"</td>
    <td>"dependant", "dependance", "dependancy"</td>
    <td />
  </tr>
  <tr>
    <td>"enumerated"</td>
    <td>"enumeral"</td>
    <td>Terminology used in C and C++ standards</td>
  </tr>
  <tr>
    <td>"epilogue"</td>
    <td>"epilog"</td>
    <td>Established convention</td>
  </tr>
  <tr>
    <td>"execution time" (noun);
      how long it takes the program to run</td>
    <td>"run time" or "runtime"</td>
    <td />
  </tr>
  <tr>
    <td>"floating-point" (adjective)</td>
    <td>"floating point"</td>
    <td />
  </tr>
  <tr>
    <td>"free software" or just "free"</td>
    <td>"Open Source" or "OpenSource"</td>
  </tr>
  <tr>
    <td>"front end" (noun)</td>
    <td>"front-end" or "frontend"</td>
    <td />
  </tr>
  <tr>
    <td>"front-end" (adjective)</td>
    <td>"front end" or "frontend"</td>
    <td />
  </tr>
  <tr>
    <td>"GNU/Linux" (except in reference to the kernel)</td>
    <td>"Linux" or "linux" or "Linux/GNU"</td>
    <td />
  </tr>
  <tr>
    <td>"link time" (noun), "link-time" (adjective);
      the time at which the program is linked</td>
    <td />
    <td />
  </tr>
  <tr>
    <td>"lowercase"</td>
    <td>"lower case" or "lower-case"</td>
    <td />
  </tr>
  <tr>
    <td>"H8S"</td>
    <td>"H8/S"</td>
    <td />
  </tr>
  <tr>
    <td>"Microsoft Windows"</td>
    <td>"Windows"</td>
    <td />
  </tr>
  <tr>
    <td>"MIPS"</td>
    <td>"Mips" or "mips"</td>
    <td />
  </tr>
  <tr>
    <td>"nonzero"</td>
    <td>"non-zero" or "non zero"</td>
    <td />
  </tr>
  <tr>
    <td>"Objective-C"</td>
    <td>"Objective C"</td>
  </tr>
  <tr>
    <td>"prologue"</td>
    <td>"prolog"</td>
    <td>Established convention</td>
  </tr>
  <tr>
    <td>"PowerPC"</td>
    <td>"powerpc", "powerPC" or "PowerPc"</td>
  </tr>
  <tr>
    <td>"Red Hat"</td>
    <td>"RedHat" or "Redhat"</td>
    <td />
  </tr>
  <tr>
    <td>"run time" (noun), "run-time" (adjective);
      the time at which the program is run</td>
    <td>"runtime"</td>
    <td />
  </tr>
  <tr>
    <td>"runtime" (both noun and adjective);
      libraries and system support present at run time</td>
    <td>"run time", "run-time"</td>
    <td />
  </tr>
  <tr>
    <td>"SPARC"</td>
    <td>"Sparc" or "sparc"</td>
  </tr>
  <tr>
    <td>"testcase", "testsuite"</td>
    <td>"test-case" or "test case", "test-suite" or "test suite"</td>
  </tr>
  <tr>
    <td>"uppercase"</td>
    <td>"upper case" or "upper-case"</td>
    <td />
  </tr>
  <tr>
    <td>"VAX", "VAXen", "MicroVAX"</td>
    <td>"vax" or "Vax", "vaxen" or "vaxes", "microvax" or "microVAX"</td>
  </tr>
</table>
"GCC" should be used for the GNU Compiler Collection, both generally and
as the GNU C Compiler in the context of compiling C; "G++" for the C++
compiler; "gcc" and "g++" (lowercase), marked up with `@command` when in
Texinfo, for the commands for compilation when the emphasis is on those;
"GNU C" and "GNU C++" for language dialects; and try to avoid the older
term "GNU CC".

Use a comma after "e.g." or "i.e." if and only if it is appropriate in
the context and the slight pause a comma means helps the reader; do not
add them automatically in all cases just because some style guides say
so. (In Texinfo manuals, `@:` should to be used after "e.g." and "i.e."
when a comma isn't used.)

In Texinfo manuals, Texinfo 4.0 features may be used, and should be used
where appropriate. URLs should be marked up with `@uref`; email
addresses with `@email`; command-line options with `@option`; names of
commands with `@command`; environment variables with `@env`. NULL should
be written as `@code{NULL}`. Tables of contents should come just after
the title page; printed manuals will be formatted (for example, by `make
dvi`) using `texi2dvi` which reruns TeX until cross-references
stabilize, so there is no need for a table of contents to go at the end
for it to have correct page numbers. The `@refill` feature is obsolete
and should not be used. All manuals should use `@dircategory` and
`@direntry` to provide Info directory information for `install-info`.

It is useful to read the [Texinfo manual][8]. Some general Texinfo style
issues discussed in that manual should be noted:

* For proper formatting of the printed manual, TeX quotes (matched `` `
  `` or ``` `` ``` and `'` or `''`) should be used; neutral ASCII double
  quotes (`"..."`) should not be. Similarly, TeX dashes (`--` (two
  hyphens) for an en dash and `---` (three hyphens) for an em dash)
  should be used; normally these dashes should not have whitespace on
  either side. Minus signs should be written as `@minus{}`.
* For an ellipsis, `@dots{}` should be used; for a literal sequence of
  three dots in a programming language, the dots should be written as
  such (`...`) rather than as `@dots{}`.
* English text in programming language comments in examples should be
  enclosed in `@r{}` so that it is printed in a non-fixed-width font.
* Full stops that end sentences should be followed by two spaces or by
  end of line. Full stops that are preceded by a lower-case letter but
  do not end a sentence should be followed by `@:` if they are not
  followed by other punctuation such as a comma; full stops, question
  marks and exclamation marks that end a sentence but are preceded by an
  upper-case letter should be written as "`@.`", "`@?`" and "`@!`",
  respectively. (This is not required if the capital letter is within
  `@code` or `@samp`.)

## Upstream packages   {#upstream}

Some files and packages in the GCC source tree are imported from
elsewhere, and we want to minimize divergence from their upstream
sources. The following files should be updated only according to the
rules set below:

* config.guess, config.sub: The master copy of these files is at
  [ftp://ftp.gnu.org/pub/gnu/config][9]. Proposed changes should be
  e-mailed to [config-patches@gnu.org](mailto:config-patches@gnu.org).
  Only after the change makes it to the FTP site should the new files be
  installed in the GCC source tree, so that their version numbers remain
  meaningful and unique. Don't install the patch, install the whole
  file.
* ltmain.sh, libtool.m4, ltoptions.m4, ltsugar.m4, ltversion.m4,
  lt~obsolete.m4, and formerly also ltconfig, ltcf-c.sh, ltcf-cxx.sh,
  ltcf-gcj.sh: The master copy of these files is the source repository
  of [GNU libtool][10]. Patches should be posted to
  [libtool-patches@gnu.org](mailto:libtool-patches@gnu.org). Only after
  the change makes it to the libtool source tree should the new files be
  installed in the GCC source tree.  
   ltgcc.m4 is not imported from upstream.  
   ltconfig and ltmain.sh are generated files from ltconfig.in and
  ltmain.in, respectively, and with libtool 2.1, the latter is generated
  from ltmain.m4sh, so, when you post the patch, make sure you're
  patching the source file, not the generated one. When you update these
  generated files in the GCC repository, make sure they report the same
  timestamp and version number, and note this version number in the
  ChangeLog.
* Top-level configure.ac, configure, Makefile.in, config-ml.in,
  config.if and most other top-level shell-scripts: Please try to keep
  these files in sync with the corresponding files in the src repository
  at sourceware.org. Some people hope to eventually merge these trees
  into a single repository; keeping them in sync helps this goal. When
  you check in a patch to one of these files, please check it in the src
  tree too, or ask someone else with write access there to do so.
* Top-level compile, depcomp, install-sh, missing, mkinstalldirs,
  symlink-tree and ylwrap: These are copied from mainline automake,
  using `automake --add-missing --copy --force-missing`.
* move-if-change: gnulib hosts this now. The latest version can be
  retrieved from
  [http://git.savannah.gnu.org/cgit/gnulib.git/plain/build-aux/move-if-change][11].
* Top-level config.rpath: This comes originally from gettext, but gnulib
  can be considered upstream. The latest version can be retrieved from
  [http://git.savannah.gnu.org/cgit/gnulib.git/plain/build-aux/config.rpath][12].
  Contents should be kept in sync with relevant parts of libtool.m4 from
  Libtool.
* gcc/doc/include/texinfo.tex: This file is copied from texinfo, the
  latest version is at
  [http://git.savannah.gnu.org/cgit/gnulib.git/plain/build-aux/texinfo.tex][13].
* gcc/config/soft-fp: The master sources (except for `t-softfp`) are in
  git glibc, and changes should go there before going into GCC.
* fastjar: The master sources were at [fastjar.sourceforge.net][14].
  However, the upstream source seems to be dead, so fastjar is
  essentially maintained in the GCC source tree.
* boehm-gc: The master sources are at
  [www.hpl.hp.com/personal/Hans_Boehm/gc/][15]. Patches should be sent
  to [boehm@acm.org](mailto:boehm@acm.org), but it's acceptable to check
  them in the GCC source tree before getting them installed in the
  master tree.
* libjava/classpath: The master sources come from [GNU Classpath][16].
  New versions of Classpath are periodically imported into the GCC
  source tree. In general local modifications are prohibited, but they
  can be checked in for emergencies, such as fixing bootstrap.
* zlib: The master sources come from [zlib.net][17]. However, the
  autoconf-based configury is a local GCC invention. Changes to zlib
  outside the build system are discouraged, and should be sent upstream
  first.
* libstdc++-v3: In docs/doxygen, comments in *.cfg.in are partially
  autogenerated from [the Doxygen tool][18]. In docs/html, the ext/lwg-*
  files are copied from [the C++ committee homepage][19], the
  27_io/binary_iostream_* files are copies of Usenet postings, and most
  of the files in 17_intro are either copied from elsewhere in GCC or
  the FSF website, or are autogenerated. These files should not be
  changed without prior permission, if at all.
* libgcc/config/libbid: The master sources come from Intel BID library
  [Intel BID library][20]. Bugs should be reported to
  [marius.cornea@intel.com](mailto:marius.cornea@intel.com) and
  [hongjiu.lu@intel.com](mailto:hongjiu.lu@intel.com). Changes to libbid
  outside the build system are discouraged, and should be sent upstream
  first.

## <a name="CandCxx">C and C++ Language Conventions</a>

The following conventions apply to both C and C++.

### <a name="C_Options">Compiler Options</a>

The compiler must build cleanly with `-Wall -Wextra`.

### <a name="C_Language">Language Use</a>

#### <a name="Assertions">Assertions</a>

Code should use `gcc_assert (EXPR)` to check invariants. Use
`gcc_unreachable ()` to mark places that should never be reachable (such
as an unreachable `default` case of a switch). Do not use `gcc_assert
(0)` for such purposes, as `gcc_unreachable` gives the compiler more
information. The assertions are enabled unless explicitly configured off
with `--enable-checking=none`. Do not use `abort`. User input should
never be validated by either `gcc_assert` or `gcc_unreachable`. If the
checks are expensive or the compiler can reasonably carry on after the
error, they may be conditioned on `--enable-checking` by using
`gcc_checking_assert`.

#### <a name="Character">Character Testing</a>

Code testing properties of characters from user source code should use
macros such as `ISALPHA` from `safe-ctype.h` instead of the standard
functions such as `isalpha` from `<ctype.h>` to avoid any
locale-dependency of the language accepted.

#### <a name="Error">Error Node Testing</a>

Testing for `ERROR_MARK`s should be done by comparing against
`error_mark_node` rather than by comparing the `TREE_CODE` against
`ERROR_MARK`; see [message][21].

#### <a name="Generated">Parameters Affecting Generated Code</a>

Internal numeric parameters that may affect generated code should be
controlled by `--param` rather than being hardcoded.

#### <a name="C_Inlining">Inlining Functions</a>

Inlining functions only when you have reason to believe that the
expansion of the function is smaller than a call to the function or that
inlining is significant to the run-time of the compiler.

### <a name="C_Formatting">Formatting Conventions</a>

#### <a name="Line">Line Length</a>

Lines shall be at most 80 columns.

#### <a name="C_Names">Names</a>

Macros names should be in `ALL_CAPS` when it's important to be aware
that it's a macro (e.g. accessors and simple predicates), but in
lowercase (e.g., `size_int`) where the macro is a wrapper for efficiency
that should be considered as a function; see messages [1][22] and
[2][23].

Other names should be lower-case and separated by low_lines.

#### <a name="Expressions">Expressions</a>

Code in GCC should use the following formatting conventions:

<table cellpadding="4">
<tr>
  <th align="left">For</th>
  <th align="right">Use...</th><th align="left">...instead of</th>
</tr><tr>
  <td align="left">logical not</td>
  <td align="right"><code>!x</code></td><td><code>! x</code></td>
</tr><tr>
  <td align="left">bitwise complement</td>
  <td align="right"><code>~x</code></td><td><code>~ x</code></td>
</tr><tr>
  <td align="left">unary minus</td>
  <td align="right"><code>-x</code></td><td><code>- x</code></td>
</tr><tr>
  <td align="left">cast</td>
  <td align="right"><code>(foo) x</code></td>
  <td><code>(foo)x</code></td>
</tr><tr>
  <td align="left">pointer dereference</td>
  <td align="right"><code>*x</code></td>
  <td><code>* x</code></td>
</tr>
</table>
## <a name="Cxx_Conventions">C++ Language Conventions</a>

The following conventions apply only to C++.

These conventions will change over time, but changing them requires a
convincing rationale.

### <a name="Cxx_Language">Language Use</a>

C++ is a complex language, and we strive to use it in a manner that is
not surprising. So, the primary rule is to be reasonable. Use a language
feature in known good ways. If you need to use a feature in an unusual
way, or a way that violates the "should" rules below, seek guidance,
review and feedback from the wider community.

All use of C++ features is subject to the decisions of the maintainers
of the relevant components. (This restates something that is always true
for gcc, which is that component maintainers make the final decisions
about those components.)

#### <a name="Variable">Variable Definitions</a>

Variables should be defined at the point of first use, rather than at
the top of the function. The existing code obviously does not follow
that rule, so variables may be defined at the top of the function, as in
C90.

Variables may be simultaneously defined and tested in control
expressions.

[Rationale and Discussion](codingrationale.html#variables)

#### <a name="Struct_Use">Struct Definitions</a>

Use the `struct` keyword for [ plain old data (POD)][24] types.

[Rationale and Discussion](codingrationale.html#struct)

#### <a name="Class_Use">Class Definitions</a>

Use the `class` keyword for [ non-POD][24] types.

A non-POD type will often (but not always) have a declaration of a [
special member function][25]. If any one of these is declared, then all
should be either declared or have an explicit comment saying that the
default is intended.

Single inheritance is permitted. Use public inheritance to describe
interface inheritance, i.e. 'is-a' relationships. Use private and
protected inheritance to describe implementation inheritance.
Implementation inheritance can be expedient, but think twice before
using it in code intended to last a long time.

Complex hierarchies are to be avoided. Take special care with multiple
inheritance. On the rare occasion that using mulitple inheritance is
indeed useful, prepare design rationales in advance, and take special
care to make documentation of the entire hierarchy clear.

Think carefully about the size and performance impact of virtual
functions and virtual bases before using them.

Prefer to make data members private.

[Rationale and Discussion](codingrationale.html#class)

#### <a name="Constructors">Constructors and Destructors</a>

All constructors should initialize data members in the member
initializer list rather than in the body of the constructor.

A class with virtual functions or virtual bases should have a virtual
destructor.

[Rationale and Discussion](codingrationale.html#constructors)

#### <a name="Conversions">Conversions</a>

Single argument constructors should nearly always be declared explicit.

Conversion operators should be avoided.

[Rationale and Discussion](codingrationale.html#conversions)

#### <a name="Over_Func">Overloading Functions</a>

Overloading functions is permitted, but take care to ensure that
overloads are not surprising, i.e. semantically identical or at least
very similar. Virtual functions should not be overloaded.

[Rationale and Discussion](codingrationale.html#overfunc)

#### <a name="Over_Oper">Overloading Operators</a>

Overloading operators is permitted, but take care to ensure that
overloads are not surprising. Some unsurprising uses are in the
implementation of numeric types and in following the C++ Standard
Library's conventions. In addition, overloaded operators, excepting the
call operator, should not be used for expensive implementations.

[Rationale and Discussion](codingrationale.html#overoper)

#### <a name="Default">Default Arguments</a>

Default arguments are another type of function overloading, and the same
rules apply. Default arguments must always be POD values, i.e. may not
run constructors. Virtual functions should not have default arguments.

[Rationale and Discussion](codingrationale.html#default)

#### <a name="Cxx_Inlining">Inlining Functions</a>

Constructors and destructors, even those with empty bodies, are often
much larger than programmers expect. Prefer non-inline versions unless
you have evidence that the inline version is smaller or has a
significant performance impact.

#### <a name="Template_Use">Templates</a>

To avoid excessive compiler size, consider implementing non-trivial
templates on a non-template base class with `void*` parameters.

#### <a name="Namespace_Use">Namespaces</a>

Namespaces are encouraged. All separable libraries should have a unique
global namespace. All individual tools should have a unique global
namespace. Nested include directories names should map to nested
namespaces when possible.

Header files should have neither `using` directives nor namespace-scope
`using` declarations.

[Rationale and Discussion](codingrationale.html#namespaces)

#### <a name="RTTI">RTTI and <code>dynamic_cast</code></a>

Run-time type information (RTTI) is permitted when certain non-default
`--enable-checking` options are enabled, so as to allow checkers to
report dynamic types. However, by default, RTTI is not permitted and the
compiler must build cleanly with `-fno-rtti`.

[Rationale and Discussion](codingrationale.html#RTTI)

#### <a name="Casts">Other Casts</a>

C-style casts should not be used. Instead, use C++-style casts.

[Rationale and Discussion](codingrationale.html#casts)

#### <a name="Exceptions">Exceptions</a>

Exceptions and throw specifications are not permitted and the compiler
must build cleanly with `-fno-exceptions`.

[Rationale and Discussion](codingrationale.html#exceptions)

#### <a name="Standard_Library">The Standard Library</a>

Use of the standard library is permitted. Note, however, that it is
currently not usable with garbage collected data.

For compiler messages, indeed any text that needs i18n, should continue
to use the existing facilities.

For long-term code, at least for now, we will continue to use `printf`
style I/O rather than `<iostream>` style I/O.

[Rationale and Discussion](codingrationale.html#stdlib)

### <a name="Cxx_Formatting">Formatting Conventions</a>

#### <a name="Cxx_Names">Names</a>

When structs and/or classes have member functions, prefer to name data
members with a leading `m_` and static data members with a leading `s_`.

Template parameter names should use CamelCase, following the C++
Standard.

[Rationale and Discussion](codingrationale.html#stdlib)

#### <a name="Struct_Form">Struct Definitions</a>

Note that the rules for classes do not apply to structs. Structs
continue to behave as before.

#### <a name="Class_Form">Class Definitions</a>

If the entire class definition fits on a single line, put it on a single
line. Otherwise, use the following rules.

Do not indent protection labels.

Indent class members by two spaces.

Prefer to put the entire class head on a single line.

>     
>     class gnuclass : base {

Otherwise, start the colon of the base list at the beginning of a line.

>     
>     class a_rather_long_class_name 
>     : with_a_very_long_base_name, and_another_just_to_make_life_hard
>     { 
>       int member; 
>     };

If the base clause exceeds one line, move overflowing initializers to
the next line and indent by two spaces.

>     
>     class gnuclass 
>     : base1 <template_argument1>, base2 <template_argument1>,
>       base3 <template_argument1>, base4 <template_argument1>
>     { 
>       int member; 
>     };

When defining a class,

* first define all public types,
* then define all non-public types,
* then declare all public constructors,
* then declare the public destructor,
* then declare all public member functions,
* then declare all public member variables,
* then declare all non-public constructors,
* then declare the non-public destructor,
* then declare all non-public member functions, and
* then declare all non-public member variables.

Semantic constraints may require a different declaration order, but seek
to minimize the potential confusion.

Close a class definition with a right brace, semicolon, optional closing
comment, and a new line.

>     
>     }; // class gnuclass

#### <a name="Member_Form">Class Member Definitions</a>

Define all members outside the class definition. That is, there are no
function bodies or member initializers inside the class definition.

Prefer to put the entire member head on a single line.

>     
>     gnuclass::gnuclass () : base_class ()
>     { 
>       ...
>     };

When that is not possible, place the colon of the initializer clause at
the beginning of a line.

>     
>     gnuclass::gnuclass ()
>     : base1 (), base2 (), member1 (), member2 (), member3 (), member4 ()
>     { 
>       ...
>     };

If the initializer clause exceeds one line, move overflowing
initializers to the next line and indent by two spaces.

>     
>     gnuclass::gnuclass ()
>     : base1 (some_expression), base2 (another_expression),
>       member1 (my_expressions_everywhere)
>     { 
>       ...
>     };

If a C++ function name is long enough to cause the first function
parameter with its type to exceed 80 characters, it should appear on the
next line indented four spaces.

>     
>     void
>     very_long_class_name::very_long_function_name (
>         very_long_type_name arg)
>     {

Sometimes the class qualifier and function name together exceed 80
characters. In this case, break the line before the `::` operator. We
may wish to do so pre-emptively for all class member functions.

>     
>     void
>     very_long_template_class_name <with, a, great, many, arguments>
>     ::very_long_function_name (
>         very_long_type_name arg)
>     {

#### <a name="Template_Form">Templates</a>

A declaration following a template parameter list should not have
additional indentation.

Prefer `typename` over `class` in template parameter lists.

#### <a name="ExternC">Extern "C"</a>

Prefer an `extern "C"` block to a declaration qualifier.

Open an `extern "C"` block with the left brace on the same line.

>     
>     extern "C" {

Close an `extern "C"` block with a right brace, optional closing
comment, and a new line.

>     
>     } // extern "C"

Definitions within the body of a namespace are not indented.

#### <a name="Namespace_Form">Namespaces</a>

Open a namespace with the namespace name followed by a left brace and a
new line.

>     
>     namespace gnutool {

Close a namespace with a right brace, optional closing comment, and a
new line.

>     
>     } // namespace gnutool

Definitions within the body of a namespace are not indented.

</body></html>



[1]: http://www.gnu.org/prep/standards_toc.html
[2]: http://gcc.gnu.org/ml/gcc/2000-09/msg00287.html
[3]: http://gcc.gnu.org/ml/gcc/2000-09/msg00290.html
[4]: http://gcc.gnu.org/cgi-bin/cvsweb.cgi/~checkout~/gcc/gcc/README.Portability?content-type=text/plain&amp;only_with_tag=HEAD
[5]: http://gcc.gnu.org/ml/gcc/1998-09/msg01000.html
[6]: http://gcc.gnu.org/ml/gcc/1998-09/msg01127.html
[7]: http://gcc.gnu.org/ml/gcc/2000-01/msg00593.html
[8]: http://www.gnu.org/software/texinfo/manual/texinfo/
[9]: ftp://ftp.gnu.org/pub/gnu/config/
[10]: http://www.gnu.org/software/libtool/libtool.html
[11]: http://git.savannah.gnu.org/cgit/gnulib.git/plain/build-aux/move-if-change
[12]: http://git.savannah.gnu.org/cgit/gnulib.git/plain/build-aux/config.rpath
[13]: http://git.savannah.gnu.org/cgit/gnulib.git/plain/build-aux/texinfo.tex
[14]: http://fastjar.sourceforge.net/
[15]: http://www.hpl.hp.com/personal/Hans_Boehm/gc/
[16]: http://www.gnu.org/software/classpath/
[17]: http://www.zlib.net/
[18]: http://www.doxygen.org/
[19]: http://www.open-std.org/jtc1/sc22/wg21/
[20]: http://www.netlib.org/misc/intel/
[21]: http://gcc.gnu.org/ml/gcc-patches/2000-10/msg00792.html
[22]: http://gcc.gnu.org/ml/gcc-patches/2000-09/msg00901.html
[23]: http://gcc.gnu.org/ml/gcc-patches/2000-09/msg00912.html
[24]: http://en.wikipedia.org/wiki/Plain_old_data_structure
[25]: http://en.wikipedia.org/wiki/Special_member_functions
