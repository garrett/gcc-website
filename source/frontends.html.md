# GCC Front Ends

Currently the main GCC distribution contains front ends for C (gcc), C++ (g++), Objective C, Fortran, Java ([GCJ](java/)), Ada (GNAT), and Go.

There are several more front ends for different languages that have been written for GCC but not yet integrated into the main distribution of the GNU Compiler Collection. Some of these may be integrated in future; others may not, for various reasons.

Example front ends for toy languages and guidance on writing front ends are listed along with [other links and readings](readings.html). The source files `tree.h` and `tree.def` are the key ones to be familiar with.

Some of these front ends are very much works in progress; others are very mature.

-   [GNU Pascal Compiler](http://www.gnu-pascal.de/gpc/h-index.html) (GPC).
-   [Mercury](http://www.mercurylang.org/download/gcc-backend.html), a declarative logic/functional language. The University of Melbourne Mercury compiler is written in Mercury; originally it compiled via C but now it also has a back end that generates assembler directly, using the GCC back end.
-   [Cobol For GCC](http://CobolForGCC.sourceforge.net/) (at an early stage of development).
-   [GNU Modula-2](http://www.nongnu.org/gm2/) implements the PIM2, PIM3, PIM4 and ISO dialects of the language. The compiler is fully operational with the GCC 4.1.2 back end (on GNU/Linux x86 systems). Work is in progress to move the frontend to the GCC trunk. The frontend is mostly written in Modula-2, but includes a bootstrap procedure via a heavily modified version of p2c.
-   Modula-3 (for links see [www.modula3.org](http://www.modula3.org/)); SRC M3 is based on an old version of GCC and PM3 and CAM3 derive from SRC M3. This compiler is written in Modula-3; for copyright and licensing reasons neither the small amount of C code that links to GCC and provides the interface to the back end, nor the front end proper, is likely to be integrated in GCC, nor is the front-end likely to change to a more normal interface of linking directly to the back end.
-   [GHDL](http://ghdl.free.fr) is a GCC front end for the VHDL (IEEE 1076) hardware design language. GHDL and its runtime library are written in Ada95 using GNAT and are distributed under the GPL. Currently they only support GNU/Linux x86 systems.
-   [PL/1 for GCC](http://pl1gcc.sourceforge.net/) is a GCC front end for the PL/I language.
-   [GCC Unified Parallel C](http://www.gccupc.org/) (GCC UPC) is a compilation and execution environment for Unified Parallel C.

