2014-09-02
: # Cilk Plus support in GCC
: Complete support for [Cilk Plus](http://cilk.org/) features was added to GCC. Contributed by Jakub Jelinek, Aldy Hernandez, Balaji V. Iyer and Igor Zamyatin.

2014-08-13
: # [New GCC version numbering scheme](/develop.html#num_scheme) announced

2014-07-16
: # GCC 4.9.1 released

2014-07-05
: # Fortran IEEE intrinsic modules
: The Fortran compiler (gfortran) has gained support for the IEEE intrinsic modules specified by the Fortran 2003 and Fortran 2008 standards. The code was contributed by François-Xavier Coudert of CNRS.

2014-06-30
: # OpenMP v4.0
: An implementation of the [OpenMP v4.0](http://www.openmp.org/mp-documents/OpenMP4.0.0.pdf) parallel programming interface for Fortran has been added and is going to be available in the upcoming GCC 4.9.1 release.

2014-06-12
: [GCC 4.7.4](gcc-4.7/) released

2014-06-10
: [ACM SIGPLAN Programming Languages Software Award](http://www.sigplan.org/node/231)

2014-05-22
: [GCC 4.8.3](gcc-4.8/) released

2014-04-22
: [GCC 4.9.0](gcc-4.9/) released

2014-02-24
: # GCC Google Summer of Code 2014
: GCC has been accepted as a [Google Summer of Code 2014 project](http://www.google-melange.com/gsoc/org2/google/gsoc2014/gcc). Students, mentors and project ideas welcome!

2014-02-17
: # Intel AVX-512 support
: Intel AVX-512 support was added to GCC. That includes inline assembly support, new registers and extending existing ones, new intrinsics, and basic autovectorization. Code was contributed by Sergey Guriev, Alexander Ivchenko, Maxim Kuznetsov, Sergey Lega, Anna Tikhonova, Ilya Tocar, Andrey Turetskiy, Ilya Verbin, Kirill Yukhin and Michael Zolotukhin of Intel, Corp.

2013-12-31
: # Altera Nios II support
: A port for Altera Nios II has been contributed by Mentor Graphics.

2013-11-24
: # Toolchain Build Robot
: The [Build Robot](http://toolchain.lug-owl.de/buildbot/) is mass-compiling GCC (stage1 only) regularly, catching build errors early.

2013-10-31
: # Andes NDS32 support
: A port for nds32, the 32-bit architecture of AndesCore families, has been contributed by Andes Technology Corporation.

2013-10-16
: [GCC 4.8.2](gcc-4.8/) released

2013-10-11
: OpenMP v4.0
: An implementation of the [OpenMP v4.0](http://www.openmp.org/mp-documents/OpenMP4.0.0.pdf) parallel programming interface for so far just C, C++ has been added. Code was contributed by Jakub Jelinek, Aldy Hernandez, Richard Henderson of Red Hat, Inc. and Tobias Burnus.

2013-10-03
: [C++11](projects/cxx0x.html) \<regex\> support
: Regular expression support in [libstdc++-v3](libstdc++/) is now available.

2013-10-01
: # Synopsys Designware ARC support
: A port for Synopsys Designware ARC has been contributed by Embecosm and Synopsys Inc.

2013-09-12
: # TI MSP430 support
: A port for the TI MSP430 has been contributed by Red Hat Inc.

2013-08-08
: # Twitter and Google+ accounts
: GCC and the GNU Toolchain Project now have accounts on [Twitter](https://twitter.com/gnutools) and [Google+](https://plus.google.com/108467477471815191158) to help developers stay informed of progress.

2013-07-15
: # IBM POWER8 support
: Support for the POWER8 processor has been contributed by IBM. This includes new VSX, HTM and atomic instructions, new intrinsics, and scheduling improvements. Little Endian support also has been enhanced, including control over vector element endianness.

2013-05-31
: [GCC 4.8.1](gcc-4.8/) released

2013-04-12
: [GCC 4.6.4](gcc-4.6/) released

2013-04-11
: [GCC 4.7.3](gcc-4.7/) released

2013-04-01
: [GCC 4.8.1](gcc-4.8/changes.html#cxx) will be [C++11](projects/cxx0x.html) feature-complete
: Support for C++11 ref-qualifiers was added to the GCC 4.8 branch, making G++ the first C++ compiler to implement all the major language features of the C++11 standard. This functionality will be available in GCC 4.8.1.

2013-03-22
: [GCC 4.8.0](gcc-4.8/) released

2013-01-23
: # GCC internals documentation
: The [GCC Resource Center](http://www.cse.iitb.ac.in/grc/) at IITB is providing documentation, tutorials and videos about GCC internals with support from the Government of India.

2012-10-24
: # ARM AArch64 support
: A port for AArch64, the 64-bit execution state in the ARMv8 architecture, has been contributed by ARM Ltd.

2012-10-10
: # IBM zEnterprise EC12 support
: Support for the latest release of the System z mainframe zEC12 has been added to the architecture back end. This work was contributed by Andreas Krebbel of IBM.

2012-09-20
: [GCC 4.7.2](gcc-4.7/) released

2012-08-14
: # GCC now uses C++ as its implementation language
: The [cxx-conversion](http://gcc.gnu.org/wiki/cxx-conversion) branch has been merged into trunk. This switches GCC's implementation language from C to [C++](codingconventions.html#Cxx_Conventions). Additionally, some data structures have been re-implemented in C++ (more details in the [merge announcement](http://gcc.gnu.org/ml/gcc-patches/2012-08/msg00711.html)). This work was contributed by Lawrence Crowl and Diego Novillo of Google.

2012-07-02
: [GCC 4.5.4](gcc-4.5/) released

2012-06-14
: [GCC 4.7.1](gcc-4.7/) released

2012-03-22
: [GCC 4.7.0](gcc-4.7/) released

2012-03-13
: [GCC 4.4.7](gcc-4.4/) released

2012-03-01
: [GCC 4.6.3](gcc-4.6/) released

2012-02-02
: # CR16 processor support
: A port for National Semiconductor's CR16 processor has been contributed by Sumanth Gundapaneni and Jayant Sonar of KPIT Cummins.

2012-02-14
: # TILE-Gx and TILEPro processor support
: Ports for the TILE-Gx and TILEPro families of processors have been contributed by Walter Lee from Tilera.

2011-11-06
: # Atomic memory model support
: C++11/C11 [memory model](http://gcc.gnu.org/wiki/Atomic/GCCMM) support has been added through a new set of built-in `__atomic` functions. Code was contributed by Andrew MacLeod, Richard Henderson, and Aldy Hernandez, all of Red Hat, Inc.

2011-11-18
: # GNU Tools Cauldron 2012
: IUUK (Computer Science Institute, Charles University), CE-ITI (Institute for Theoretical Computer Science) and Google are organizing a [workshop for GNU Tools developers](http://gcc.gnu.org/wiki/cauldron2012). The workshop will be held in July 2012 at Charles University, Prague.

2011-11-15
: # Transactional memory support
: An implementation of the ongoing [transactional memory](http://gcc.gnu.org/wiki/TransactionalMemory) standard has been added. Code was contributed by Richard Henderson, Aldy Hernandez, and Torvald Riegel, all of Red Hat, Inc. The project was partially funded by the [Velox](http://www.velox-project.eu/) project. This feature is experimental and is available for C and C++ on selected platforms.

2011-11-10
: # POWER7 on the GCC Compile Farm
: IBM has donated a 64 processor POWER7 machine (3.55 GHz, 64 GB RAM) to the [GCC Compile Farm project](http://gcc.gnu.org/wiki/CompileFarm). Hosting is donated by the OSU Open Source Lab.

2011-11-03
: # Epiphany processor support
: A port for Adapteva's Epiphany multicore processor has been contributed by Embecosm.

2011-10-26
: [GCC 4.6.2](gcc-4.6/) released

2011-08-02
: OpenMP v3.1
: An implementation of the [OpenMP v3.1](http://www.openmp.org/mp-documents/OpenMP3.1.pdf) parallel programming interface for C, C++ and Fortran has been added. Code was contributed by Jakub Jelinek of Red Hat, Inc. and Tobias Burnus.

2011-07-15
: # TI C6X processor support
: A port for the TI C6X family of processors has been contributed by CodeSourcery.

2011-06-27
: [GCC 4.3.6](gcc-4.3/) released

2011-06-27
: [GCC 4.6.1](gcc-4.6/) released

2011-04-28
: [GCC 4.5.3](gcc-4.5/) released

2011-04-16
: [GCC 4.4.6](gcc-4.4/) released

2011-04-04
: # GCC at Google Summer of Code
: GCC has been accepted to Google's Summer of Code 2011. We are currently accepting student applications.

2011-03-25
: [GCC 4.6.0](gcc-4.6/) released

2011-01-11
: # Objective-C enhanced significantly
: GCC 4.6 will support many [new Objective-C features](gcc-4.6/changes.html#objective-c), such as declared and synthesized properties, dot syntax, fast enumeration, optional protocol methods, method/protocol/class attributes, class extensions and a new GNU Objective-C runtime API. This was contributed by Nicola Pero and Iain Sandoe, with support from Mike Stump.

2010-12-16
: [GCC 4.5.2](gcc-4.5/) has been released.

2010-12-2
: [GCC 4.6](gcc-4.6/changes.html#go) will support the [Go programming language](http://golang.org/). The new frontend was contributed by Ian Lance Taylor at Google.

2010-11-16
: [GCC 4.6](gcc-4.6/changes.html#libquadmath) will include the `libquadmath` library, which provides quad-precision mathematical functions on targets supporting the `__float128` datatype. The library is used to provide the `REAL(16)` type in GNU Fortran on such targets. It has been contributed by François-Xavier Coudert.

2010-10-1
: [GCC 4.4.5](gcc-4.4/) has been released.

2010-09-28
: Our old Bugzilla instance has been upgraded to the latest release 3.6.2, bringing a better user experience and a new and powerful API for external tools. The upgrade has been done by Frédéric Buclin of the Bugzilla project at Mozilla.

2010-09-28
: Support has been added for the [Xilinx MicroBlaze softcore processor](gcc-4.6/changes.html#microblaze) target by Michael Eager, Eager Consulting.

2010-07-31
: [GCC 4.5.1](gcc-4.5/) has been released.

2010-05-22
: [GCC 4.3.5](gcc-4.3/) has been released.

2010-04-29
: [GCC 4.4.4](gcc-4.4/) has been released.

2010-04-14
: [GCC 4.5.0](gcc-4.5/) has been released.

2010-01-26
: Kaveh Ghazi has integrated GCC with the [MPC](http://www.multiprecision.org/) library. As of the upcoming GCC 4.5.0 release this library will be [required to build GCC](gcc-4.5/changes.html#mpccaveat). Using MPC allows more effective and accurate complex number [compile-time optimizations](gcc-4.5/changes.html#mpcopts).

2010-01-25
: An experimental [profile mode](http://gcc.gnu.org/onlinedocs/libstdc++/manual/profile_mode.html) has been added. This is an implementation of many C++ Standard library constructs with an additional analysis layer that gives performance improvement advice based on recognition of suboptimal usage patterns. Code was contributed by Silvius Rus, Lixia Liu, and Changhee Jung with the assistance of Benjamin Kosnik, Paolo Carlini, and Jonathan Wakely.

2010-01-21
: [GCC 4.4.3](gcc-4.4/) has been released.

2009-10-26
: Support has been added for the [Renesas RX processor (RX)](gcc-4.5/changes.html#rx) target by Red Hat, Inc.

2009-10-15
: [GCC 4.4.2](gcc-4.4/) has been released.

2009-10-3
: The [LTO](http://gcc.gnu.org/wiki/LinkTimeOptimization) branch has been merged into trunk. The next release of GCC will feature a new whole-program optimizer, able to perform interprocedural optimizations across different files, even if they are written in different languages.

2009-08-4
: [GCC 4.3.4](gcc-4.3/) has been released.

2009-07-22
: [GCC 4.4.1](gcc-4.4/) has been released.

2009-06-24
: Support has been added for the [Toshiba Media embedded Processor (MeP)](gcc-4.5/changes.html#mep) target by Red Hat, Inc.

2009-05-6
: GCC can now be extended using a generic [plugin framework](http://gcc.gnu.org/wiki/plugins-branch) on host platforms that support dynamically loadable objects.

2009-04-21
: [GCC 4.4.0](gcc-4.4/) has been released.

2009-01-27
: The GCC Steering Committee, along with the Free Software Foundation and the Software Freedom Law Center, is pleased to announce the release of a new [GCC Runtime Library Exception](http://www.gnu.org/licenses/gcc-exception.html).
: This license exception has been developed to allow various GCC libraries to upgrade to GPLv3. It will also enable the development of a plugin framework for GCC. ([Rationale document and FAQ](http://www.gnu.org/licenses/gcc-exception-faq.html))

2009-01-24
: [GCC 4.3.3](gcc-4.3/) has been released.

2008-09-4
: A [port for the picochip](gcc-4.4/changes.html#picochip) target has been contributed by Picochip Designs Limited.

2008-08-27
: [GCC 4.3.2](gcc-4.3/) has been released.

2008-06-6
: [GCC 4.3.1](gcc-4.3/) has been released.

2008-06-6
: An implementation of the [OpenMP v3.0](http://openmp.org/mp-documents/spec30.pdf) parallel programming interface for C, C++ and Fortran has been added. Code was contributed by Jakub Jelinek, Richard Henderson and Ulrich Drepper of Red Hat, Inc.

2008-05-22
: AMD Developer Central has donated two bi-quad core machines with the latest AMD Opteron 8354 "Barcelona B3" processors and 16GB of RAM to the [GCC Compile Farm project](http://gcc.gnu.org/wiki/CompileFarm) for use by free software developers. Hosting is donated by INRIA Saclay.

2008-05-19
: [GCC 4.2.4](gcc-4.2/) has been released.

2008-03-5
: [GCC 4.3.0](gcc-4.3/) has been released.

2008-02-1
: [GCC 4.2.3](gcc-4.2/) has been released.

2008-01-8
: Jakub Jelinek, Joseph Myers, and Richard Guenther [join the GCC release management team](http://gcc.gnu.org/ml/gcc/2008-01/msg00133.html), quadrupling its head count.

2008-01-2
: [Gfortran annual report for 2008](http://gcc.gnu.org/ml/gcc/2008-01/msg00009.html)

2007-10-7
: [GCC 4.2.2](gcc-4.2/) has been released.

2007-09-11
: An experimental [parallel mode](http://gcc.gnu.org/onlinedocs/libstdc++/manual/parallel_mode.html) has been added. This is a parallel implementation of many C++ Standard library algorithms, like `std::accumulate`, `std::for_each`, `std::transform`, or `std::sort`, to give but four examples. Code was contributed by Johannes Singler and Leonor Frias, with the support of the University of Karlsruhe. Assisting were Felix Putze, Marius Elvert, Felix Bondarenko, Robert Geisberger, Robin Dapp, and Benjamin Kosnik of Red Hat.

2007-07-18
: [GCC 4.2.1](gcc-4.2/) has been released.

2007-07-2
: C interoperability support (ISO Bind C) has been added to the Fortran compiler. The code was contributed by Christopher D. Rickett of Los Alamos National Lab.

2007-06-2
: Experimental support for the upcoming ISO C++0x standard been added. Enabled with `-std=gnu++0x` or `-std=c++0x`, this offers a first look at [upcoming C++0x features](gcc-4.3/cxx0x_status.html) and will be available in GCC 4.3. Code was contributed by Douglas Gregor of Indiana University, Russell Yanofsky, Benjamin Kosnik of Red Hat and Paolo Carlini of Novell, and reviewed by Jason Merrill of Red Hat and Mark Mitchell and Nathan Sidwell of CodeSourcery.

2007-05-13
: [GCC 4.2.0](gcc-4.2/) has been released.

2007-03-9
: All m68k targets now support ColdFire processors and offer the choice between ColdFire and non-ColdFire libraries at configure time. There have been [several other significant changes](gcc-4.3/changes.html#m68k) to the m68k and ColdFire support. This work was contributed by Nathan Sidwell of CodeSourcery and others.

2007-02-13
: [GCC 4.1.2](gcc-4.1/) has been released.

2007-01-25
: Interprocedural optimization passes have been reorganized to operate on SSA This enables more precise function analysis and optimization while inlining, significantly improving the performance of programs with high abstraction penalty. Code from [ipa-branch](gcc-4.3/changes.html#ipa) contributed by Jan Hubicka, SUSE labs and Razya Ladelsky, IBM Haifa, was reviewed by Diego Novillo, Richard Guenther, Roger Sayle and Ian Lance Taylor.

2007-01-8
: Andrew Haley and Tom Tromey of Red Hat merged the `gcj-eclipse` branch to svn trunk. GCC now uses the Eclipse compiler as a front end, enabling all 1.5 language features. This merge also brings in a new, generics-enabled version of Classpath, including [some new tools](gcc-4.3/changes.html#gcjtools). All this will appear in GCC 4.3.

2007-01-6
: Kaveh Ghazi has integrated the GCC middle-end with the [MPFR](http://www.mpfr.org/) library, allowing more effective [compile-time optimizations](gcc-4.3/changes.html#mpfropts). As a result, this library and the [GMP](http://gmplib.org/) library are now [required to build GCC](gcc-4.3/changes.html#mpfrcaveats).

2007-01-5
: [Memory SSA](http://gcc.gnu.org/wiki/mem-ssa), a new representation for memory expressions in SSA form has been contributed by Diego Novillo of Red Hat. This new mechanism improves [compile-times and memory utilization](http://gcc.gnu.org/ml/gcc-patches/2006-12/msg00436.html) by the compiler.

2007-01-3
: Trevor Smigiel and Andrew Pinski of Sony Computer Entertainment Inc. have contributed the Synergistic Processor Unit (SPU) port for the Cell Broadband Engine Architecture (BEA).

2007-01-1
: 2006 has been a very productive year for the new Fortran frontend, with [lots of improvements and fixes](http://gcc.gnu.org/ml/gcc/2007-01/msg00059.html).

2006-09-5
: A forward propagation pass on RTL was contributed by Paolo Bonzini of University of Lugano, and Steven Bosscher while working for Novell.

2006-05-24
: [GCC 4.1.1](gcc-4.1/) has been released.

2006-03-10
: [GCC 4.0.3](gcc-4.0/) has been released.

2006-03-9
: Richard Henderson, Jakub Jelinek and Diego Novillo of Red Hat Inc, and Dmitry Kurochkin have contributed an implementation of the [OpenMP](http://openmp.org/wp/) v2.5 parallel programming interface for C, C++ and Fortran.

2006-03-6
: [GCC 3.4.6](gcc-3.4/) has been released.

2006-02-28
: [GCC 4.1.0](gcc-4.1/) has been released.

2005-11-30
: [GCC 3.4.5](gcc-3.4/) has been released.

2005-10-26
: GCC has moved from CVS to [SVN](svn.html) for revision control.

2005-09-28
: [GCC 4.0.2](gcc-4.0/) has been released.

2005-08-22
: Red Hat Inc has contributed a port for the MorphoSys family.

2005-07-20
: Red Hat Inc has contributed a port for the Renesas R8C/M16C/M32C families.

2005-07-17
: GCC 4.1 stage 2 has been closed. The following projects were contributed during stage 1 and stage 2: New C Parser, LibAda GNATTools Branch, Code Sinking, Improved phi-opt, Structure Aliasing, Autovectorization Enhancements, Hot and Cold Partitioning, SMS Improvements, Integrated Immediate Uses, Tree Optimizer Cleanups, Variable-argument Optimization, Redesigned VEC API, IPA Infrastructure, Altivec Rewrite, Warning Message Control, New SSA Operand Cache Implementation, Safe Builtins, Reimplementation of IBM Pro Police Stack Detector, New DECL hierarchy. More information about these projects can be found at [GCC 4.1 projects](http://gcc.gnu.org/wiki/GCC_4.1_Projects).

2005-07-7
: [GCC 4.0.1](gcc-4.0/) has been released.

2005-05-18
: [GCC 3.4.4](gcc-3.4/) has been released.

2005-05-03
: [GCC 3.3.6](gcc-3.3/) has been released.

2005-04-20
: [GCC 4.0.0](gcc-4.0/) has been released.

2005-04-12
: Diego Novillo of Red Hat has contributed a Value Range Propagation pass.

2005-04-5
: Analog Devices has contributed a port for the Blackfin processor. See the [Blackfin projects](http://blackfin.uclinux.org/gf/) page for more information and ports of binutils and gdb.

2005-02-06
: gcc.gnu.org suffered hardware failure and had to be restored from backups. We do not believe any data was lost in the CVS repository. We did lose any pending messages in the mail queue as that does not get backed up. At this time, everything should be functional except for htdig. The mailing list archives on the web site are also out of date and will be updated soon. New mail will update the archives correctly, however. If you find any other problems, please email <overseers@gcc.gnu.org>

2005-01-27
: GCC now has a [Wiki](http://gcc.gnu.org/wiki/).

2004-11-4
: [GCC 3.4.3](gcc-3.4/) has been released.

2004-09-30
: [GCC 3.3.5](gcc-3.3/) has been released.

2004-09-9
: The next major version of GCC following the current 3.4 release series will be called GCC 4.0.

2004-09-6
: [GCC 3.4.2](gcc-3.4/) has been released.

2004-07-1
: [GCC 3.4.1](gcc-3.4/) has been released.

2004-05-13
: The [tree-ssa branch](http://gcc.gnu.org/projects/tree-ssa/) has been [merged into mainline](http://gcc.gnu.org/ml/gcc/2004-05/msg00679.html).

2004-04-20
: [GCC 3.4.0](gcc-3.4/) has been released.

2004-02-25
: The [tree-ssa branch](http://gcc.gnu.org/projects/tree-ssa/) has been frozen to be incorporated into GCC 4.0.0. Tree SSA incorporates two new high-level intermediate languages (GENERIC and GIMPLE), an optimization framework for GIMPLE based on the Static Single Assignment (SSA) representation, several SSA-based optimizers and various other improvements to the internal structure of the compiler that allow new optimization opportunities that were difficult to implement before.

2004-02-24
: [GCC 3.3.3](gcc-3.3/) has been released.

2004-02-6
: Josef Zlomek of SUSE Labs and Daniel Berlin of IBM Research have contributed Variable Tracking. It generates more accurate debug info about locations of variables and allows debugging code compiled with `-fomit-frame-pointer`.

2003-10-18
: Bernardo Innocenti of Develer S.r.l. has contributed the [m68k-uclinux target](http://www.uclinux.org/pub/uClinux/uclinux-elf-tools/gcc-3/) and improved support for ColdFire cores, based on former work by Paul Dale (SnapGear, Inc.) and Peter Barada (Motorola, Inc.).

2003-10-17
: [GCC 3.3.2](gcc-3.3/) has been released.

2003-08-27
: Nicolas Pitre has contributed his hand-coded floating-point support code for ARM. It is both significantly smaller and faster than the existing C-based implementation. The arm-elf configuration uses the new code now, and other ports will follow.

2003-08-8
: [GCC 3.3.1](gcc-3.3/) has been released.

2003-06-26
: Ben Elliston of Wasabi Systems, Inc. has converted the existing ARM processor pipeline description to the new [DFA pipeline description model](http://gcc.gnu.org/onlinedocs/gccint/Processor-pipeline-description.html). It will be part of the GCC 3.4.0 release.

2003-05-27
: Proceedings and photographs of participants are available for the First Annual [GCC Developers' Summit](http://www.gccsummit.org/), which took place May 25-27, 2003.

2003-05-14
: [GCC 3.3](gcc-3.3/) has been released.

2003-04-25
: [GCC 3.2.3](gcc-3.2/) has been released.

2003-02-05
: [GCC 3.2.2](gcc-3.2/) has been released.

2003-01-29
: Andrew Haley of Red Hat completed the work begun by Bo Thorsen of SuSE to port [GCJ](java/) to the AMD x86-64 architecture. This is the first implementation of the Java programming language to be made available on that platform. It will be part of the GCC 3.3 release.

2003-01-28
: The ongoing effort to remove warnings from the GCC code base itself, spear-headed by Kaveh Ghazi, has paid off: For our development versions and snapshots, we now enable `-Werror` during a full bootstrap.

2003-01-22
: The GCC Steering Committee has named Gabriel Dos Reis as release manager for the upcoming GCC 3.2.2 release, allowing Mark Mitchell to focus his efforts on the GCC 3.3 and 3.4 release series. 3.2.2 is intended to be a bug fix release only.

2003-01-10
: Geoffrey Keating of Apple Computer, Inc., with support from Red Hat, Inc., has contributed a [precompiled header](http://gcc.gnu.org/onlinedocs/gcc/Precompiled-Headers.html#Precompiled%20Headers) implementation that can dramatically speed up compilation of some projects.

2002-12-27
: Mark Mitchell of CodeSourcery has contributed a [new, hand-crafted recursive-descent C++ parser](http://gcc.gnu.org/ml/gcc/2000-10/msg00573.html) sponsored by the Los Alamos National Laboratory. The new parser is more standard conforming and fixes many bugs (about 100 in our bug database alone) from the old YACC-derived parser.

2002-12-4
: Nathan Sidwell of CodeSourcery has contributed an [implementation of non-trivial covariant returns for non-varadic virtual functions](http://gcc.gnu.org/ml/gcc-patches/2002-11/msg01858.html).

2002-11-21
: [GCC 3.2.1](gcc-3.2/) has been released. We plan to shortly create the GCC 3.3 release branch (but want to fix a couple of high-priority regressions first).

2002-08-14
: [GCC 3.2](gcc-3.2/) has been released.

2002-07-26
: [GCC 3.1.1](gcc-3.1/) has been released.

2002-07-19
: Michael Matz of SuSE, Daniel Berlin, and Denis Chertykov have contributed a new register allocator. IBM and Rice University have allowed use of their register allocator software patents for graph coloring and register coalescing.

2002-05-28
: Support for all the systems [obsoleted in GCC 3.1](gcc-3.1/changes.html#obsolete_systems) has been removed from the development sources. (These targets can still be restored if a maintainer appears.)

2002-05-15
: [GCC 3.1](gcc-3.1/) has been released.

2002-05-5
: Aldy Hernandez, of Red Hat, Inc, has contributed extensions to the PowerPC port supporting the AltiVec programming model (SIMD). The support, though presently useful, is experimental and is expected to stabilize for 3.2. The support is written to conform to Motorola's AltiVec specs.

2002-05-2
: HP and CodeSourcery announced that HP will sponsor Mark Mitchell's work as GCC Release Manager through April 2003.

2002-04-30
: Vladimir Makarov, of Red Hat, Inc, has contributed a new scheme for describing processor pipelines, commonly referred to as the [DFA scheduler.](news/dfa.html)

2002-04-15
: The Chill front end (that already was omitted from GCC 3.0) has been removed from the GCC source tree.

2002-02-25
: We have branched for GCC 3.1 ([release criteria](gcc-3.1/criteria.html), [changes](gcc-3.1/changes.html)) and are concentrating on bug fixes. The 3.1 release is [planned for late April](develop.html#future).

2002-02-21
: GCC 3.0.4 has been released.

2002-02-9
: Alexandre Oliva, of Red Hat, Inc., has contributed a port to the SuperH SH5 64-bit RISC microprocessor architecture, extending the existing SH port.

2002-01-24
: Tensilica has contributed a port to the configurable and extensible Xtensa microprocessor architecture.

2002-01-14
: Richard Stallman has changed the licensing of the Classpath AWT implementation to match the licensing of the rest of Classpath. This means that the only remaining barrier to AWT for libgcj is manpower. Work has already begun to merge the Classpath and libgcj AWT implementations.

2002-01-8
: SuSE Labs developers Jan Hubicka, Bo Thorsen and Andreas Jaeger have contributed a port to the AMD x86-64 architecture.

2001-12-20
: GCC 3.0.3 has been released.

2001-11-3
: Hans-Peter Nilsson has contributed a port to [MMIX](http://www-cs-faculty.stanford.edu/~knuth/mmix.html), the CPU architecture used in new editions of Donald E. Knuth's The Art of Computer Programming.

2001-10-25
: GCC 3.0.2 has been released.

2001-10-11
: Axis Communications has contributed its port to the CRIS CPU architecture, used in the ETRAX system-on-a-chip series. See [developer.axis.com](http://developer.axis.com/) for technical information.

2001-10-5
: Alexandre Oliva of Red Hat has generalized the tree inlining infrastructure, formerly in the C++ front end, so that it is now used in the C front end too.

2001-10-2
: Ada Core Technologies, Inc, has contributed its GNAT Ada 95 front end and associated tools. The GNAT compiler fully implements the Ada language as defined by the ISO/IEC 8652 standard.

2001-09-11
: Roman Lechtchinsky, Technische Universität Berlin, has donated support for the Cray T3E platform.

2001-08-29
: Jan Hubicka, SuSE Labs, together with Richard Henderson, Red Hat, and Andreas Jaeger, SuSE Labs, has contributed [infrastructure for profile driven optimizations](news/profiledriven.html).

2001-08-25
: Geoffrey Keating of Red Hat has donated support for Sanyo's Stormy16 CPU core.

2001-08-20
: GCC 3.0.1 has been released.

2001-08-16
: The gcc.gnu.org machine will be moving to a new physical location with significantly improved bandwidth and backup on Saturday, August 18th. The move is expected to take less than two hours; DNS will be adjusted accordingly, the new IP address will be 209.249.29.67.

2001-07-17
: The [Steering Committee](steering.html) adopted a [new development plan](develop.html) which we will start using for GCC 3.1, scheduled for April 15, 2002.

2001-07-9
: Daniel Berlin and Jeff Law have contributed a [Sparse Conditional Constant Propagation](news/ssa-ccp.html) optimization pass.

2001-06-18
: [GCC 3.0](gcc-3.0/) has been released.

2001-03-16
: [GCC 2.95.3](gcc-2.95/index.html) has been released.

2001-02-12
: Our CVS tree has branched for the GCC 3.0 release process and Mark Mitchell, our release manager, has provided some [guidelines for the GCC 3.0 branch](http://gcc.gnu.org/ml/gcc/2001-02/msg00403.html).

2001-02-12
: Hans-Peter Nilsson, our search-engine volunteer, tweaked the search-engine to include all mailing lists (including libstdc++ and GCJ).

2001-01-28
: Tom Tromey has moved the Java mailing lists and web pages to gcc.gnu.org. Now the GCJ project is fully integrated into GCC.

2001-01-21
: Neil Booth has contributed [improvements](news/dependencies.html) to the dependency generation machinery of the C preprocessor, adding some new functionality and correcting some undesirable behaviour of the old implementation.

2001-01-15
: The GCC development tree is in a slush state, with the goal of stabilization for branching for GCC 3.0.

2000-12-19
: The runtime library for the [Java front end](java/), `libgcj`, has been moved into the GCC tree. This means that a separate download will no longer be required for Java support.

2000-12-4
: Nick Clifton of Red Hat has donated support for the Intel XScale architecture.

2000-11-26
: The C, C++ and Objective C front ends now use the integrated preprocessor exclusively; their independent ability to tokenize an input stream has been removed.

2000-11-18
: G++ is now using a new C++ ABI that represents classes more compactly, uses shorter mangled names, and is optimized for higher run-time performance. The implementation of the new ABI was contributed by Mark Mitchell, Nathan Sidwell, and Alexander Samuel of CodeSourcery, LLC.

2000-11-18
: GCC now supports ISO C99 declarations in `for` loops (`for (int i = 0; i < 10; i++) /* ... */`). These are only supported in C99 mode (command-line options `-std=gnu99` or `-std=c99`), which will be the default in some future release, but not in GCC 3.0.

2000-11-14
: Michael Matz has donated an implementation of the Lengauer and Tarjan algorithm for computing dominators in the CFG. This algorithm can be significantly faster and more space efficient than our older algorithm. For one particularly nasty CFG from complex C++ code (more than 77000 basic blocks) compile time dropped from more than 40 minutes to around 25 minutes. Memory consumption was also dramatically decreased.

2000-11-13
: We have now switched the C++ front end to use [libstdc++-v3](libstdc++/), a new implementation of the ISO Standard C++ Library which brings significant changes and improvements over our "old" library. There still be may some rough edges, but we are addressing problems as soon as we learn about them -- please help testing and improving "your" ports!

2000-11-13
: # GCC now supports two more ISO C99 features
: -   The builtin boolean `_Bool` type and the `<stdbool.h>` header. (GCC 2.95 had a non-conforming `<stdbool.h>` header; code that used that header will not be binary compatible with code using the new conforming version.)
: -   Mixed declarations and code in compound statements.

2000-11-2
: The C, C++ and Objective C front ends to GCC now use an integrated preprocessor by default. If all goes well, this will also be the default mode for GCC 3.0.

2000-11-1
: Support for C99's `_Pragma` operator has been added to the preprocessor. This feature effectively makes it possible to have `#pragma` directives be part of macro expansions, and to have their arguments expanded too if necessary.

2000-10-6
: We would like to point out that GCC 2.96 is not a formal GCC release nor will there ever be such a release. Rather, GCC 2.96 has been the code- name for our development branch that will eventually become GCC 3.0. [More...](gcc-2.96.html)

2000-Sep-11
: Zack Weinberg of Cygnus, a Red Hat company, has contributed modifications to the C, C++, and Objective C compilers which permit them to use the C preprocessor library (cpplib) directly instead of via a separate executable.

: This is not yet the default mode, but we hope it will be the default in GCC 3.0. When it is used the compiler will be faster because it will not have to do lexical analysis twice, nor save the preprocessed output to a temporary file. In the future, this will permit better error messages, and more detailed debugging information particularly when complex macros are used.

2000-Sep-11
: Neil Booth has contributed a new lexer and macro-expander for the C preprocessor. The lexer makes a single pass over the source files, whereas previously it made two. The macro expander operates on lexical tokens instead of text strings.

: ISO C, C++, and Objective C use the new preprocessor. Traditional (K+R) C, Fortran, and Chill use an older implementation (taken from GCC 1) which obeys the rules for pre-standard C preprocessing. Either version may be used to preprocess assembly language.

2000-05-2
: Stan Cox and Jason Eckhardt of Cygnus, a Red Hat company, have contributed a [basic block reordering pass](news/reorder.html). The optimization can reposition basic blocks from across the entire function in an attempt to reduce branch penalties and enhance instruction cache efficiency.

: Our thanks go to Michael Hayes, Jan Hubicka, and Graham Stott who noticed or fixed defects or made other useful suggestions.

2000-05-1
: Richard Earnshaw of ARM Ltd, and Nick Clifton of Cygnus, a Red Hat company, have contributed a new back end for the Arm and Thumb processors.

: The new back end combines code generation for the Arm, the Thumb and the StrongArm into one compiler, with the target processor and instruction sets being selectable via command line switches.

2000-04-30
: Michael Meissner and Nick Clifton of Cygnus, a Red Hat company, have contributed a port for the Mitsubishi D30V processor.

: Michael Meissner and Richard Henderson of Cygnus, a Red Hat company, have contributed a new if-conversion pass. The code runs faster and identifies more optimization opportunities than the old code. In addition, it also has support for conditional (predicated) execution, such as is found in the Intel IA-64 architecture, the ARM processors, and numerous embedded LIW and DSP parts.

2000-03-22
: The Steering Committee has appointed Mark Mitchell, of CodeSourcery, LLC, to manage the GCC 3.0 release and as a new Steering Committee member. CodeSourcery will be providing time from Mark, Alex Samuel, and other personnel, to manage the release. Thanks!

: The Steering Committee and the GCC community owe Jeff Law an immense debt for his work as release manager for the EGCS 1.0.x, 1.1.x, and GCC 2.95.x series of releases. He has done an outstanding job.

2000-03-18
: Andy Vaught has started work on GNU Fortran 95, the Fortran front end destined to implement the latest standard. See [this page](http://g95.sourceforge.net/) for its current status.

2000-03-17
: Jim Wilson and Richard Henderson of Cygnus, a Red Hat company, and David Mosberger of HP labs have contributed a port for the Intel Itanium (aka IA-64) processor.
: Jeff Law and Richard Henderson of Cygnus, a Red Hat company, have contributed RTL based tail call elimination optimizations. Support currently exists for the Alpha, HPPA, ia32 and MIPS processors. Long term the RTL based tail call optimizations will be replaced with a tree based tail call optimizer.

2000-03-14
: CodeSourcery, LLC is now providing nightly snapshots of GCC, distributed as RPMs for GNU/Linux on Intel platforms, plus build logs and testsuite results. In order to allow users to more easily confirm whether the current snapshot of GCC fixes a particular bug, an online compilation web form is provided.

2000-03-13
: Denis Chertykov contributed an AVR port. AVR is a family of micro controllers made by Atmel with embedded FLASH program memory and embedded RAM. It is the first GCC port to an 8-bit microprocessor with a 16-bit address bus.

2000-03-9
: CodeSourcery, LLC and Cygnus, a Red Hat company, have contributed an implementation of [static single assignment](news/ssa.html) (SSA) representation. SSA will facilitate the implementation of powerful code optimizations in GCC.

2000-03-2
: Jason Molenda, who had a major role in setting up and managing the gcc.gnu.org (originally egcs.cygnus.com) machine and site, is leaving Cygnus. We would like to thank him for his efforts and support behind the scenes and wish Jason all the best in his new job.

2000-02-23
: Cygnus, a Red Hat company, contributed an M\*Core port.

2000-01-4
: Steve Chamberlain has contributed a picoJava port.

1999-12-10
: CodeSourcery, LLC has contributed a new [inliner for C++](news/inlining.html). As a result, the compiler may use dramatically less time and memory to compile programs that make heavy use of templates.

1999-12-1
: Cygnus has donated support for the Matsushita AM33 processor (a member of the MN10300 processor family). The MN103 family is targeted towards embedded consumer products such as DVD players, HDTV, etc.

1999-10-27
: [GCC 2.95.2 is released](gcc-2.95/index.html).

1999-10-16
: Craig Burley, our lead Fortran developer and the original author of g77, announced that he will stop working on g77 beyond the 2.95 series. On behalf of the entire GCC team, the steering committee would like to thank Craig for his work.

: Craig has written a detailed analysis of the [current state](http://world.std.com/~burley/g77-why.html) and [possible future](http://world.std.com/~burley/g77-next.html) of g77, available at his [g77 web site](http://world.std.com/~burley/g77.html).

: If you are interested in helping with g77, please [contact us](mailto:gcc@gcc.gnu.org?subject=g77%20Help%20Wanted?)!

1999-10-12
: We are pleased to announce that Richard Earnshaw and Jason Merrill have been given global write permissions throughout the GCC sources.
: Cygnus has installed [various upgrades](news/server.html) to improve services for GCC and other open source projects hosted by Cygnus.

1999-10-11
: The gcc [steering committee](steering.html) welcomes a new member: Gerald Pfeifer. His insights into political issues and his web improvement work were and will be of great use.

1999-09-21
: Nick Clifton of Cygnus Solutions has donated support for the Fujitsu FR30 processor. The FR30 is a low-cost 32bit cpu intended for larger embedded applications. It has a simple load/store architecture, 16 general registers and a variable length instruction set.

1999-09-20
: Cygnus Solutions has donated two new global optimizers to GCC. [Global Null Pointer Test Elimination](news/null.html) and [Global Code Hoisting/Unification](news/unify.html).

1999-09-3
: Long time GCC contributors Mark Mitchell and Richard Kenner have been given global write permissions. They are authorized to install and approve patches to any part of the compiler. Richard Kenner will initially be working on merging in the remaining changes from the old GCC 2 sources.

1999-09-2
: Richard Henderson has finished merging the ia32 backend rewrite into the mainline GCC sources. The rewrite is designed to improve optimization opportunities for the Pentium II target, but also provides a cleaner way to optimize for the Pentium III, AMD-K7 and other high end ia32 targets as they appear.

1999-08-31
: Cygnus Solutions has released libgcj version 2.95.1 Java runtime libraries for use with GCC 2.95.1.

1999-08-19
: [GCC 2.95.1 is released](gcc-2.95/index.html).

1999-08-4
: A new snapshot of the new Standard C++ Library V3 has been released. You can find more information from the [libstdc++ project's home page](libstdc++/).

: Cygnus Solutions has released libgcj version 2.95 Java runtime libraries for use with GCC 2.95.

1999-08-2
: Mumit Khan has pre-built gcc-2.95 binary packages for Windows platforms.

1999-07-31
: [GCC 2.95 is released](gcc-2.95/index.html).

1999-07-11
: Cygnus Solutions has donated support for a generic i386-elf target. (Note that this will not be included in gcc 2.95.)

1999-06-29
: Cygnus Solutions has donated hpux11 support. (Note that this will not be included in gcc 2.95.)

1999-06-15
: Cygnus Solutions has donated a major rewrite of the Intel IA-32 back end, focusing on better optimization for the Pentium II. (Note that this will not be included in gcc 2.95.)

1999-05-27
: Toon Moene has emailed (and posted) his notes on the GNU Fortran (`g77`) Birds-of-a-Feather (BOF) session at LinuxExpo to the appropriate lists, and Craig Burley has made Toon's notes available (in edited form) at <http://world.std.com/~burley/bof.html>.
: Probably the most important decision reached at the meeting is that Craig Burley will undertake the long-awaited “0.6 rewrite” of the `g77` front end as his top priority for the `gcc` 3.0 release, rather than focusing on implementing some of the “most wanted” features that didn't require the rewrite, such as Cray pointers.
: The BOF provided us with some additional information to guide future development of GNU Fortran. Thanks to all who attended, whether in person or in spirit!

1999-05-18
: The sixth snapshot of the ongoing re-written C++ Standard Library has been released. It includes SGI STL 3.2, an automatically generated `<limits>`, a partially re-written valarray, a working stringbuf and stringstream (for basic types). For more information, please check [libstdc++ home page](libstdc++/).

1999-04-23
: `g77` now supports optional run-time checking of array subscript expressions via the `-fbounds-check` compiler option. (The same option applies to whatever bounds-checking applies for other languages, such as Java. The `-ffortran-bounds-check` option specifies bounds-checking for Fortran code.)

1999-04-20
: Yes, it is not a hoax: The egcs steering committee is appointed official GNU maintainer for GCC; the egcs team will be responsible for rolling out future GCC releases.
: This will require some changes in policy and procedures for the project. We will provide more information on those changes as they are available.
: [www.gnu.org](http://www.gnu.org) has the FSF announcement under the "GNU flashes" heading.

1999-04-15
: Mark Mitchell is now a co-maintainer of the C++ front end along with Jason Merrill.

1999-04-13
: We have set up a new [mailing list](lists.html) [gcc-cvs-wwwdocs](http://gcc.gnu.org/ml/gcc-cvs-wwwdocs/) that tracks checkins to the egcs webpages CVS repository.

1999-04-7
: Cygnus announces the first public release of libgcj, the runtime component of the GNU compiler for Java.
: [Read the release announcement](news/javaannounce.html).
: [Goto the libgcj homepage](java/).

1999-04-6
: A new snapshot of the C++ standard library re-write has been released. This release includes SGI STL 3.12, a working valarray, and several (but not all) parts of templatized iostreams--for more information see: [libstdc++ home page](libstdc++/).

1999-03-23
: Through the efforts of John Wehle and Bernd Schmidt, GCC will now attempt to keep the stack 64bit aligned on the x86 and allocate doubles on 64bit boundaries. This can significantly improve floating point performance on the x86. Work will continue on aligning the stack and floating point values in the stack.

1999-03-15
: [egcs-1.1.2 is released](egcs-1.1/index.html).

1999-03-10
: Cygnus donates [improved global constant propagation](news/cprop.html) and [lazy code motion optimizer framework](news/lcm.html).

1999-03-7
: The egcs project now has [additional online documentation](onlinedocs/).

1999-02-26
: Richard Henderson of Cygnus Solutions has donated a major rewrite of the [control flow analysis pass](news/cfg.html) of the compiler.

1999-02-25
: [Marc Espie](mailto:espie@openbsd.org) has donated support for OpenBSD on the Alpha, SPARC, x86, and m68k platforms. Additional targets are expected in the future.

1999-01-21
: Cygnus donates support for the PowerPC 750 processor. The PPC750 is a 32bit superscalar implementation of the PowerPC family manufactured by both Motorola and IBM. The PPC750 is targeted at high end Macs as well as high end embedded applications.

1999-01-18
: Christian Bruel and Jeff Law donate improved [local dead store elimination.](news/dse.html)

1999-01-14
: Cygnus donates support for Hypersparc (SS20) and Sparclite86x (embedded) processors.

1998-12-7
: Cygnus donates support for demangling of HP aCC symbols.

1998-12-4
: [egcs-1.1.1 is released](egcs-1.1/index.html).

1998-11-26
: A database with test results is now available online, thanks to Marc Lehmann.

1998-11-23
: `egcs` now can dump flow graph information usable for [graphical representation](news/egcs-vcg.html). Contributed by Ulrich Drepper.

1998-11-21
: Cygnus donates support for the SH4 processor.

1998-11-10
: An official steering committee has been formed. Here is the [original announcement](steering.html).

1998-11-5
: The third snapshot of the rewritten `libstdc++` is available. You can read some more on [libstdc++/](libstdc++/).

1998-10-27
: Bernd Schmidt donates [localized spilling support](news/spill.html).

1998-09-22
: IBM Corporation delivers an update to the IBM Haifa [instruction scheduler](news/scheduler.ps) and new [software pipelining and branch optimization](news/pipelining.ps) support.

1998-09-18
: Michael Hayes donates [c4x port.](http://www.elec.canterbury.ac.nz/c4x/)

1998-09-6
: Cygnus donates [Java front end](java/gcj-announce.txt).

1998-09-3
: [egcs-1.1 is released](egcs-1.1/index.html).

1998-08-29
: Cygnus donates [Chill front end and runtime](news/chill.html).

1998-08-25
: David Miller donates rewritten [SPARC back end](news/sparc.html).

1998-08-19
: Mark Mitchell donates [load hoisting and store sinking](news/hoist.html) support.

1998-07-15
: The first snapshot of the rewritten `libstdc++` is available. You can read some more [here](libstdc++/).

1998-06-29
: Mark Mitchell donates [alias analysis framework](news/alias.html).

1998-05-26
: We have added two new mailing lists for the egcs project. **gcc-cvs** and **egcs-patches**.

: When a patch is checked into the CVS repository, a check-in notification message is automatically sent to the **gcc-cvs** mailing list. This will allow developers to monitor changes as they are made.

: Patch submissions should be sent to **egcs-patches** instead of the main egcs list. This is primarily to help ensure that patch submissions do not get lost in the large volume of the main mailing list.

1998-05-18
: Cygnus donates [gcse optimization pass](news/gcse.html).

1998-05-15
: [egcs-1.0.3 released](egcs-1.0/)!

1998-03-18
: [egcs-1.0.2 released](egcs-1.0/)!

1998-02-26
: The egcs web pages are now supported by egcs project hardware and are searchable with webglimpse. The CVS sources are browsable with the free [cvsweb](http://people.freebsd.org/~fenner/cvsweb/) package.

1998-02-7
: Stanford has volunteered to host a high speed mirror for egcs. This should significantly improve download speeds for releases and snapshots. Thanks Stanford and Tobin Brockett for the use of their network, disks and computing facilities!

1998-01-12
: **Remote access to CVS sources is available!**

1998-01-6
: [egcs-1.0.1 released](egcs-1.0/)!

1997-12-3
: [egcs-1.0 released](egcs-1.0/)!

1997-08-15
: The egcs project is [announced publicly](news/announcement.html) and the first snapshot is put on-line.
