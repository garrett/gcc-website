# GCC: Anonymous read-only rsync access

We are offering our SVN repository and various other data like our FTP site through anonymous [rsync](http://rsync.samba.org).

That way you can make local copies of the GCC SVN repository to ease the burden on the GCC main site, and browse the source locally.

## Getting a local copy of GCC repository using rsync

The GCC repository is available at `rsync://gcc.gnu.org/gcc-svn`. As of January 2011 it consumes about 23GB of disk space which takes a substantial time to transfer. Subsequent synchronizations will be much faster, as rsync uses a smart algorithm to only transfer differences.

Here is how you get a copy of the repository:

>     rsync --archive --delete --compress rsync://gcc.gnu.org/gcc-svn /usr/local/gcc-local-repo

The same command can be run periodically to synchronize your copy of the repository.

Other rsync options that you might want to use include `--verbose` and `--progress` to provide feedback, including during the initial phase that is otherwise silent.

To get a list of available rsync targets, run:

>     rsync rsync://gcc.gnu.org/

## Using the local repository {#svn}

Refer to [SVN instructions](svn.html) to check out your local copy of the repository. Note that the rsync command above will mirror the repository at its root directory, so the URL you will need to use to check out from your local repository will look something like `file:///usr/local/gcc-local-repo/` instead of `svn://gcc.gnu.org/svn/gcc/` (i.e., without the trailing `/svn/gcc` directory names that would be included for gcc.gnu.org access).
