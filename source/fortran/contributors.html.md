# Contributors to GNU Fortran

Andy Vaught started writing the parser for g95 in the summer of 1999. Early 2000 he moved the project to SourceForge. In the summer of 2002 Paul Brook and Steven Bosscher contributed the "translator code" that coupled the parser trees to the (then brand new) tree-ssa middle end of GCC. As of July 25th, 2003, g95 is part of GCC (as gfortran).

## Major code contributors

-   Andy Vaught
-   Katherine Holcomb
-   Paul Brook
-   Steven Bosscher
-   Arnaud Desitter
-   Canqun Yang
-   Xiaoqiang Zhang
-   Feng Wang

## Cleanups and Lord High Fixer

-   Richard T. Henderson

## Small patches (no copyright assignment)

-   Niels Kristian Bech Jensen
-   Scott Robert Ladd
-   Steven G. Johnson
-   Tobias Schlueter

## Small fixes

-   Toon Moene
-   Bud Davis

## Helpful comments and bug reports

-   Erik Schnetter
-   Steven G. Kargl
-   W. Clodius
-   Claus Fischer

