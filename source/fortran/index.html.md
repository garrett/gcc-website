# Welcome to the home of GNU Fortran

The purpose of the GNU Fortran (GFortran) project is to develop the Fortran compiler front end and run-time libraries for GCC, the GNU Compiler Collection. GFortran development is part of the [GNU Project](http://www.gnu.org/). We seek to bring free number crunching to a broad spectrum of platforms and users.

In particular, the project wishes to reach users of the Fortran language, be it in the scientific community, education, or commercial environments. The GFortran compiler is fully compliant with the Fortran 95 Standard and includes legacy F77 support. In addition, a significant number of Fortran 2003 and Fortran 2008 features are implemented. Please give it a try. If you encounter problems, contact us at the mailing list or file a problem report in bugzilla.

GFortran development follows the open development process. We do this to attract a diverse team of developers and to ensure that GFortran works on multiple architectures and diverse environments. We always need more help. If you are interested in participating, please contact us at <fortran@gcc.gnu.org>. (Also check out our [mailing lists page)](../lists.html)

## The Wiki and Getting the Compiler

For additional info on GFortran developments, you may find the [GFortran wiki](http://gcc.gnu.org/wiki/GFortran) useful. Anyone may contribute information to the wiki. (Neither copyright paperwork nor a patch review process is required.)

The GNU Project is about providing source code for its programs. For convenience, a number of people regularly build binaries for different platforms. Links to these can be found at the [wiki](http://gcc.gnu.org/wiki/GFortran). Most of the binary executables are the latest development snapshots of GFortran and are provided to encourage testing. We also want new users, from students to masters of the art of Fortran, to try GFortran. It really is a great compiler!

## Project Objectives

We strive to provide a high quality Fortran compiler that works well on a variety of native targets. This means:

-   Conformance to Fortran standards, primarily Fortran 95, 2003, and 2008

-   Performance of executables and computational accuracy

-   Reasonable compile speed and cross compilation capability

-   Good diagnostics and debugging features

-   Legacy code support where practical.

### Extensions in GNU Fortran

The initial goal of the GNU Fortran Project was construction of a Fortran 95 compiler that complies with the ISO Fortran 95 Programming Language standard [ISO/IEC 1539-1:1997(E)]. We are now well into F2003 and F2008 features. The [GFortran wiki](http://gcc.gnu.org/wiki/GFortran) and [Bugzilla](http://gcc.gnu.org/bugzilla/) list features under development or yet to be implemented. Compiler capability is quite extensive and includes nearly all g77 features. We highly encourage users to move from g77, which is no longer maintained, and start taking advantage of GFortran's modern features. Legacy g77 code will compile fine in almost all cases.

## Status of Compiler and Run-time Library

We regularly update the [status](http://gcc.gnu.org/wiki/GFortran#news) of the front end and run-time library development. For a TODO list, check [Bugzilla](http://gcc.gnu.org/bugzilla/) and the [GFortran wiki](http://gcc.gnu.org/wiki/GFortran).

## Contributing

We encourage everyone to [contribute](../contribute.html) changes and help test GNU Fortran. GNU Fortran is developed on the mainline of GCC and has been part of the compiler collection since the 4.0.0 release. We provide read access to our development sources for everybody by [anonymous SVN](../svn.html). If you do not have SVN access (for instance if you are behind a firewall prohibiting the SVN protocol), you might want to download [snapshots](../snapshots.html).

Contributions will be reviewed by at least one of the following people:

-   Paul Brook

-   Steven Bosscher

-   Bud Davis

-   Jerry DeLisle

-   Toon Moene

-   Tobias Schlueter

-   Janne Blomqvist

-   Steve Kargl

-   Thomas Koenig

-   Paul Thomas

-   Janus Weil

-   Daniel Kraft

-   Daniel Franke

Under the rules specified below:

-   All [normal requirements](../contribute.html) for patch submission (assignment of copyright to the FSF, testing, ChangeLog entries, etc) still apply, and reviewers should ensure that these have been met before approving changes.

-   Approval should be necessary for patches which don't fall under the obvious rule. So, with the approver list put in place, everybody (except maintainers) should still seek approval for his/her patches. We have found the mutual peer review process really works well.

-   Patches should only be reviewed by people who know the affected parts of the compiler. (i.e. the reviewer has to be sure he/she knows stuff well enough to make a good judgment.)

-   Large/complicated patches should still go by one of our maintainers, or team consensus.

-   We are all reasonable people, and nobody is working under employer pressure or needs an ego-boost badly, so in general we assume that no-one deliberately does anything stupid :-)

The directories involved are:

1.  gcc/gcc/fortran/

2.  gcc/gcc/testsuite/gfortran.dg/

3.  gcc/gcc/testsuite/gfortran.fortran-torture/

4.  gcc/libgfortran/

## Documentation

The manuals for release and current development versions of GNU Fortran can be downloaded from the [wiki documentation](http://gcc.gnu.org/wiki/GFortran#download) page or the [GCC online documents](http://gcc.gnu.org/onlinedocs/) page.

## Usage

Here is a short [explanation](http://gcc.gnu.org/wiki/GFortranUsage) on how to invoke and use the compiler once you have built it (or downloaded the binary).

## Suggested Reading

We provide links to [other information](../readings.html#fortran) relevant to Fortran programmers; the [GFortran wiki](http://gcc.gnu.org/wiki/GFortran#manuals) contains further links.
