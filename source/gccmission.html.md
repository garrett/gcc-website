# GCC Development Mission Statement (1999-04-22)

GCC development is a part of the [GNU Project](http://www.gnu.org/), aiming to improve the compiler used in the GNU system including the GNU/Linux variant. The GCC development effort uses an open development environment and supports many other platforms in order to foster a world-class optimizing compiler, to attract a larger team of developers, to ensure that GCC and the GNU system work on multiple architectures and diverse environments, and to more thoroughly test and extend the features of GCC.

## Free Software Project

-   Supporting the goals of the GNU project, as defined by the FSF.
-   Compilers are available under the terms of the GPL.
-   Copyrights for the compilers are to be held by the FSF.
-   Other components (runtime libraries, testsuites, etc) will be available under various free licenses with copyrights being held by individual authors or the FSF.
-   All legal relationships with contributors and users are the responsibility of the FSF.
-   Patches must be legally acceptable for inclusion into the GNU project.

## Design and Development Goals

-   New languages
-   New optimizations
-   New targets
-   Improved runtime libraries
-   Faster debug cycle
-   Various other infrastructure improvements

## Open Development Environment

-   Encourage cooperation and communication between developers.
-   Work more closely with "consumers".
-   Code available to everyone at any time, and everyone is welcome to participate in development.
    -   Patches will be considered equally based on their technical merits.
    -   All individuals and companies are welcome to contribute as long as they accept the groundrules.
-   Open mailing lists.
-   Developer friendly tools and procedures (i.e., cvs, multiple maintainers, etc).
-   Conflicts of interest exist for many GCC developers; the developers as well as the [GCC Steering Committee](steering.html) will not allow those conflicts of interest to have an effect on the development of GCC.

