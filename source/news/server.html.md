# GCC Network/Server Upgrades

October 12, 1999

First, Cygnus has installed another T1 link to help with the ever increasing bandwidth requirements for hosting GCC and other open source projects. I believe the last bandwidth report indicated we were pumping 45+G for http and ftp access, another 1-2G for mailing lists and an unknown (but large) sum for CVS access.

Second, Cygnus (Jason Molenda in particular) installed a new server for the open source projects hosted at Cygnus. The old server had pegged out in the spring/early summer; Jason & I had been nursing it along until we had the new server ready.

The new server should have enough disk space, IO bandwidth & processing power to fill our expected needs for the next 18-24 months.

At some point in the future, the old server will be resurrected as a dedicated anonymous CVS server for Cygnus hosted open source projects. Additionally, we will also begin to establish anonymous CVS mirrors as host sites express interest and resource availability.
