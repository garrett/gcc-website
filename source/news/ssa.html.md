# Static Single Assignment

March 9, 2000

We are pleased to announce that [CodeSourcery, LLC](http://www.codesourcery.com) and [Cygnus, a Red Hat company](http://www.redhat.com) have contributed an implementation of the static single assignment (SSA) representation for the GCC compiler. SSA is used in many modern compilers to facilitate a wide range of powerful optimizations. Now that SSA has been added to GCC, many optimizations can be added that were previously difficult or impossible.

In a function that has been converted to SSA form, each register is assigned a value only once. This simplification makes implementation of optimization algorithms possible or easier, since the path from assignment to use of a register is more easily determined. After optimizations have been performed, the function is converted out of SSA form before code generation proceeds.
