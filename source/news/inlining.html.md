# Inlining Improvements

December 5, 1999

We are pleased to announce that [CodeSourcery, LLC](http://www.codesourcery.com), as part of a continuing contract with the Accelerated Strategic Computing Initiative (ASCI) program at the Los Alamos National Laboratory, has contributed a new inliner for C++.

This inliner works at an earlier phase of the compiler than the traditional GCC inliner. As a result, the compiler may use dramatically less time and memory to compile programs that make heavy use of templates, such as C++ expression-template programs. One program that previously required 247MB of memory to compile, and about six minutes of compile time, now takes only 157MB and about two minutes to compile. Although this inliner currently only works on C++ code, we expect that it will soon be used in C, Java, and other languages as well.

In addition, the new inliner may occasionally generate somewhat better code since more high-level information is available to optimizers. In the future, the new inliner will be used to facilitate a number of new optimizations, like scattering and gathering of loads and stores. In addition, further compile-time memory and speed improvements are likely through simplifications of the code after inlining. These optimizations will greatly reduce the C++ "abstraction penalty" and improve the usability of the GNU C++ compiler.
