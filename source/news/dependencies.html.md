# Dependency Generation Improvements

22 January 2001

The following is a brief summary of the improvements made to the preprocessor's Makefile dependency generation functionality.

-   Consider the simple example of a file `test.c` that includes the file `test.h`, and the command line

            /usr/bin/gcc -MD -c test.c -o subdir/bar.o
          

    With previous versions of CPP, this command line would leave a file `test.d` in the current directory, and its contents would be

            test.o: test.c test.h
          

    which is pretty useless. The latest versions of CPP instead do what you probably want - they make the target `subdir/bar.o`, place the dependency file in the same directory as the object file, and name it after the object file. Thus in the file `subdir/bar.d` you will find

            subdir/bar.o: test.c test.h
          

-   By default CPP uses the main file name, excluding any path, and appends the object suffix, normally \`\`.o'', to it to obtain the name of the target for dependency generation. With `-MT` or `-MQ` you can specify a target yourself, overriding the default one.

    If you want multiple targets, you can specify them as a single argument to a switch, or use multiple switches. The targets you specify are output in the order they appear on the command line. `-MQ` is identical to `-MT`, except that the target name is quoted for Make, but with `-MT` it isn't. For example, `-MT '$(objpfx)foo.o'` gives

            $(objpfx)foo.o: /tmp/foo.c
          

    but `-MQ '$(objpfx)foo.o'` gives

            $$(objpfx)foo.o: /tmp/foo.c
          

    The default target is automatically quoted, as if it were given with `-MQ`.

-   When used with `-M` or `-MM`, `-MF` specifies a file to write the dependencies to. This allows the preprocessor to write the preprocessed file to stdout normally. If no `-MF` switch is given, and one is not implied by `-MD` or `-MMD`, CPP sends the rules to stdout and suppresses normal preprocessed output.

-   Finally, `-MP` instructs CPP to add a phony target for each dependency other than the main file, causing each to depend on nothing. These dummy rules work around errors `make` gives if you remove header files without updating the `Makefile` to match.

    This is typical output:-

        test.o: /tmp/test.c /tmp/test.h

        /tmp/test.h:
          


