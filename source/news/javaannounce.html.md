# libgcj Announcement

April 7, 1999

We are very pleased to announce the public release of libgcj, the run-time component of GCJ, the GNU Compiler for Java. This allows you to build executable programs from Java source, without needing any JDK components.

Libgcj includes the core run-time environment, such as object management, garbage collection, thread support. It also includes the core Java classes from java.lang, java.util, and parts of java.io, java.net, and java.text. Many things (such as object serialization, java.awt, java.util.zip, and most of java.lang.reflect) are not yet implemented.

Libgcj is licensed under the terms of the LGPL; see the file COPYING.LIB for details.

gdb 4.18, to be released very soon, is Java-aware and can debug Gcj-compiled programs. This release means that there is a complete suite of free software tools for compiled Java.

For instructions on downloading and installation, and for more information on gcj and libgcj in general, see the Gcj and Libgcj homepage at [http://gcc.gnu.org/java/](../java/).
