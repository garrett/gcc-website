# GCC mirror sites

[Our releases](releases.html) are available on the [GNU FTP server and its mirrors](http://www.gnu.org/prep/ftp.html). The following sites mirror the gcc.gnu.org FTP site (Phoenix, Arizona, USA) directly:

-   Austria: [gd.tuwien.ac.at](ftp://gd.tuwien.ac.at/gnu/gcc/), thanks to Antonin.Sprinzl at tuwien.ac.at
-   Bulgaria: [gcc.igor.onlinedirect.bg](http://gcc.igor.onlinedirect.bg/), thanks to igor at onlinedirect.bg
-   Canada: <http://gcc.parentingamerica.com>, thanks to James Miller (jmiller at parentingamerica.com).
-   Canada: <http://gcc.skazkaforyou.com>, thanks to Sergey Ivanov (mirrors at skazkaforyou.com)
-   France (no snapshots): [ftp.lip6.fr](ftp://ftp.lip6.fr/pub/gcc/), thanks to ftpmaint at lip6.fr
-   France, Brittany: [ftp.irisa.fr](ftp://ftp.irisa.fr/pub/mirrors/gcc.gnu.org/gcc/), thanks to ftpmaint at irisa.fr
-   France, Versailles: [ftp.uvsq.fr](ftp://ftp.uvsq.fr/pub/gcc/), thanks to ftpmaint at uvsq.fr
-   Germany, Berlin: [ftp.fu-berlin.de](ftp://ftp.fu-berlin.de/unix/languages/gcc/), thanks to ftp at fu-berlin.de
-   Germany: [ftp.gwdg.de](ftp://ftp.gwdg.de/pub/misc/gcc/), thanks to emoenke at gwdg.de
-   Germany: [ftp.mpi-sb.mpg.de](ftp://ftp.mpi-sb.mpg.de/pub/gnu/mirror/gcc.gnu.org/pub/gcc/), thanks to ftpadmin at mpi-sb.mpg.de
-   Germany: <http://gcc.cybermirror.org>, thanks to Sascha Schwarz (cm at cybermirror.org)
-   Greece: [ftp.ntua.gr](ftp://ftp.ntua.gr/pub/gnu/gcc/), thanks to ftpadm at ntua.gr
-   Hungary, Budapest: [robotlab.itk.ppke.hu](http://robotlab.itk.ppke.hu/gcc/), thanks to Adam Rak (neurhlp at gmail.com)
-   Japan: [ftp.dti.ad.jp](ftp://ftp.dti.ad.jp/pub/lang/gcc/), thanks to IWAIZAKO Takahiro (ftp-admin at dti.ad.jp)
-   Japan: [ftp.tsukuba.wide.ad.jp](http://ftp.tsukuba.wide.ad.jp/software/gcc/), thanks to Kohei Takahashi (tsukuba-ftp-servers at tsukuba.wide.ad.jp)
-   Latvia, Riga: [mirrors.webhostinggeeks.com/gcc/](http://mirrors.webhostinggeeks.com/gcc/), thanks to Igor (whg.igp at gmail.com)
-   The Netherlands, Nijmegen: [ftp.nluug.nl](ftp://ftp.nluug.nl/mirror/languages/gcc), thanks to Jan Cristiaan van Winkel (jc at ATComputing.nl)
-   Slovakia, Bratislava: [gcc.fyxm.net](http://gcc.fyxm.net/), thanks to Jan Teluch (admin at 2600.sk)
-   UK: <ftp://ftp.mirrorservice.org/sites/sourceware.org/pub/gcc/>, thanks to mirror at mirrorservice.org
-   UK, London: <http://gcc-uk.internet.bs>, thanks to Internet.bs (info at internet.bs)
-   US, Saint Louis: <http://gcc.petsads.us>, thanks to Sergey Kutserey (s.kutserey at gmail.com)
-   US, San Jose: [http://www.netgull.com](http://www.netgull.com/gcc/), thanks to admin at netgull.com

The archives there will be signed by one of the following GnuPG keys:

-   1024D/745C015A 1999-11-09 Gerald Pfeifer \<gerald@pfeifer.com\>
     Key fingerprint = B215 C163 3BCA 0477 615F 1B35 A5B3 A004 745C 015A
-   1024D/B75C61B8 2003-04-10 Mark Mitchell \<mark@codesourcery.com\>
     Key fingerprint = B3C4 2148 A44E 6983 B3E4 CC07 93FA 9B1A B75C 61B8
-   1024D/902C9419 2004-12-06 Gabriel Dos Reis \<gdr@acm.org\>
     Key fingerprint = 90AA 4704 69D3 965A 87A5 DCB4 94D0 3953 902C 9419
-   1024D/F71EDF1C 2000-02-13 Joseph Samuel Myers \<jsm@polyomino.org.uk\>
     Key fingerprint = 80F9 8B2E 0DAB 6C82 81BD F541 A7C8 C3B2 F71E DF1C
-   2048R/FC26A641 2005-09-13 Richard Guenther \<richard.guenther@gmail.com\>
     Key fingerprint = 7F74 F97C 1034 68EE 5D75 0B58 3AB0 0996 FC26 A641
-   1024D/C3C45C06 2004-04-21 Jakub Jelinek \<jakub@redhat.com\>
     Key fingerprint = 33C2 35A3 4C46 AA3F FB29 3709 A328 C3A2 C3C4 5C06

If you wish to host a new mirror site, please contact <gcc@gcc.gnu.org>. Include the URL of the gcc.gnu.org mirror area, the country/city where the mirror is located, and a contact e-mail address (and name if appropriate). If you limit access to your mirror ensure the gcc.gnu.org subnet is granted access in any case.
