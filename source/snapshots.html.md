# GCC Snapshots

We recommend that you use [SVN to access our current development sources](svn.html).

We also make regular snapshots available for FTP download from our [mirror sites](mirrors.html) about once a week. These snapshots are intended to give everyone access to work in progress. Any given snapshot may generate incorrect code or even fail to build.

If you plan on downloading and using snapshots, we highly recommend you subscribe to the GCC mailing lists. See [mailing lists](lists.html) on the main GCC page for instructions on how to subscribe.

When using the diff files to update from older snapshots to newer snapshots, make sure to use "-E" and "-p" arguments to patch so that empty files are deleted and full pathnames are provided to patch. If your version of patch does not support "-E", you'll need to get a newer version. Also note that you may need autoconf, autoheader and various other programs if you use diff files to update from one snapshot to the next.

`contrib/gcc_update` can be used to apply diffs between successive snapshot versions and preserve relations between generated files so that autoconf et al aren't needed. This is documented in comments in contrib/gcc\_update.

The program `md5sum` — which is included with the [GNU Coreutils](https://www.gnu.org/software/coreutils/) — can be used to verify the integrity of a snapshot or release. The release script generates the file `MD5SUMS` that provides a 128-bit checksum for every file in the tarball.  Use the following command to verify the sources:
 `md5sum --check MD5SUMS | grep -v OK$`
