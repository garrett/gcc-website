# Links and Selected Readings

## GCC-specific Literature

-   [An Introduction to GCC](http://www.network-theory.co.uk/gcc/intro/) by Brian J. Gough.
-   [GNU C Compiler Internals (Wikibook)](http://en.wikibooks.org/wiki/GNU_C_Compiler_Internals), numerous contributors.
-   [Compilation of Functional Programming Languages using GCC -- Tail Calls](http://users.cecs.anu.edu.au/~baueran/thesis/) by Andreas Bauer.
-   [Porting GCC for Dunces](http://ftp.axis.se/pub/users/hp/pgccfd/) by Hans-Peter Nilsson \<<hans-peter.nilsson@axis.com>\>.
-   [Using, Maintaining and Enhancing COBOL for the GNU Compiler Collection (GCC)](http://cobolforgcc.sourceforge.net/cobol_toc.html) by Joachim Nadler and Tim Josling \<<tej@melbpc.org.au>\>.
-   [The V3 multi-vendor standard C++ ABI](http://mentorembedded.github.com/cxx-abi/) is used in GCC releases 3.0 and above.
-   [Compiling and testing complete gcc/glibc cross-toolchains for Linux targets](http://kegel.com/crosstool/) by Dan Kegel.

## Chip Documentation

The list below is based on the subdirectory names of the gcc/config directory, which typically matches with the CPU name in the configuration name. In some cases, different (but similar) CPUs are put into one directory; the names in parentheses list such similar CPU names.

-   alpha
    Manufacturer: Compaq (DEC)
    [Calling Standard for AXP Systems](http://www.tru64unix.compaq.com/docs/base_doc/DOCUMENTATION/V51A_HTML/ARH9MBTE/TITLE.HTM) documents the calling conventions for Digital Unix; chapters 2-4 apply to other Alpha Unix systems as well.
    [Alpha Hardware Reference Manuals](http://h18000.www1.hp.com/cpq-alphaserver/technology/chip-docs.html)
-   andes (nds32)
    Manufacturer: Various licenses of Andes Technology Corporation.
    CPUs include: AndesCore families N7, N8, SN8, N9, N10, N12 and N13.
    [Andes Documentation](http://www.andestech.com/en/products/documentation.htm)
    GDB includes a simulator for all CPUs.
-   arc
    Manufacturer: ARC Cores (Argonaut)
-   arm (armv2, thumb)
    Manufacturer: Various, by license from ARM
    CPUs include: ARM7 and ARM7T series (eg. ARM7TDMI), ARM9 and StrongARM
    [ARM Documentation](http://infocenter.arm.com/help/index.jsp)
-   AVR
    Manufacturer: Atmel
    [AVR Documentation](http://www.atmel.com/products/microcontrollers/avr/)
-   Blackfin
    Manufacturer: Analog Devices
    [uClinux and GNU toolchains for the Blackfin](http://blackfin.uclinux.org/gf/)
    [Blackfin Documentation](http://www.analog.com/en/processors-dsp/blackfin/products/index.html)
-   c4x
    Manufacturer: Texas Instruments
    Exact chip name: TMS320C4X
-   C6X
    Manufacturer: Texas Instruments
    Exact chip name: TMS320C6X
    [Site for the Linux on C6X project](http://linux-c6x.org/)
-   CR16
    Manufacturer: National Semiconductor
    Acronym stands for: CompactRISC 16-bit
    GDB includes a simulator
-   CRIS
    Manufacturer: Axis Communications
    Acronym stands for: Code Reduced Instruction Set
    The CRIS architecture is used in the ETRAX system-on-a-chip series.
    [Site with CPU documentation](http://developer.axis.com/)
-   Epiphany
    Manufacturer: Adapteva
    [Manufacturer's website](http://www.adapteva.com/) with additional information about the Epiphany architecture.
-   fr30
    Manufacturer: Fujitsu
    Acronym stands for: Fujitsu RISC
    GDB includes a [CGEN](http://sourceware.org/cgen/) generated simulator.
-   h8300
    Manufacturer: Renesas
    Exact chip name: H8/300
    GDB includes a simulator.
    [H8/300 Application Binary Interface for GCC](http://gcc.gnu.org/projects/h8300-abi.html).
-   i386 (i486, i586, i686, i786)
    Manufacturer: Intel
    [Intel®64 and IA-32 Architectures Software Developer's Manuals](http://developer.intel.com/products/processor/manuals/index.htm)
    Some information about optimizing for x86 processors, links to x86 manuals and documentation:
    <http://www.agner.org/optimize/>
    [www.sandpile.org:](http://www.sandpile.org) Christian Ludloff's technical x86 processor information.
-   i860
    Manufacturer: Intel
-   m32c
    Manufacturer: Renesas
    [Renesas M16C Family (R32C/M32C/M16C) Site](http://www.renesas.com/products/mpumcu/m16c/m16c_landing.jsp)
    GDB includes a simulator.
-   m32r
    Manufacturer: Renesas
    GDB includes a CGEN generated simulator.
-   m68hc11 (m68hc12)
    Manufacturer: Motorola
    GDB includes a 68HC11 and a 68HC12 simulator.
-   m68k
    Manufacturer: Motorola
-   mcore
    Manufacturer: Motorola
    GDB includes a simulator.
-   MeP
    Manufacturer: Toshiba
    SID includes a MeP simulator.
-   MicroBlace
    Manufacturer: Xilinx
    [MicroBlaze Processor Reference Guide](http://www.xilinx.com/support/documentation/sw_manuals/xilinx11/mb_ref_guide.pdf)
    GDB includes a simulator for an earlier version of the processor.
-   mips (mipsel, mips64, mips64el)
    The \*el variants are little-endian configurations.
-   MMIX
    Manufacturer: none. There is a simulator, see links below.
    Acronym stands for: Roman numeral for 2009, pronounced [EM-micks]. The number stands for the average of numbers of "14 actual computers very similar to MMIX". The name may also be due to a predecessor appropriately named MIX.
    MMIX is used in program examples in [Donald E. Knuth](http://www-cs-faculty.stanford.edu/~knuth/)'s [The Art of Computer Programming](http://www-cs-faculty.stanford.edu/~knuth/taocp.html) (ISBN 0-201-89683-4).
    [Knuth's MMIX page](http://www-cs-faculty.stanford.edu/~knuth/mmix.html) has more information about MMIX. Knuth also wrote a [book specifically about MMIX](http://www-cs-faculty.stanford.edu/~knuth/mmixware.html) (MMIXware, ISBN 3-540-66938-8).
-   mn10300
    Manufacturer: Matsushita
    Alternate chip name: AM30
    GDB includes a simulator.
-   msp430
    Manufacturer: Texas Instruments
    GDB includes a simulator.
-   ns32k
    Manufacturer: National Semiconductor
    [NS32FX200 Home Page](http://www.national.com/pf/NS/NS32FX200.html)
-   pa
    Manufacturer: HP
    [PA-RISC](http://h21007.www2.hp.com/portal/site/dspp/menuitem.1b39e60a9475acc915b49c108973a801/?chid=6dc55e210a66a010VgnVCM100000275d6e10RCRD#topic_pa_risc_architecture) is preferred over the older HPPA acronym (Hewlett-Packard Precision Architecture).
    <http://parisc-linux.org/documentation/index.html> is another good source of PA-RISC documention.
-   pdp11
    Manufacturer: DEC
    [PDP-11 FAQ](http://www.village.org/pdp11/faq.html)
    [Simulators](http://simh.trailing-edge.com/)
-   rs6000 (powerpc, powerpcle)
    Manufacturer: IBM, Motorola
    [AIX V4.3 Assembler Language Ref.](http://publib16.boulder.ibm.com/pseries/en_US/infocenter/base/43_docs/aixassem/alangref/toc.htm)
    [AIX 5L Assembler Language Ref.](http://publibn.boulder.ibm.com/doc_link/en_US/a_doc_lib/aixassem/alangref/alangreftfrm.htm)
    [Documentation and tools at power.org](https://www.power.org/documentation/)
-   rx
    Manufacturer: Renesas
    [RX610 Hardware Manual](http://documentation.renesas.com/eng/products/mpumcu/rej09b0460_rx610hm.pdf)
-   sh
    Manufacturer: Renesas, or various licensed by SuperH Inc
    CPUs include: SH1, SH2, SH2-DSP, SH3, SH3-DSP, SH4, SH5 series.
    [Renesas SuperH Processors](http://www.renesas.com/products/mpumcu/superh/superh_landing.jsp)
    [SuperH Home Page](http://www.superh.com/)
    GDB includes a simulator.
-   sparc (sparclite, sparc64)
    Manufacturer: Sun
    Acronym stands for: Scalable Processor ARChitecture
-   tilegx
    Manufacturer: Tilera
    [Documentation](http://www.tilera.com/scm/docs/index.html)
-   tilepro
    Manufacturer: Tilera
    [Documentation](http://www.tilera.com/scm/docs/index.html)
-   v850
    Manufacturer: NEC
-   vax
    Manufacturer: DEC
-   xtensa
    Manufacturer: Tensilica
-   z/Architecture (S/390)
    Manufacturer: IBM
    [ESA/390 Principles of Operation](http://publibfp.boulder.ibm.com/cgi-bin/bookmgr/BOOKS/dz9ar008/CCONTENTS)
    [z/Architecture Principles of Operation](http://publibfp.boulder.ibm.com/cgi-bin/bookmgr/BOOKS/dz9zr002/CCONTENTS)

## Collected Papers/Sites on Standards, Compilers, Optimization, Etc.

### C information

-   C standards information:
    -   [WG14 (C standards committee)](http://www.open-std.org/jtc1/sc22/wg14/)
    -   [Draft C99 Rationale](http://www.open-std.org/jtc1/sc22/wg14/www/docs/n897.pdf)
    -   [C99 Defect Reports](http://www.open-std.org/jtc1/sc22/wg14/www/docs/summary.htm)
    -   [C89 Rationale (sources)](ftp://ftp.uu.net/doc/standards/ansi/X3.159-1989/)
    -   [C89 Rationale (HTML)](http://www.lysator.liu.se/c/rat/title.html)
    -   [C89 Technical Corrigendum 1](http://www.open-std.org/jtc1/sc22/wg14/www/docs/tc1.htm)
    -   [C89 Technical Corrigendum 2](http://www.open-std.org/jtc1/sc22/wg14/www/docs/tc2.htm)
    -   [C89 Defect Reports](http://www.open-std.org/jtc1/sc22/wg14/www/docs/dr.htm)
-   Sequence point rules in C:
    -   [A formal model of sequence points and related issues by Clive Feather](http://www.open-std.org/jtc1/sc22/wg14/www/docs/n925.htm)
    -   [C formalised in HOL, thesis by Michael Norrish](http://www.cl.cam.ac.uk/techreports/UCAM-CL-TR-453.pdf)
    -   [Sequence points analysis by Raymond Mak](http://www.open-std.org/jtc1/sc22/wg14/www/docs/n926.htm)
    -   [Another formalism for sequence points by D. Hugh Redelmeier](http://www.open-std.org/jtc1/sc22/wg14/www/docs/n927.htm)
-   C historical information:
    -   [The Development of the C Language](http://cm.bell-labs.com/cm/cs/who/dmr/chist.html), by Dennis M. Ritchie (also in [PostScript](http://cm.bell-labs.com/cm/cs/who/dmr/chist.ps) and [PDF](http://cm.bell-labs.com/cm/cs/who/dmr/chist.pdf)).
    -   An early [C Reference Manual](http://cm.bell-labs.com/cm/cs/who/dmr/cman.ps).

### C++ information

-   [ISO Committee homepage (defects list, etc)](http://www.open-std.org/jtc1/sc22/wg21/)

-   [The SGI STL page](http://www.sgi.com/tech/stl/)
-   [STLport homepage](http://www.stlport.org/)

-   [Template Numeric Toolkit](http://math.nist.gov/tnt/)
-   [The Boost C++ Libraries](http://www.boost.org)

-   [Internet sites for C++ users](http://www.robertnz.net/cpp_site.html)

-   [Nathan Myers' locale page](http://www.cantrip.org/cpp.html)
-   [Nicolai Josuttis' Standard Library book](http://www.josuttis.com/libbook/)

### Objective-C Information

-   [Objective-C Language Description](http://developer.apple.com/documentation/Cocoa/Conceptual/ObjectiveC/) (Apple Computer)

### Fortran information {#fortran}

-   Fortran standards information:
    -   [Fortran standards committee](http://www.j3-fortran.org/) (includes references to the current draft of the Fortran standard)
-   Fortran historical information:
    -   [FORTRAN 77 Standard](http://www.fortran.com/F77_std/rjcnf.html)
-   [Fortran FAQs, books and software](http://www.fortran.com/info.html)
-   Testing and Validation - Some packages aimed at Fortran compiler validation.
    -   [FORTRAN 77 test suite](http://www.itl.nist.gov/div897/ctg/fortran_form.htm) by the NIST Information Technology Laboratory ([license](http://www.itl.nist.gov/div897/ctg/software.htm)) The test suite contains legal and operational Fortran 77 code.
    -   IDRIS [Low level bench tests of Fortran 95](http://www.idris.fr/data/publications/F95/test_F95_english.html). It tests some Fortran 95 intrinsics.
    -   The g77 testsuite (which is part of GCC).
    -   Test suite of [ftnchek](http://www.dsm.fordham.edu/~ftnchek/), included in its distribution. It contains some illegal code and therefore makes it possible to stress the compiler error handling.
    -   [Checking properties of the compiler and the run-time environment](http://ftp.cac.psu.edu/pub/ger/fortran/chksys.htm) by Arjen Markus (source provided).
    -   [gdbf95](http://gdbf95.sourceforge.net/) test suite.
    -   Tests of run-time checking capabilities
        -   [Polyhedron tests](http://www.polyhedron.com/)
        -   [compilers test sets](http://ftp.aset.psu.edu/pub/ger/fortran/test/) by H. Knoble & A. Desitter
-   Other resources:
    -   [Michel Olagnon's Fortran 90 List](http://www.ifremer.fr/ditigo/molagnon/fortran90/engfaq.html) contains a "Tests and Benchmarks" section mentioning commercial test suites.
    -   [Herman D. Knoble's Fortran Resources](http://www.personal.psu.edu/faculty/h/d/hdk/fortran.html) contain some sections on compiler validation and benchmarking.
    -   Complying with Fortran 90, How does the current crop of Fortran 90 compilers measure up to the standard?, Steven Baker, Dr Dobb's, January 1995. It described the results of several commercial test suites.
-   Fortran packages - Some packages containing (mostly) legal Fortran code.
    -   Books with source code:
        -   [Example programs](http://www.fortranplus.co.uk/fortran_books.html) of Introducing Fortran 95 by Ian Chivers and Jane Sleightholme.
        -   [Examples](http://www.fortran.com/F/books.html) of Essential Fortran 90 & 95 by Loren Meissner.
        -   [Source code](http://www.mhhe.com/engcs/general/chapman/intro/) of Introduction To Fortran 90/95 by Stephen Chapman.
        -   [Source code](http://www.mhhe.com/engcs/general/chapman/) of Fortran 90/95 for Scientists and Engineers by Stephen Chapman.
-   Historical material - A few links for your enjoyment.
    -   [Historic Documents in Computer Science](http://www.fh-jena.de/~kleine/history/) by Karl Kleine
    -   [The writings of Edsger W. Dijkstra (RIP)](http://www.cs.utexas.edu/users/EWD/)

### Ada information

-   Ada standards information:
    -   [WG9 (Ada standards committee):](http://www.open-std.org/jtc1/sc22/wg9/)
        -   [Ada Issues](http://www.ada-auth.org/ais.html)
    -   [List of Ada standards (Ada Information Clearinghouse):](http://www.adaic.org/ada-resources/standards/)
        -   [Annotated Ada 2005 Reference Manual](http://www.adaic.org/resources/add_content/standards/05aarm/html/AA-TTL.html)
        -   [Ada 2005 Reference Manual](http://www.adaic.org/resources/add_content/standards/05rm/html/RM-TTL.html)
        -   [Ada 2005 Rationale](http://www.adaic.org/resources/add_content/standards/05rat/html/Rat-TTL.html)
        -   [Annotated Ada 95 Reference Manual](http://www.adaic.org/resources/add_content/standards/95aarm/AARM_HTML/AA-TTL.html)
        -   [Ada 95 Reference Manual](http://www.adaic.org/resources/add_content/standards/95lrm/ARM_HTML/RM-TTL.html)
        -   [Ada 95 Rationale](http://www.adaic.org/resources/add_content/standards/95rat/rat95html/rat95-contents.html)
        -   [Ada 83 Reference Manual](http://archive.adaic.com/standards/83lrm/html/ada_lrm.html)
        -   [Ada 83 Rationale](http://archive.adaic.com/standards/83rat/html/Welcome.html)
-   Related standards:
    -   [Ada Semantic Interface Specification (ASIS)](http://www.sigada.org/WG/asiswg/)
-   Compiler validation:
    -   [Ada Conformity Assessment Test Suite (ACATS)](http://www.ada-auth.org/acats.html)
-   Other resources:
    -   [AdaCore — Libre Site](http://libre.adacore.com/)
    -   [GNAT: The GNU Ada Compiler](https://www2.adacore.com/gap-static/GNAT_Book/html/)
    -   [Ada Quality & Style Guide](http://www.adaic.org/resources/add_content/docs/95style/html/cover.html)
    -   [Guide for the use of the Ada Ravenscar Profile in high integrity systems](http://www.sigada.org/ada_letters/jun2004/ravenscar_article.pdf)

### Go information

-   [General Go information](http://golang.org/)
-   [Go language specification](http://golang.org/ref/spec)

### Modula 3 information

-   <http://www.cmass.com>
-   <http://www.modula3.org>

### Miscellaneous information

-   [What Every Computer Scientist Should Know about Floating-Point Arithmetic](http://www.validlab.com/goldberg/paper.pdf) by David Goldberg, including Doug Priest's supplement (PDF format)
-   [Differences Among IEEE 754 Implementations](http://www.validlab.com/goldberg/addendum.html) by Doug Priest (included in the PostScript-format document above)
-   [IEEE 754r](http://en.wikipedia.org/wiki/IEEE_754r), an ongoing revision to the IEEE 754 floating point standard.
-   [Information on the Digital UNIX/Compaq Tru64 UNIX object file format](http://www.tru64unix.compaq.com/docs/base_doc/DOCUMENTATION/V51_HTML/SUPPDOCS/OBJSPEC/TITLE.HTM)

-   [IBM Journal of Research and Development](http://www.almaden.ibm.com/journal/)
-   [System V PowerPC ABI](http://refspecs.freestandards.org/elf/elfspec_ppc.pdf)
-   [Application Binary Interface for the ARM Architecture (EABI)](http://www.arm.com/products/DevTools/ABI.html)
-   [DWARF Workgroup](http://dwarfstd.org/)
-   [Debugging/object info](ftp://ftp.sgi.com/sgi/dev/davea/objectinfo.html) by David B.Anderson (including links to some DWARF tools)

-   EG3 maintains a list of compiler Internet resources, including FAQ's, papers, hot list pages, potential software/shareware, all known companies, etc.:
    -   <http://www.eg3.com/embeddedsoftware/compiler.htm>
    -   These resource pages are published as part of EG3's Free Electronic Engineers' Toolbox at:
        -   <http://www.eg3.com/index.htm>

-   [Links related to many compiler topics](http://www.compilerconnection.com/)
-   [comp.compilers archive](http://compilers.iecc.com/index.phtml)
-   Steven Muchnick (1997) "Advanced Compiler Design and Implementation". 880pp. ISBN: 1-55860-320-4.
-   Robert Morgan (1998) "Building an Optimizing Compiler". 300pp. ISBN: 1-55558-179-X.
-   [The Open Group](http://www.opengroup.org/) has quite a bit on [POSIX and friends](http://pubs.opengroup.org/onlinepubs/009695399/toc.htm).
-   [Unicode](http://www.unicode.org) and [Unicode Normalization Forms](http://www.unicode.org/reports/tr15/).

## Chip Documentation of Obsolete Ports

Below is the list of ports that GCC used to support.

-   1750a
    Exact chip name: MIL-STD-1750A processor
    Manufacturers: various
    [Specification](http://legacy.cleanscape.net/stdprod/xtc1750a/resources/mil-std-1750.zip)
-   a29k
    Manufacturer: AMD
-   clipper
    Manufacturer: Intergraph
    Exact machine name: CLIPPER
-   convex (c1, c2, c3[248])
    Manufacturer: Convex (HP)
-   d30v
    Manufacturer: Mitsubishi
    There is no longer any reference to this chip anywhere on the manufacturer's web site; it may be dead.
    GDB includes a simulator.
-   dsp16xx
    Manufacturer: AT&T
-   m88k
    Manufacturer: Motorola
-   mn10200
    Manufacturer: Matsushita
    GDB includes a simulator.
-   pj (picoJava)
    Manufacturer: Sun
-   romp
    Manufacturer: IBM
    Acronym stands for: Research/Office Products MicroProcessor
    The ROMP was the processor inside the IBM PC/RT.

