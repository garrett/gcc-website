This page documents the procedure for making a new release branch.

1.  Execute the following commands, substituting appropriate version numbers:

    >     svn cp svn+ssh://gcc.gnu.org/svn/gcc/trunk svn+ssh://gcc.gnu.org/svn/gcc/branches/gcc-3_5-branch

2.  Update the file `gcc/DEV-PHASE` on the branch, to say "prerelease" instead of "experimental".
3.  Update the file `gcc/BASE-VER` on the mainline, to use the next major release number (e.g., 3.2 instead of 3.1).
4.  Update the GIT mirror by running on `gcc.gnu.org` the following command, substituting appropriate version numbers:

    >     $ cd /git/gcc.git/.git
    >     $ git symbolic-ref refs/heads/gcc-4_7-branch refs/remotes/gcc-4_7-branch

5.  Create a new `htdocs/gcc-VERSION` directory for the next major release in the `wwwdocs` repository and populate it with initial copies of `changes.html` and `criteria.html`.
6.  Add `index.html` and `buildstat.html` pages to the directory corresponding to the newly created release branch. Update the toplevel `buildstat.html` accordingly.
7.  Update the toplevel `index.html` page to show the new active release branch, the current release series, and active development (mainline). Update the version and development stage for mainline.
8.  Update `maintainer-scripts/crontab` on the mainline by adding an entry to make snapshots of the new branch and adjusting the version number of the mainline snapshots. Run `svn update` in the `scripts` directory of the gccadmin account, and then actually install the updated crontab there.
     Generate the next mainline snapshot manually, using the `-p` option of the `gcc_release` script. For that single run, adjust the script such that the announcement mail is sent to you personally so that you can adjust references to the previous snapshot in the `README` and `index.html` files of the new snapshot as well as the mail itself before relaying it.
9.  [Regenerate `gcc.pot`](translation.html#regen) and `cpplib.pot`. [Send them to the translation project](http://gcc.gnu.org/translation.html#submit) if no snapshot of this version was sent during development stage 3 or 4.
10. Create new versions in Bugzilla corresponding to the new mainline version. This can be accomplished by choosing "products", choosing "gcc", and editing the versions. Please do **not** delete old versions.
11. Create new target milestones in Bugzilla corresponding to the new mainline version. This can be accomplished by choosing "products", choosing "gcc", and editing the milestones. Please do **not** delete old target milestones.
12. Create new target milestones in Bugzilla for the second dot release on the branch. For example, when branching 3.4, create a milestone for 3.4.1 for PRs that can't be fixed in time for 3.4.0.
13. Ask Daniel Berlin to adjust all PRs with the just-branched version in their summary to also contain the new version corresponding to mainline.

