# Managing Bugs (Bugzilla and the testsuite)

This section contains information mostly intended for GCC contributors.

## Reporting and Fixing Bugs

If you *find* a bug, but you are not fixing it (yet):

1.  Create a (minimal) testcase and add a bug report including it to the bug database (Bugzilla).
2.  Enter the version number of the compiler in the *Known To Fail* field. If you are reporting the bug against a release branch, or the trunk, enter the version number of the next planned release from that branch.

If you want to *provide additional information* about a bug in the database:

-   If the bug is a segmentation fault in the compiler, [provide information about its location](segfault.html).
-   [Minimize the test case](minimize.html).
-   Try the test case with earlier and later versions of GCC to determine which versions it affects and whether it is a [regression](#regression).
-   If it is a regression, [identify the patch](reghunt.html) that introduced it.
-   Update the *Known To Fail* and *Known To Work* fields to reflect passing and failing versions that you know about.

If you *fix* a bug:

1.  Check in your fixes.
2.  If there is already a testcase in the testsuite, remove the XFAIL. Else, create a minimal testcase (maybe the bug database already provides one) and add it to our testsuite, marking it as PASS.
3.  If there is an associated bug report in the database, close it.
4.  If, after your fix, the bug is present on some release branches but not others, update the *Known To Fail* and *Known To Work* fields to indicate which versions still have the bug.

## Maintainer's View of Fields

Bugzilla offers a number of different fields. From a maintainer's perspective, these are the relevant ones and what their values mean:

The status and resolution fields define and track the life cycle of a bug. In addition to their [regular descriptions](http://gcc.gnu.org/bugzilla/page.cgi?id=fields.html), we also use two adition status values:

**WAITING**
:   The submitter was asked for further information, or asked to try out a patch. The PR remains in that state until the submitter responds.
**SUSPENDED**
:   Work on the problem has been postponed. This happens if a timely solution is not possible or is not cost-effective at the present time. The PR continues to exist, though a solution is not being actively sought. If the problem cannot be solved at all, it should be closed rather than suspended.

The following two fields describe how serious a bug is from a user's perspective (severity) and which priority we assign to it in fixing it:

### Severity {#severity align="center"}

This field describes the impact of a bug.

Critical

crashes, memory leaks and similar problems on code that is written in a common enough style to affect a significant fraction of users

Normal

major loss of functionality

Minor

minor loss of functionality, misspelled word, or other problem where an easy workaround exists

Enhancement

Request for enhancement

### Priority {#priority align="center"}

For [regressions](#regression) this field describes the importance and order in which a bug should be fixed. Priorities are set by the release management team only. If you think a priority is wrong, set it to P3 and add a note. The available priorities are:

P1

Most important. This generally labels a regression which the release manager feels should be addressed for the next release including wrong-code regressions.
 A P1 regression blocks the release.

P2

This generally indicates a regression users will notice on a major platform, which is not severe enough to block a release though. This includes bugs that were already present in a previous release.

P3

The default priority for new PRs which have not been prioritized yet. Priorities below P3 are not on the radar of release management.

P4

An important regression on a platform that is not in the list of primary or secondary targets or a regression that users will not see for release builds. This includes bugs with error-recovery or ice-checking keywords.

P5

A less important regression on a platform that is not in the list of primary or secondary targets.

### Assigned To

This is the person in charge of resolving the bug. Every time this field changes, the status changes to NEW to make it easy to see which new bugs have appeared on a person's list.

### Keywords

These are used to further classify the problem reports. Follow the link for a [complete list of the keywords including descriptions](http://gcc.gnu.org/bugzilla/describekeywords.cgi).

## Procedures and Policies

**Putting reports in components "C", "C++", and "optimization" in state "NEW"** requires that there is a reduced, small testcase. This makes sure that all NEW reports are really analyzed and are ready to be handed off to the people actually fixing bugs.

**Regressions** should be explicitly marked as such. The summary line should read

> [*branches-to-fix* regression] *rest-of-summary*

where *branches-to-fix* is the list of maintained branches (separated by slashes) that need fixing. The target milestone should be set to the next release of the newest active release branch that needs fixing (the rationale is that a patch will have to go to the newest release branch before any other release branch). The priority of a regression should initially be set to P3. The milestone and the priority can be changed by the release manager and his/her delegates.

**If a patch fixing a PR has been submitted**, a link to the message with the patch should be added to the PR, as well as the keyword "patch".

**Meta-bugs (reports with the keyword "meta-bug")** are used to group PRs that have a common denominator. Meta-bugs do not have testcases of their own, but provide links to regular PRs via Bugzilla's "depends on/blocks" mechanism instead: they depend on the regular PRs. Information concerning the majority of bugs blocking a meta-bug should be added to the meta-bug instead of each single PR.

**Bugs with keyword "ice-on-invalid-code"**, where GCC emits a sensible error message before issuing an ICE (the ICE will be replaced by the message "confused by earlier errors, bailing out" in release versions) should get "minor" severity and the additional keyword "error-recovery".

**Bugs in component "bootstrap"** that refer to older releases or snapshots/CVS versions should be put into state "WAITING", asking the reporter whether she can still reproduce the problem and to report her findings in any case (whether positive or negative).

-   If the response is "works now", close the report,
-   if the response is "still broken", change the state to "NEW", and
-   if no response arrives, close the PR after three months.

**Bugs that are in state "WAITING"** because they lack information that is necessary for reproducing the problem can be closed if no response was received for three months.

**When closing a PR** because it got fixed, please set the **target milestone** to the first release where it will be/has been fixed. Also adjust the target milestone when a fix is **backported**. Finally, please adjust the *Known To Fail* and *Known To Work* fields to record what was fixed.

**Duplicate PRs** should be marked as such. If you encounter a PR that is marked as "resolved fixed", but should be marked as a duplicate, please change the resolution. (You have to reopen the PR before you can resolve it as a duplicate.)
