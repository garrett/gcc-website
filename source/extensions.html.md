# GCC extensions

This is a list of (experimental) extensions to GCC which, for one reason or the other, did not (yet) make it into the official source tree. Please direct feedback and bug reports to their respective maintainers, not our mailing lists.

## [GCC MELT](http://gcc-melt.org)

MELT is a high-level *domain specific language* to ease the development of GCC extensions. It is available as a GCC experimental branch on `svn://gcc.gnu.org/svn/gcc/branches/melt-branch`, and also as a GCC [meta-] *plugin* (GPLv3 licensed, FSF copyrighted).

The MELT language is translated to C, and provides powerful features (pattern-matching, functional, object, reflective programming styles, ability to mix C and MELT code, Lisp look,...) to ease development of GCC plugin-like extensions.

## [StarPU](http://runtime.bordeaux.inria.fr/StarPU/)

StarPU is a GCC extension and run-time support library for hybrid CPU/GPU task programming. Its GCC plug-in allows programmers to annotate C code to describe tasks and their implementations. Each task may have one or more implementations, such as CPU implementations or implementations written in OpenCL.

StarPU's support library schedules tasks over the available CPU cores and GPUs, and is also responsible for scheduling any data transfers between main memory and GPUs.

## [PDP-10 port](http://pdp10.nocrew.org/gcc/)

This is an experimental port of GCC to the DEC PDP-10 architecture.

## Bounds checking patches for [GCC releases](http://sourceforge.net/projects/boundschecking/)

These patches add a `-fbounds-checking` flag that adds bounds checking tests to pointer and array accesses. Richard Jones developed the [patches against gcc 2.7](http://www.doc.ic.ac.uk/~phjk/BoundsChecking.html) in 1995. Herman ten Brugge is the current maintainer and has updated the patches for GCC 2.95.2 and later. William Bader has [patches as well](http://williambader.com/bounds/example.html#download).

You may freely mix object modules compiled with and without bounds checking. The bounds checker also includes replacements for `mem*` and `str*` routines and can detect invalid calls against checked memory objects, even from modules compiled without bounds checking.
