# C++1y/C++14 Support in GCC

GCC is beginning to introduce support for the next revision of the C++ standard, which is expected to be published in 2014.

C++1y features are available as part of the "mainline" GCC compiler in the trunk of [GCC's Subversion repository](../svn.html) and in GCC 4.8 and later. To enable C++1y support, add the command-line parameter `-std=c++1y` to your `g++` command line. Or, to enable GNU extensions in addition to C++0x extensions, add `-std=gnu++1y` to your `g++` command line.

**Important**: Because the ISO C++14 draft is still evolving, GCC's support is **experimental**. No attempt will be made to maintain backward compatibility with implementations of C++1y features that do not reflect the final standard.

## C++1y Language Features

The following table lists new language features that have been accepted into the C++1y standard. The "Proposal" column provides a link to the ISO C++ committee proposal that describes the feature, while the "Available in GCC?" column indicates the first version of GCC that contains an implementation of this feature (if it has been implemented).

|Language Feature|Proposal|Available in GCC?|
|:---------------|:-------|:----------------|
|Tweak to certain C++ contextual conversions|[N3323](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2012/n3323.pdf)|[4.9](../gcc-4.9/changes.html#cxx)|
|Binary literals|[N3472](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2012/n3472.pdf)|[4.3](../gcc-4.3/changes.html#cxx) (GNU) 
[4.9](../gcc-4.9/changes.html#cxx) (N3472)|
|Return type deduction for normal functions|[N3638](http://isocpp.org/files/papers/N3638.html)|[4.8](../gcc-4.8/changes.html#cxx) (N3386) 
[4.9](../gcc-4.9/changes.html#cxx) (N3638)|
|Runtime-sized arrays with automatic storage duration 
(Moved from the standard to a separate technical specification)|[N3639](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3639.html)|?.? (GNU VLAs) 
[4.9](../gcc-4.9/changes.html#cxx) (N3639)|
|Generalized lambda capture (init-capture)|[N3648](http://isocpp.org/files/papers/N3648.html)|[4.5](../gcc-4.5/changes.html#cplusplus) (partial) 
[4.9](../gcc-4.9/changes.html#cxx) (N3648)|
|Generic (polymorphic) lambda expressions|[N3649](http://isocpp.org/files/papers/N3649.html)|[4.9](../gcc-4.9/changes.html#cxx)|
|Variable templates|[N3651](http://isocpp.org/files/papers/N3651.pdf)|No [[WIP](http://gcc.gnu.org/ml/gcc-patches/2013-03/msg01295.html)]|
|Relaxing requirements on constexpr functions|[N3652](http://isocpp.org/files/papers/N3652.html)|No|
|Member initializers and aggregates|[N3653](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3653.html)|No|
|Clarifying memory allocation|[N3664](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3664.html)|N/A|
|Sized deallocation|[N3778](http://isocpp.org/files/papers/n3778.html)|No|
|[[deprecated]] attribute|[N3760](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3760.html)|[4.9](../gcc-4.9/changes.html#cxx) (N3797)|
|Single-quotation-mark as a digit separator|[N3781](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3781.pdf)|[4.9](../gcc-4.9/changes.html#cxx) (N3797)|

## Development Branches

### C++1y Concepts Branch

Concepts was a major feature planned for the C++11 standard, but it was eventually dropped due to concerns about both the description and implementability. Since the publication of C++11, people have been working on scaled-down versions of the concepts feature. One approach to concepts, known as [Concepts Lite](http://concepts.axiomatics.org/~ans/), has been prototyped in GCC and seems likely to be published as a Technical Specification around the time of the next standard. The initial implementation is available from the link above, and it is in the process of being cleaned up and moved into the `c++-concepts` branch.
