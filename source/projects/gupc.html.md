# GNU Unified Parallel C (GUPC)

![GUPC Logo](gupc.png "GUPC")

The GNU UPC project implements a compilation and execution environment for programs written in the [UPC (Unified Parallel C)](http://upc.gwu.edu/) language. The GNU UPC compiler extends the capabilities of the GNU GCC compiler. The GUPC compiler is implemented as a C Language dialect translator, in a fashion similar to the implementation of the GNU Objective C compiler.

## Project Goal {style="clear: left"}

To encourage the use and adoption of UPC, GUPC provides a free, generally available implementation of the UPC language dialect. By implementing UPC, GUPC provides a high-level tool for creating software targeted at parallel architectures. The UPC language makes it easier to express algorithms that run on parallel, High Performance Computing (HPC) systems.

The GUPC release includes a support library, *libupc*, and extensions to the "C" parser that recognizes the UPC language syntax.

## Features

-   [UPC 1.2 specification](http://www.gwu.edu/~upc/docs/upc_specs_1.2.pdf) compliant
-   [UPC collectives](http://upc.gwu.edu/docs/UPC_Coll_Spec_V1.0.pdf) library support
-   [GASP](http://gasp.hcs.ufl.edu) support; GASP is a performance tool interface for Global Address Space Languages
-   Fast bit packed pointer-to-shared support
-   Configurable UPC pointer-to-shared representation
-   Pthreads support (where each UPC thread is mapped to a pthread)
-   GUPC-provided libupc supports symmetric multiprocessor (SMP) systems
-   Libupc will associate each UPC thread with a particular CPU via Linux processor affinity operations and the NUMA library, when available.
-   For multi-node and large-scale systems support, the [Berkeley UPC runtime](http://upc.lbl.gov/download/source.shtml#runtime) can be built with GUPC as the compiler front-end.

## Supported Platforms

At this time, GUPC has been tested on the following platforms:

-   Intel x86\_64 Linux uniprocessor and symmetric multiprocessor systems (Fedora Core 11 and Ubuntu 9.04)
-   Intel ia64 (Itanium) Linux uniprocessor and symmetric multiprocessor systems (SUSE Linux Enterprise Server 11)
-   Intel x86 Linux uniprocessor and symmetric multiprocessor systems (CentOS 5.3)
-   Intel x86 Apple Mac OS X uniprocessor and symmetric multiprocessor systems (Leopard 10.5.7+, Snow Leopard 10.6, and Lion 10.7)
-   Mips2 32-bit (-n32) ABI and mips4 64-bit (-n64) ABI (SGI IRIX 6.5)
-   Cray XT3/4/5 CNL and Catamount
-   As a front-end to the Berkeley UPC Berkeley UPC runtime on various High-Performance Computing (HPC) systems

If you would like to learn of future ports to other platforms, or would like to discuss the feasibility of implementing GUPC on a platform of interest to you, we recommend that you join the [GUPC discussion list](#gupc_discuss).

## Download

The latest release of GUPC can be downloaded from [gccupc.org](http://www.gccupc.org/downloads/upc-downloads).

Alternatively, read-only SVN access to the GUPC branch can be used to acquire the latest development source tree:

    svn checkout svn://gcc.gnu.org/svn/gcc/branches/gupc

## Documentation

For a list of configuration switches that you can use to build GUPC, consult the GUPC [configuration page](http://gccupc.org/gcc-upc-info/gcc-upc-configuration).

For a quick summary of the switches used to compile and link a UPC program, consult the GUPC [manual page](http://gccupc.org/gcc-upc-info/gcc-upc-man-page).

## The GNU UPC Discussion List {#gupc_discuss}

The GNU UPC Discussion list provides a forum for tracking the status of UPC compilers based on the GNU GCC compiler. Announcements of new compiler ports, future plans, as well as known problems will be posted to the GUPC discussion list. To subscribe to the GNU UPC list, send an email message to [gcc-upc-request@hermes.gwu.edu](mailto:://gcc-upc-request@hermes.gwu.edu) with the single word "subscribe" (without the quotes) in the body of the message, or visit the mailing list [sign up](https://hermes.gwu.edu/cgi-bin/wa?SUBED1=gcc-upc&A=1) page.

## Contributing

We encourage everyone to [contribute changes](../contribute.html) and help test GUPC. GUPC is currently an SVN branch. We provide read access to our development sources for everybody with [anonymous SVN](../svn.html).

## Reporting Bugs

Bugs in GUPC should be reported via [GCC Bugzilla](http://gcc.gnu.org/bugzilla/). In all cases, please add "UPC" to the keywords field in the bug report.

## Status

**February 28, 2010**
:   Merged with the current GCC trunk, and checked into the **gupc** branch.


