# Documentation projects

This page lists projects for GCC's documentation. Some of these concern the internals documentation of GCC, and obviously require intimate knowledge of GCC's internals. The other projects are about work on the user documentation, and could be taken on by anyone who has mastered US English and has basic technical writing skills.

-   [Fully document the interface of front ends to GCC](#frontend_middleend_interface)
-   [Better documentation of how GCC works and how to port it](#internals_and_porting)
-   [Fully document the back-end intermediate language data structures](#RTL)
-   [Improve the indexing of the GCC manual](#improve_manual_index)
-   [Roll information in external documents into the official manual](#external_documents)
-   [Improve user and installation documentation.](#user_level_documentation)
-   [Revisit the list of "Actual Bugs" listed in the manual](#revisit_actual_bugs)

Always, anytime, feel free to shout at anyone who sends in a patch without including all relevant documentation changes.

It is also always appreciated if you read the whole manual and become familiar with what is documented where, and what documentation appears to be missing. Report or fix any problems you see.

* * * * *

## Fully document the interface of front ends to GCC

Fully document the interface of front ends to GCC, that is, the `tree`, `cgraph`, and `langhooks` interfaces, and the various functions, data structures, etc., that a front end must or may provide.

We've got quite a bit of this but it is scattered all over the place. It belongs in the official manual. There is a [C/C++ specific manual](http://gcc.gnu.org/onlinedocs/gccint/Trees.html), which is incomplete, though. The file `gcc/LANGUAGES` contains incomplete and outdated information about changes made in not so recent years to the `tree` interface. Several people have written partial manuals on implementing new front ends. Pointers to some of those can be found in our [readings list](../readings.html). With the advent of tree-ssa, most of these manuals are obsolete.

## Better documentation of how GCC works and how to port it

The porting manual describes what used to be the proper way to write a GCC back end. It is several years out of date. Find all the out-of-date advice for porters and replace it with correct advice. Mark old, deprecated features as such. Replace examples using old targets with examples for newer targets.

Here is an outline proposed by Allan Adler.

1.  Overview of this document
2.  The machines on which GCC is implemented
    1.  Prose description of those characteristics of target machines and their operating systems which are pertinent to the implementation of GCC.
        1.  target machine characteristics
        2.  comparison of this system of machine characteristics with other systems of machine specification currently in use

    2.  Tables of the characteristics of the target machines on which GCC is implemented.
    3.  A priori restrictions on the values of characteristics of target machines, with special reference to those parts of the source code which entail those restrictions
        1.  restrictions on individual characteristics
        2.  restrictions involving relations between various characteristics

    4.  The use of GCC as a cross-compiler
        1.  cross-compilation to existing machines
        2.  cross-compilation to non-existent machines

    5.  Assumptions which are made regarding the target machine
        1.  assumptions regarding the architecture of the target machine
        2.  assumptions regarding the operating system of the target machine
        3.  assumptions regarding software resident on the target machine
        4.  where in the source code these assumptions are in effect made.

3.  A systematic approach to writing the files `tm.h` and `xm.h`
    1.  Macros which require special care or skill
    2.  Examples, with special reference to the underlying reasoning

4.  A systematic approach to writing the machine description file
    1.  Minimal viable sets of insn descriptions
    2.  Examples, with special reference to the underlying reasoning

5.  Uses of the file aux-output.c
6.  Specification of what constitutes correct performance of an implementation of GCC
    1.  The components of GCC
    2.  The itinerary of a C program through GCC
    3.  A system of benchmark programs
    4.  What your RTL and assembler should look like with these benchmarks
    5.  Fine tuning for speed and size of compiled code

7.  A systematic procedure for debugging an implementation of GCC
    1.  Use of GDB
        1.  the macros in the file .gdbinit for GCC
        2.  obstacles to the use of GDB
            1.  functions implemented as macros can't be called in GDB

    2.  Debugging without GDB
        1.  How to turn off the normal operation of GCC and access specific parts of GCC

    3.  Debugging tools
    4.  Debugging the parser
        1.  how machine macros and insn definitions affect the parser

    5.  Debugging the recognizer
        1.  how machine macros and insn definitions affect the recognizer

    6.  7.  ... ditto for other components...
    8.  

8.  Data types used by GCC, with special reference to restrictions not specified in the formal definition of the data type
9.  References to the literature for the algorithms used in GCC

## Fully document the back-end intermediate language data structures

Document every RTX code and accessor macro, every insn name, every `tm.h` macro and every target hook thoroughly. (See [this list of undocumented tm.h macros](http://gcc.gnu.org/ml/gcc/2001-06/msg00507.html)).

These may involve hunting down whoever added whichever thing it is and torturing information out of them.

Work out the correct argument and return types for each tm.h macro, and make the manual describe them with `@deftypefn` and similar using C prototypes. For those macros for which performance is not important, change them to be functions, in the `targetm` structure for target hooks.

## Improve the indexing of the GCC manual.

All command-line options should be indexed, and there should be index entries for the text of all error messages that might be confusing, if there is a relevant part of the manual. See a [message to gcc-bugs](http://gcc.gnu.org/ml/gcc-bugs/2001-02/msg00384.html) about this.

## Roll information in external documents the official manual.

Start with the [readings list](../readings.html) and the secondary Texinfo documents in the source tree, such as `libstdc++-v3/docs/html/17_intro/porting.texi`. Pick your favorite FAQ from the lists and roll it into the manual.

## Improve user and installation documentation.

-   Add information on relevant standards. Document the exact semantics of all the extensions. Also say what they're good for. If they're useless, admit it.
-   Improve support for building other manual formats. For example, arrange for `make dvi` at top level to build DVI versions of all manuals. Add a `make html` target to build HTML versions of manuals (using `makeinfo --html`).
-   Consider adding targets to build PostScript and PDF versions of all manuals (`texinfo.tex` includes some support for PDF output).
-   Make sure all Texinfo manuals are included and installed.

## Revisit the list of "Actual Bugs" in the manual

Go through the list of "Actual Bugs" in `gcc/doc/trouble.texi`. Work out what they refer to, if necessary by asking people who were involved in GCC development when those bugs were documented. If a bug is no longer present, remove it from the list; if it is still present, file a bug report in the [GCC bug tracking system](http://gcc.gnu.org/bugzilla/) or fix the bug yourself.
