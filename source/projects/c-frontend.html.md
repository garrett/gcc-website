# Projects for the C front end

The following are some ideas for C front-end projects to work on.

## More warnings

Warn about statements that are undefined because the order of evaluation of increment operators makes a big difference. Here is an example:

    *foo++ = hack (*foo);

`-Wsequence-point` does some of this, but not that particular case.

## Changes to support C99 standard

The new version of the C standard (ISO/IEC 9899:1999) requires a number of library changes; these have to be provided by the C library, and not by gcc. In addition, there are also changes to the language proper, and some compiler support is needed for the new library features. An [overview](../c99status.html) of the C99 implementation status is available.

## Implement various builtin functions for ISO C99's `<tgmath.h>`

Design and implement builtin functions that assist in implementing the ISO C99 `<tgmath.h>` macros. These builtins could be along the general lines of the macros used in glibc 2.2 (for example, the implementation of `<tgmath.h>` might use `__builtin_tgmath_unary_real_only (value, log2f, log2, log2l)` to implement the `log2` macro), but should be designed so that, if the Annex G imaginary types are implemented in GCC, glibc does not need to change again to allow for them. This means additional functions, beyond those corresponding to the macros in glibc, are needed to handle the trigonometric functions which G.7 specifies have real or imaginary result type for imaginary arguments, but which have complex result type for complex arguments. The design for these builtins should be discussed with the gcc and libc-alpha lists.

## Format (`printf`, `scanf` and `strftime`) checking

-   Make re-entrant: eliminate global state used for `$` format checking.
-   `strftime`: warnings for use of `0`, `-` or `_` flags without width on formats where inappropriate. Similar warnings where use of one format feature is useless unless another one is also used also apply for `printf` formats (`0` and `-` flags when no width is specified and so there is no padding) and for `strfmon` formats.
-   Integer `printf` formats: warn for integer constant argument out of range of unpromoted type (including signed/unsigned where it doesn't fit in range).
-   Fix detection of whether format strings are narrow or wide on targets where `wchar_t` is `char`. This may require changes elsewhere in the compiler to mark string constants with this information.
-   Include an option to warn for format extensions that have been obsoleted by standard C99 features.
-   Meaning of "quad" and `%q`: anything more accurate than `long long`? See messages [1](http://gcc.gnu.org/ml/gcc-bugs/1999-02n/msg00164.html) and [2](http://gcc.gnu.org/ml/gcc-bugs/2000-08/msg00272.html) about this to gcc-bugs. (Note that `%q` is now largely obsolete given the new C99 formats and macros.)
-   Wide string format support. See a [message soliciting views on the user interface to this](http://gcc.gnu.org/ml/gcc/2000-12/msg00170.html) and send any views on this to the `gcc` mailing list.
-   Clean up how `format.exp` handles `TORTURE_OPTIONS`, by making changes elsewhere in the test harness so that `TORTURE_OPTIONS` is initialized for each `dg-init` rather than just once and so needing saving and restoring.
-   When a `format` or `format_arg` attribute is applied to a function with a non-prototype declaration, and the function is later redeclared with a prototype or defined, the attribute should be checked again to see if it makes sense given the argument types of the function. Similarly, these checks should be made if the original attribute is on an old-style function definition.
-   Implement more checks related to `__attribute__((__nonnull__))`, such as for functions such as `execle` that require `NULL`-terminated argument lists.
-   Possible security warnings under `-Wformat-security`:
    -   Warn for `sprintf` into fixed length buffer if the output can't be proved not to overrun. Similarly for `scanf` `%s` and `%[...]` without width to fixed length buffer (or possibly to any buffer); or `%s`, `%[...]` and `%c` with width to too short a buffer, including `%lc`, `%ls` and `%l[...]`.
-   (Maybe eventually:) Extensible format checking: allow programs such as OS kernels and parts of GCC to tell GCC about additional format conversion characters recognised by their format functions.
-   `fopen`, `freopen`: warning for the incorrect format of the mode parameter, see [PR c/5127](http://gcc.gnu.org/ml/gcc-bugs/2001-12/msg00649.html). Some ideas for how to implement such a warning were discussed [here](http://gcc.gnu.org/ml/gcc-bugs/2001-12/msg00651.html).

