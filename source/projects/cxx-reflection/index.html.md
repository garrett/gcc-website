# Compile Time Reflection for C++

This works is part of the effort of providing support for compile time reflection in C++. A basic example of such feature is the type traits proposal by John Maddock. More generally, this project aims at providing support for compile-time features that enhance and facilitate development of C++ libraries and application programs.

# Template Meta-Programming

This sub-project aims at implementing and providing experience with features that improve support for template meta-programming and constrained genericity in C++.

### Contributing

Checkout the `cxx-reflection-branch` branch in [our respository](../../svn.html).

When posting to the development lists, please mark messages and patches with `[cxx-reflection]` in the subject. As this is a branch, and not the mainline, the usual maintainer rules do not apply. This branch is maintained by [Gabriel Dos Reis](mailto:gdr@acm.org). Approval from the usual maintainers will be needed when submitting patches from the branch onto the mainline.

### Branch stability

While this is an experimental branch and people are encouraged to add new features to it, default bootstraps and c-torture, C++ and Java tests *must always work* before a patch is allowed in the branch. If you want to contribute a new feature that is still not complete, you should:

1.  Add a new -f flag to enable your code.
2.  Document the new -f flag in doc/invoke.texi.
3.  Configure and bootstrap the compiler with all the default front ends enabled.
4.  Run `make check` with no new regressions.

Patches that break default bootstraps will be removed (if a fix is not immediately obvious).

When submitting patches that implement new fonctionalities, please include a reference to the paper and/or book where you are getting the complete syntactic and semantic specifications from. If it's your own research work, include a Technical Report, Thesis or Paper reference for it.

There are regular mainline merges about 3 or 4 times a month. The latest merge tag is always added to GCC's version string. A merge may be postponed if there is major breakage in mainline.

Work from the branch will be incorporated into mainline only if

1.  it can be proved that it provides better way to implement an existing standard feature; or
2.  it aims at implementing features under standardization; or
3.  its specifications are clearly written in Standardeze and its usefulness is acknowledged and accepted by the C++ front-end maintainers.

