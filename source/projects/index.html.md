# GCC Projects

This is an incomplete list of things you could work on if you want to help develop GCC:

-   If you are new to GCC, start with our list of [projects for beginner GCC hackers.](beginner.html)
-   Investigate bugs and attempt to fix bugs in our [bug tracking system](http://gcc.gnu.org/bugzilla/), see whether they still are present in current GCC.
-   [Projects for the GCC web pages.](../about.html#TODO)
-   [Projects for improving the GCC documentation](documentation.html).
-   There are several [development branches](../svn.html#devbranches) with projects that you may be able to help out with.
-   Investigate and fix some of the known [optimizer inadequacies](optimize.html).
-   [Projects for the C preprocessor.](cpplib.html)
-   [Projects for improving the C front end.](c-frontend.html)
-   [Implementing new C++14 features.](cxx1y.html)
-   [The GNU UPC Project.](gupc.html)
-   [Improve the installation procedure](#improve_the_installation_procedure)
-   [Simpler porting](#simpler_porting)
-   [Generalize the machine model](#generalize_the_machine_model)
-   [The old PROBLEMS file](#the_old_problems_file)

Remember to [keep other developers informed](../contributewhy.html) of any substantial projects you intend to work on.

* * * * *

## Improve the installation procedure

-   See [a message from Zack Weinberg](http://gcc.gnu.org/ml/gcc/2000-11/msg00556.html), and follow-ups to it, for some discussion of some of these ideas.
-   Move the installation from incomprehensible shell scripts in the Makefiles to more comprehensible shell scripts outside the Makefiles.
-   When installing as root, make the installed headers in `$(libsubdir)` be owned by root rather than the user who did the build.
-   Be consistent about what programs get links such as `i686-pc-linux-gnu-gcc` and `i686-pc-linux-gnu-gcc-3.4.1`. Any program embedding architecture or version dependencies may need such links.
-   Add a configure option `--enable-cc-links` or similar which causes links to the compiler driver or shell scripts to be installed under names `cc` (the traditional Unix compiler name and a legacy utility in Unix98, accepting an unspecified C language variant), `c89` (POSIX; a script running `gcc   -std=c89 -pedantic`) and `c99` (the Austin Group revision of POSIX; a script running `gcc -std=c99   -pedantic`) for systems such as GNU/Linux where GCC is the system compiler.
-   Fix the issues discussed in [PR other/346](http://gcc.gnu.org/PR346).

## Simpler porting

Right now, describing the target machine's instructions is done cleanly, but describing its addressing mode is done with several ad-hoc macro definitions. Porting would be much easier if there were an RTL description for addressing modes like that for instructions. Tools analogous to genflags and genrecog would generate macros from this description.

There would be one pattern in the address-description file for each kind of addressing, and this pattern would have:

-   the RTL expression for the address
-   C code to verify its validity (since that may depend on the exact data).
-   C code to print the address in assembly language.
-   C code to convert the address into a valid one, if it is not valid. (This would replace LEGITIMIZE\_ADDRESS).
-   Register constraints for all indeterminates that appear in the RTL expression.

## Generalize the machine model

Some new compiler features may be needed to do a good job on machines where static data needs to be addressed using base registers.

Some machines have two stacks in different areas of memory, one used for scalars and another for large objects. The compiler does not now have a way to understand this.

## The old PROBLEMS file

The following used to be in a file `PROBLEMS` in the GCC distribution. Probably much of it is no longer relevant as of GCC 3.0 (the file hadn't changed since GCC 2.0), but some might be. Someone should go through it, identifying what is and isn't relevant, adding anything applicable to current GCC (and describing a bug) to our bug-tracking system and/or updating this patch to remove such analysed entries from the list.

1.  Possible special combination pattern: If the two operands to a comparison die there and both come from insns that are identical except for replacing one operand with the other, throw away those insns. Ok if insns being discarded are known 1 to 1. An andl \#1 after a seq is 1 to 1, but how should compiler know that?
2.  Any number of slow zero-extensions in one loop, that have their clr insns moved out of the loop, can share one register if their original life spans are disjoint. But it may be hard to be sure of this since the life span data that regscan produces may be hard to interpret validly or may be incorrect after cse.
3.  In cse, when a bfext insn refers to a register, if the field corresponds to a halfword or a byte and the register is equivalent to a memory location, it would be possible to detect this and replace it with a simple memory reference.

