# MIPS O64 Application Binary Interface for GCC

The O64 ABI is almost identical to the classic 32-bit MIPS ABI. The differences are enumerated below.

## 64-Bit Integer Registers

The integer registers are assumed to have 64 bits, rather than 32 bits. For example, two 64-bit integers may be added with a single add instruction.

## Argument Passing

Arguments are passed on 64-bit boundaries, rather than 32-bit boundaries, when they are passed on the stack. For example, given:

>         void f() { g(1, 2, 3, 4, 5, 6, 7, 8, 9, 10); }

all arguments beginning with `5` are passed on the stack. The argument `6` is placed 8 bytes beyond argument `5` -- not 4 bytes as it would be in 32-bit mode. In little-endian mode, arguments are packed towards the lower-address side of the 8-byte slot; in big-endian mode, they are packed towards the upper-address side.

## Floating-Point Arguments

If the first and second arguments floating-point arguments to a function are 32-bit values, they are passed in `$f12` and `$f14`. If the first is a 32-bit value and the second is a 64-bit value, they are passed in `$f12` and `$f13`. If they are both 64-bit values, they are passed in `$f12` and `$f13`.

## ELF Header

The `e_flags` field in the ELF header for object files generated using this ABI contains `E_MIPS_ABI_O64`, which has the value `0x2000`.
