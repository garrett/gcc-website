# C++0x/C++11 Support in GCC

C++0x was the working name of the ISO C++ 2011 standard, which introduced a host of new features into the standard C++ language and library. This project sought to implement new C++11 features in GCC, and made it the first compiler to bring feature-complete C++11 to C++ programmers.

C++11 features are available as part of the "mainline" GCC compiler in the trunk of [GCC's Subversion repository](../svn.html) and in GCC 4.3 and later. To enable C++0x support, add the command-line parameter `-std=c++0x` to your `g++` command line. Or, to enable GNU extensions in addition to C++0x extensions, add `-std=gnu++0x` to your `g++` command line. GCC 4.7 and later support `-std=c++11` and `-std=gnu++11` as well.

**Important**: GCC's support for C++11 is still **experimental**. Some features were implemented based on early proposals, and no attempt will be made to maintain backward compatibility when they are updated to match the final C++11 standard.

## C++11 Language Features

The following table lists new language features that have been accepted into the C++11 standard. The "Proposal" column provides a link to the ISO C++ committee proposal that describes the feature, while the "Available in GCC?" column indicates the first version of GCC that contains an implementation of this feature (if it has been implemented).

For information about C++11 support in a specific version of GCC, please see:

-   [GCC 4.3 C++0x Status](../gcc-4.3/cxx0x_status.html)
-   [GCC 4.4 C++0x Status](../gcc-4.4/cxx0x_status.html)
-   [GCC 4.5 C++0x Status](../gcc-4.5/cxx0x_status.html)
-   [GCC 4.6 C++0x Status](../gcc-4.6/cxx0x_status.html)
-   [GCC 4.7 C++11 Status](../gcc-4.7/cxx0x_status.html)
-   [GCC 4.8 C++11 Status](../gcc-4.8/cxx0x_status.html)

Language Feature

Proposal

Available in GCC?

Rvalue references

[N2118](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n2118.html)

[GCC 4.3](../gcc-4.3/changes.html)

    Rvalue references for `*this`

[N2439](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2439.htm)

[GCC 4.8.1](../gcc-4.8/changes.html)

Initialization of class objects by rvalues

[N1610](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2004/n1610.html)

Yes

Non-static data member initializers

[N2756](http://www.open-std.org/JTC1/SC22/WG21/docs/papers/2008/n2756.htm)

[GCC 4.7](../gcc-4.7/changes.html)

Variadic templates

[N2242](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2242.pdf)

[GCC 4.3](../gcc-4.3/changes.html)

    Extending variadic template template parameters

[N2555](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2555.pdf)

[GCC 4.4](../gcc-4.4/changes.html)

Initializer lists

[N2672](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2672.htm)

[GCC 4.4](../gcc-4.4/changes.html)

Static assertions

[N1720](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2004/n1720.html)

[GCC 4.3](../gcc-4.3/changes.html)

`auto`-typed variables

[N1984](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n1984.pdf)

[GCC 4.4](../gcc-4.4/changes.html)

    Multi-declarator `auto`

[N1737](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2004/n1737.pdf)

[GCC 4.4](../gcc-4.4/changes.html)

    Removal of auto as a storage-class specifier

[N2546](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2546.htm)

[GCC 4.4](../gcc-4.4/changes.html)

    New function declarator syntax

[N2541](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2541.htm)

[GCC 4.4](../gcc-4.4/changes.html)

New wording for C++0x lambdas

[N2927](http://www.open-std.org/JTC1/SC22/WG21/docs/papers/2009/n2927.pdf)

[GCC 4.5](../gcc-4.5/changes.html)

Declared type of an expression

[N2343](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2343.pdf)

[GCC 4.3](../gcc-4.3/changes.html)

    decltype and call expressions

[N3276](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2011/n3276.pdf)

[GCC 4.8.1](../gcc-4.8/changes.html)

Right angle brackets

[N1757](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2005/n1757.html)

[GCC 4.3](../gcc-4.3/changes.html)

Default template arguments for function templates

[DR226](http://www.open-std.org/jtc1/sc22/wg21/docs/cwg_defects.html#226)

[GCC 4.3](../gcc-4.3/changes.html)

Solving the SFINAE problem for expressions

[DR339](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2634.html)

[GCC 4.4](../gcc-4.4/changes.html)

Template aliases

[N2258](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2258.pdf)

[GCC 4.7](../gcc-4.7/changes.html)

Extern templates

[N1987](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n1987.htm)

Yes

Null pointer constant

[N2431](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2431.pdf)

[GCC 4.6](../gcc-4.6/changes.html)

Strongly-typed enums

[N2347](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2347.pdf)

[GCC 4.4](../gcc-4.4/changes.html)

Forward declarations for enums

[N2764](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2764.pdf)

[GCC 4.6](../gcc-4.6/changes.html)

Generalized attributes

[N2761](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2761.pdf)

[GCC 4.8](../gcc-4.8/changes.html)

Generalized constant expressions

[N2235](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2235.pdf)

[GCC 4.6](../gcc-4.6/changes.html)

Alignment support

[N2341](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2341.pdf)

[GCC 4.8](../gcc-4.8/changes.html)

Delegating constructors

[N1986](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n1986.pdf)

[GCC 4.7](../gcc-4.7/changes.html)

Inheriting constructors

[N2540](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2540.htm)

[GCC 4.8](../gcc-4.8/changes.html)

Explicit conversion operators

[N2437](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2437.pdf)

[GCC 4.5](../gcc-4.5/changes.html)

New character types

[N2249](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2249.html)

[GCC 4.4](../gcc-4.4/changes.html)

Unicode string literals

[N2442](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2442.htm)

[GCC 4.5](../gcc-4.5/changes.html)

Raw string literals

[N2442](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2442.htm)

[GCC 4.5](../gcc-4.5/changes.html)

Universal character name literals

[N2170](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2170.html)

[GCC 4.5](../gcc-4.5/changes.html)

User-defined literals

[N2765](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2765.pdf)

[GCC 4.7](../gcc-4.7/changes.html)

Standard Layout Types

[N2342](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2342.htm)

[GCC 4.5](../gcc-4.5/changes.html)

Defaulted and deleted functions

[N2346](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2346.htm)

[GCC 4.4](../gcc-4.4/changes.html)

Extended friend declarations

[N1791](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2005/n1791.pdf)

[GCC 4.7](../gcc-4.7/changes.html)

Extending `sizeof`

[N2253](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2253.html)

[GCC 4.4](../gcc-4.4/changes.html)

Inline namespaces

[N2535](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2535.htm)

[GCC 4.4](../gcc-4.4/changes.html)

Unrestricted unions

[N2544](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2544.pdf)

[GCC 4.6](../gcc-4.6/changes.html)

Local and unnamed types as template arguments

[N2657](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2657.htm)

[GCC 4.5](../gcc-4.5/changes.html)

Range-based for

[N2930](http://www.open-std.org/JTC1/SC22/WG21/docs/papers/2009/n2930.html)

[GCC 4.6](../gcc-4.6/changes.html)

Explicit virtual overrides

[N2928](http://www.open-std.org/JTC1/SC22/WG21/docs/papers/2009/n2928.htm)
[N3206](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2010/n3206.htm)
[N3272](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2011/n3272.htm)

[GCC 4.7](../gcc-4.7/changes.html)

Minimal support for garbage collection and reachability-based leak detection

[N2670](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2670.htm)

No

Allowing move constructors to throw [noexcept]

[N3050](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2010/n3050.html)

[GCC 4.6](../gcc-4.6/changes.html)

Defining move special member functions

[N3053](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2010/n3053.html)

[GCC 4.6](../gcc-4.6/changes.html)

Concurrency

Sequence points

[N2239](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2239.html)

Yes

Atomic operations

[N2427](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2427.html)

[GCC 4.4](../gcc-4.4/changes.html)

Strong Compare and Exchange

[N2748](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2748.html)

[GCC 4.5](../gcc-4.5/changes.html)

Bidirectional Fences

[N2752](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2752.htm)

[GCC 4.8](../gcc-4.8/changes.html)

Memory model

[N2429](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2429.htm)

[GCC 4.8](../gcc-4.8/changes.html)

Data-dependency ordering: atomics and memory model

[N2664](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2664.htm)

[GCC 4.4](../gcc-4.4/changes.html)
(memory\_order\_consume)

Propagating exceptions

[N2179](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2179.html)

[GCC 4.4](../gcc-4.4/changes.html)

Abandoning a process and at\_quick\_exit

[N2440](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2440.htm)

[GCC 4.8](../gcc-4.8/changes.html)

Allow atomics use in signal handlers

[N2547](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2547.htm)

Yes

Thread-local storage

[N2659](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2659.htm)

[GCC 4.8](../gcc-4.8/changes.html)

Dynamic initialization and destruction with concurrency

[N2660](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2660.htm)

[GCC 4.3](../gcc-4.3/changes.html)

C99 Features in C++11

`__func__` predefined identifier

[N2340](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2340.htm)

[GCC 4.3](../gcc-4.3/changes.html)

C99 preprocessor

[N1653](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2004/n1653.htm)

[GCC 4.3](../gcc-4.3/changes.html)

`long long`

[N1811](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2005/n1811.pdf)

[GCC 4.3](../gcc-4.3/changes.html)

Extended integral types

[N1988](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n1988.pdf)

Yes

## C++11 Library Features

The status of the library implementation can be tracked in this [table](http://gcc.gnu.org/onlinedocs/libstdc++/manual/status.html#status.iso.200x)

## Historical Branches

### C++0x Concepts Branch

Support for the [Concepts](http://www.generic-programming.org/languages/conceptcpp/) feature, which is not part of C++11, is not under active development at this time. The prototype implementation, [ConceptGCC](http://www.generic-programming.org/software/ConceptGCC/), can be found on the `branches/conceptgcc-branch`. Previously there was a plan to develop a superior implementation on a separate development branch, `branches/cxx0x-concepts-branch`, but not much work was done on that branch.
