# Welcome to the home of GOMP

The GOMP project has developed an implementation of [OpenMP](http://openmp.org/wp/) for the C, C++, and [Fortran](../../fortran/) compilers in the [GNU Compiler Collection](../../) and is further improving it. As part of the [GNU Project](../../), GOMP simplifies parallel programming for all GNU system variants. This effort operates in an open environment to attract developers and ensure applicability across multiple architectures and applications.

Traditionally, programmers have used architecture-specific methods to effectively program tightly-parallelized computers — high band-width clusters, SMP machines, or multi-core processors. Parallel programming has thus been a time-consuming and arcane task.

OpenMP offers a simple way of exploiting parallelism without interfering with algorithm design; an OpenMP program compiles and operates correctly in both parallel and serial execution environments. Using OpenMP's directive-based parallelism also simplifies the act of converting existing serial code to efficient parallel code.

## Project goal

To remain relevant, free software development tools must support emerging technologies. By implementing OpenMP, GOMP provides a simplified syntax tools for creating software targeted at parallel architectures. OpenMP's platform-neutral syntax meshes well with the portability goals of GCC and other GNU projects.

The GOMP release includes a support library, libgomp, and extensions to target language parsers. A long-term goal is the generation of efficient and small code for OpenMP applications.

## Contributing

We encourage everyone to [contribute changes](../../contribute.html) and help test GOMP. GOMP has been merged into the SVN trunk. We provide read access to our development sources for everybody with [anonymous SVN](../../svn.html).

## Reporting Bugs

Bugs in GOMP should be reported via [bugzilla](http://gcc.gnu.org/bugzilla/). In all cases, please add "openmp" to the keywords field in the bug report.

## Documentation

libgomp, the GOMP support library, has [online documentation](http://gcc.gnu.org/onlinedocs/libgomp/) available.

## Status

**Jun 18, 2014**
:   The last major part of Fortran OpenMP v4.0 support has been committed into SVN mainline.

**Oct 11, 2013**
:   The `gomp-4_0-branch` has been merged into SVN mainline, so GCC 4.9 and later will feature OpenMP v4.0 support for C and C++.

**July 23, 2013**
:   The final [OpenMP v4.0](http://www.openmp.org/mp-documents/OpenMP4.0.0.pdf) specification has been released.

**Aug 2, 2011**
:   The `gomp-3_1-branch` has been merged into SVN mainline, so GCC 4.7 and later will feature OpenMP v3.1 support.

**July 9, 2011**
:   The final [OpenMP v3.1](http://www.openmp.org/mp-documents/OpenMP3.1.pdf) specification has been released.

**February 6, 2011**
:   A draft of the OpenMP v3.1 specification has been released for public review. The `gomp-3_1-branch` branch has been created in SVN and work began on implementing v3.1 support.

**June 6, 2008**
:   The `gomp-3_0-branch` has been merged into SVN mainline, so GCC 4.4 and later will feature OpenMP v3.0 support.

**May 12, 2008**
:   The final [OpenMP v3.0](http://openmp.org/mp-documents/spec30.pdf) specification has been released.

**October 22, 2007**
:   Draft of the OpenMP v3.0 specification has been released for public review, the `gomp-3_0-branch` branch has been created in SVN and work began on implementing v3.0 support.

**March 9, 2006**
:   The branch has been merged into mainline, so starting with GCC 4.2 the compiler supports the OpenMP v2.5 specification.

**November 18, 2005**
:   The branch is ready to be merged into mainline. All three front ends are functional and there should not be many corners of the standard left to implement. There are 5 main modules to merge into mainline: (1) runtime library, (2) code generation, (3) C front end, (4) C++ front end, and, (5) Fortran front end.

**October 20, 2005**
:   The runtime library is functionally complete. The syntax parsers for C, C++ and Fortran are complete, though there are still dusty corners with respect to semantic translation to be resolved. Adventurous users who don't mind the compiler crashing on every other source file are encouraged to begin filing bugs.


