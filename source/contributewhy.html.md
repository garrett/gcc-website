# Extending and Contributing to GCC

If you are writing a major extension to GCC, such as a port to a new computer, a new language, or a major rewrite of some optimization passes, please keep in mind the importance of keeping other developers informed. Part of being a good cooperating member of the GCC development team is the responsibility to consider what the other developers need in order to work effectively.

Nobody likes to do a lot of work and find it was duplicated effort. So when you work on a major new feature, you should tell <gcc@gcc.gnu.org> what you are working on, and give occasional reports of how far you have come and how confident you are that you will finish the job. This way, other developers (if they are paying attention) will be aware which projects would duplicate your effort, and can either join up with you, or at least avoid spending time on something that will be unnecessary because of your work.

You should also monitor the [gcc@gcc.gnu.org list](lists.html) to see if someone else mentions working on a similar project to yours. If that happens, speak up!

If you are thinking of taking a contract to develop changes under a temporary delayed-release agreement, please negotiate the agreement so that you can give progress reports before the release date, even though you cannot release the code itself. Also please arrange so that, when the agreed-on date comes, you can release whatever part of the job you succeeded in doing, even if you have not succeeded in finishing it. Someone else may be able to finish the job.

Many people have done GCC ports on their own, to a wide variety of processors, without much communication with the GCC development team. However, many of those ports have been lost over time, or have proven very hard to integrate. So, what we're asking is that, to the maximum extent possible, you communicate with us as early on and as much as possible.

Here are some questions GCC porters may have with our answers to them. While the focus here is on new back ends, we believe that the issues are similar for new front ends (new languages), as well as for student efforts to implement new compiler optimizations.

*Our engineers understand our processor better than anyone else, and we have a tight schedule. Why should we work with the GCC developers, when we can get the code out faster by whacking it out on our own?*

You understand your processor better than anyone else. However, the GCC developers understand GCC better than anyone else; furthermore, the GCC developers tend to have a wide breadth of experience across a large number of processors. It has been our experience that few problems encountered in compiler development are unique to a particular processor. The vast majority of the time an issue that arises in one processor has also shown up in other processors.

The intimate knowledge of GCC internals as well as a wide breadth of processor knowledge means that there is a good chance that at least one GCC developer has already addressed issues you are likely to face when doing the port to your processor. The developers can help guide you towards a workable long term solution, possibly saving you significant time in your development cycle.

If getting the sources into the official GCC distributions is one of your goals, then engaging other GCC developers early will also likely shorten your development time. By interacting as early as possible you are more likely to write code which can be easily accepted into the official sources when you are finished. If you wait until you think you are done to begin interaction with the GCC team, you might find that you did some things wrong and you may have to rewrite parts of your GCC port, which is a waste of your valuable time.

*Why should we care if our port is integrated into the official GCC sources? We can distribute it ourselves to whoever is interested.*

Yes, the GNU GPL allows you to do that. But by doing so, you end up having to maintain that code yourself; this can be a significant effort over time as the GCC sources change rapidly.

You also lose the advantage of wider exposure by including your port in the official GCC sources maintained by the GNU Project. The wider exposure in the GCC developer and tester community will help keep your port up to date with the current sources. You may even find that volunteers will run the ever-growing test suite on your port and fix problems during the development cycle -- sometimes without your intervention.

It has been our experience that integrated ports tend to ultimately be of better quality and stay up to date from release to release.

*Why should we communicate up front? We're happy to let the GCC developers integrate our stuff later.*

See above. It will save work for you over both the short and the long term, and it is the right thing to do.

*Aspects of my processor that my port exploits are still under NDA.*

Nevertheless, if the processor is out, or you have customers planning to use it, some of them may already be trying to port GCC on their own. Your customers might be happier to know that your port will eventually be available. Also, there is no requirement that GCC use all the features of your chip from day one.

Assume that your processor has some new functionality that will make a class of applications run much faster than with other processors. However that functionality is still covered by NDA, but the basic core architecture is not. It is still to your advantage to go ahead and work with the developers early to provide a "base port" for the chip. That base port would only use the publicly available specifications until such time as the NDA is lifted. Once the NDA is lifted you can work with the developers to provide the code necessary to take advantage of the new functionality.

Ultimately, cooperating with the free software community as early as possible helps you by decreasing your development cycle, decreasing your long term maintenance costs and may help raise interest in your processor by having a free compiler implementation available to anyone who wants to take a look.

Finally, please note that GPL-covered code may *not* be distributed under an NDA, [as explained by Richard Stallman](http://gcc.gnu.org/ml/gcc/2001-07/msg01342.html).
