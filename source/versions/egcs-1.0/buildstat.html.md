# Build and Test status for EGCS 1.0

Regression testing is relative to gcc-2.7.\*; only regressions relative to gcc-2.7 are noted.

It looks like many ports are regressing on g++.pt/typename5.C. It is not included in the regression test status below.

**alpha-dec-osf2.0**
 3-staged native. No gcc-2.7 results to compare with, but EGCS test results look good.

**alpha-dec-osf3.2**
 3-staged native. No regressions.

**alpha-dec-osf4.0**
 3-staged native. Testing in progress.

**alphaev56-dec-osf4.0b**
 3 staged native. Note EGCS 1.0 treats ev56 just like ev5.

**alphaev5-unknown-linux-gnulibc1**
 3-staged native using binutils-2.8.1. No regressions.

**hppa1.1-hp-hpux9**
 3-staged native using HP assembler. Major regressions in g++ testsuite due to problems with EH on this platform. We recommend using gas on this platform.

3-staged native using gas. No regressions.

3-staged native using gas and --enable-shared. Many EH tests fail, apparently EH & -fPIC are not playing well together on this configuration.

**hppa1.1-hp-hpux10.20**
 3-staged native using gas. No regressions.

3-staged native using gas and --enable-shared. Many EH tests fail, apparently EH & -fPIC are not playing well together on this configuration.

**i686-pc-linux-gnulibc1 Debian 1.3**
 3-staged native using binutils-2.8.0.1.15. No regressions.

**i586-pc-linux-gnulibc1 SuSE 5.0**
 3 staged native using binutils-2.8.1.0.15 No regressions. Minor EH failures due to slightly dated binutils.

3-staged native using binutils-2.8.1. No regressions. Also has minor EH failures, probably due to old binutils too.

**i386-pc-sysv4.2uw2.1.2**
 3 staged native with BOOT\_CFLAGS='-march=pentiumpro -O3 -malign-jumps=4 -malign-loops=4 -malign-functions=4' Look like there's some problems with libstdc++ tests.

**i386-linux-gnulibc1**

**i586-pc-linux-gnu**

**i586-pc-linux-gnulibc1 RH4.2**

**i?86-pc-solaris\***
 You'll need a patch to fix an EGCS bug on this platform (fixed as of EGCS 1.0.1).

**i386-pc-bsdi3.0**

**i386-freebsd**

**i586-pc-sco3.2v5.0.2**
 3 staged native.

**i586-pc-sco3.2v5.0.4**
 3-staged native using native assembler. Tests look good.

**i686-pc-linux-gnulibc1 Slackware 3.1**
 3 staged native tests look good.

**i586-pc-linux-gnulibc1 RH4.0**

**i486-l586-pc-linux-gnulibc1 SuSE 4.4.1\#1**

**mips-sgi-irix4.0.4**
 3 staged native EH tests fail badly for irix4.

**mips-sgi-irix5.2**

**mips-sgi-irix5.3**
 3-staged native g++.jason/dtor5.C regressions.

**mips-sgi-irix6.2**
 3-staged native. g++.jason/rfg10.C regression.

**mips-sgi-irix6.3**
 3-staged native. g++.jason/rfg10.C regression.

**m68k-unknown-linux-gnulibc1**
 3-staged native.

**m68k-hp-netbsd1.2**

**m68k-next-nextstep3**
 3-staged native. Much better than gcc-2.7.2.3 for C code.

**m68k-sun-sunos4.1.1**

**rs6000-ibm-aix4.3.2**

**powerpc-ibm-aix4.1.5**
 3-staged native. 931004-11 and 931004-12 regress.

**powerpc-ibm-aix4.2.1.0**
 3 staged native. 931004-11 , 931004-12 a few g++ tests too. Note we have conflicting g++ test results on this platform.

**powerpc-ibm-aix4.2**

**powerpc-unknown-linux-gnulibc1**
 3-staged native --enable shared (PowerMac).

**sparc-sun-solaris2.6**
 3-staged native ultrasparc 2.

**sparc-sun-solaris2.5**
 3 staged native --enable-haifa no regressions.

**sparc-sun-solaris2.5.1**
 3-staged native.

3-staged native --enable-haifa no regressions.

3-staged native --enable-shared --with-gnu-gas --with-gnu-ld.

**sparc-sun-sunos4.1.3**

