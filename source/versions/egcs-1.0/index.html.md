# EGCS 1.0

December 3, 1997: We are pleased to announce the release of EGCS 1.0.
 January 6, 1998: We are pleased to announce the release of EGCS 1.0.1.
 March 16, 1998: We are pleased to announce the release of EGCS 1.0.2.
 May 15, 1998 We are pleased to announce the release of EGCS 1.0.3.

EGCS is a collaborative effort involving several groups of hackers using an open development model to accelerate development and testing of GNU compilers and runtime libraries.

An important goal of EGCS is to allow wide scale testing of experimental features and optimizations; therefore, EGCS contains some features and optimizations which are still under development. However, EGCS has been carefully tested and should be comparable in quality to most GCC releases.

EGCS 1.0 is based on an August 2, 1997 snapshot of the GCC 2.8 development sources; it contains nearly all of the new features found in GCC 2.8.

EGCS 1.0 also contains many improvements and features not found in GCC 2.7 and even the GCC 2.8 series (which was released after the original EGCS 1.0 release).

-   Integrated C++ runtime libraries, including support for most major GNU/Linux systems!
-   The integrated libstdc++ library includes a verbatim copy of SGI's STL release.
-   Integrated GNU Fortran compiler.
-   New instruction scheduler.
-   New alias analysis code.

See the [new features](features.html) page for a more complete list of new features.

EGCS 1.0.1 is a minor update to the EGCS 1.0 compiler to fix a few critical bugs and add support for Red Hat 5.0 Linux. Changes since the EGCS 1.0 release:

-   Add support for Red Hat 5.0 Linux and better support for Linux systems using glibc2.

    Many programs failed to link when compiled with EGCS 1.0 on Red Hat 5.0 or on systems with newer versions of glibc2. EGCS 1.0.1 should fix these problems.

-   Compatibility with both EGCS 1.0 and GCC 2.8 libgcc exception handling interfaces.

    To avoid future compatibility problems, we strongly urge anyone who is planning on distributing shared libraries that contain C++ code to upgrade to EGCS 1.0.1 first.

    Soon after EGCS 1.0 was released, the GCC developers made some incompatible changes in libgcc's exception handling interfaces. These changes were needed to solve problems on some platforms. This means that GCC 2.8.0, when released, will not be seamlessly compatible with shared libraries built by EGCS 1.0. The reason is that the libgcc.a in GCC 2.8.0 will not contain a function needed by the old interface.

    The result of this is that there may be compatibility problems with shared libraries built by EGCS 1.0 when used with GCC 2.8.0.

    With EGCS 1.0.1, generated code uses the new (GCC 2.8.0) interface, and libgcc.a has the support routines for both the old and the new interfaces (so EGCS 1.0.1 and EGCS 1.0 code can be freely mixed, and EGCS 1.0.1 and GCC 2.8.0 code can be freely mixed).

    The maintainers of GCC 2.x have decided against including seamless support for the old interface in 2.8.0, since it was never "official", so to avoid future compatibility problems we recommend against distributing any shared libraries built by EGCS 1.0 that contain C++ code (upgrade to 1.0.1 and use that).

-   Various bugfixes in the x86, hppa, mips, and rs6000/ppc back ends.

    The x86 changes fix code generation errors exposed when building glibc2 and the usual GNU/Linux dynamic linker (ld.so).

    The hppa change fixes a compiler abort when configured for use with RTEMS.

    The MIPS changes fix problems with the definition of LONG\_MAX on newer systems, allow for command line selection of the target ABI, and fix one code generation problem.

    The rs6000/ppc change fixes some problems with passing structures to varargs/stdarg functions.

-   A few machine independent bugfixes, mostly to fix code generation errors when building Linux kernels or glibc.

-   Fix a few critical exception handling and template bugs in the C++ compiler.

-   Fix Fortran namelist bug on alphas.

-   Fix build problems on x86-solaris systems.

EGCS 1.0.2 is a minor update to the EGCS 1.0.1 compiler to fix several serious problems in EGCS 1.0.1.

-   General improvements and fixes
    -   Memory consumption significantly reduced, especially for templates and inline functions.
    -   Fix various problems with glibc2.1.
    -   Fix loop optimization bug exposed by rs6000/ppc port.
    -   Fix to avoid potential code generation problems in jump.c.
    -   Fix some undefined symbol problems in dwarf1 debug support.
-   g++/libstdc++ improvements and fixes
    -   libstdc++ in the EGCS release has been updated and should be link compatible with libstdc++-2.8.
    -   Various fixes in libio/libstdc++ to work better on GNU/Linux systems.
    -   Fix problems with duplicate symbols on systems that do not support weak symbols.
    -   Memory corruption bug and undefined symbols in bastring have been fixed.
    -   Various exception handling fixes.
    -   Fix compiler abort for very long thunk names.
-   g77 improvements and fixes
    -   Fix compiler crash for omitted bound in Fortran CASE statement.
    -   Add missing entries to g77 lang-options.
    -   Fix problem with -fpedantic in the g77 compiler.
    -   Fix "backspace" problem with g77 on alphas.
    -   Fix x86 backend problem with Fortran literals and -fpic.
    -   Fix some of the problems with negative subscripts for g77 on alphas.
    -   Fixes for Fortran builds on cygwin32/mingw32.
-   platform specific improvements and fixes
    -   Fix long double problems on x86 (exposed by glibc).
    -   x86 ports define i386 again to keep imake happy.
    -   Fix exception handling support on NetBSD ports.
    -   Several changes to collect2 to fix many problems with AIX.
    -   Define \_\_ELF\_\_ for GNU/Linux on rs6000.
    -   Fix -mcall-linux problem on GNU/Linux on rs6000.
    -   Fix stdarg/vararg problem for GNU/Linux on rs6000.
    -   Allow autoconf to select a proper install problem on AIX 3.1.
    -   m68k port support includes -mcpu32 option as well as cpu32 multilibs.
    -   Fix stdarg bug for irix6.
    -   Allow EGCS to build on irix5 without the gnu assembler.
    -   Fix problem with static linking on sco5.
    -   Fix bootstrap on sco5 with native compiler.
    -   Fix for abort building newlib on H8 target.
    -   Fix fixincludes handling of math.h on SunOS.
    -   Minor fix for Motorola 3300 m68k systems.

EGCS 1.0.3 is a minor update to the EGCS 1.0.2 compiler to fix a few problems reported by Red Hat for builds of Red Hat 5.1.

-   Generic bugfixes:
    -   Fix a typo in the libio library which resulted in incorrect behavior of istream::get.
    -   Fix the Fortran negative array index problem.
    -   Fix a major problem with the ObjC runtime thread support exposed by glibc2.
    -   Reduce memory consumption of the Haifa scheduler.
-   Target specific bugfixes:
    -   Fix one x86 floating point code generation bug exposed by glibc2 builds.
    -   Fix one x86 internal compiler error exposed by glibc2 builds.
    -   Fix profiling bugs on the Alpha.
    -   Fix ImageMagick & emacs 20.2 build problems on the Alpha.
    -   Fix rs6000/ppc bug when converting values from integer types to floating point types.

The EGCS 1.0 releases include installation instructions in both HTML and plaintext forms (see the INSTALL directory in the toplevel directory of the distribution). However, we also keep the most up to date [installation instructions](http://gcc.gnu.org/install/) and [build/test status](buildstat.html) on our web page. We will update those pages as new information becomes available.

And, we can't in good conscience fail to mention some [caveats](caveats.html) to using EGCS.

Update: Big thanks to Stanford for providing a high speed link for downloading EGCS (go.cygnus.com)!

Download EGCS from ftp.cygnus.com (USA California) or go.cygnus.com (USA California -- High speed link provided by Stanford).

The EGCS 1.0 release is also available many mirror sites.
 [Goto mirror list to find a closer site](../mirrors.html)

We'd like to thank the numerous people that have contributed new features, test results, bugfixes, etc. Unfortunately, they're far too numerous to mention by name.
