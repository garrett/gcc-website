# EGCS 1.0 features

-   Core compiler is based on the gcc2 development tree from Aug 2, 1997, so we have most of the [features found in GCC 2.8](features-2.8.html).
-   Integrated GNU Fortran compiler based on g77-0.5.22-19970929.
-   Vast improvements in the C++ compiler; so many they have [page of their own!](c++features.html)
-   Integrated C++ runtime libraries, including support for most major GNU/Linux systems!
-   New instruction scheduler from IBM Haifa which includes support for function wide instruction scheduling as well as superscalar scheduling.
-   Significantly improved alias analysis code.
-   Improved register allocation for two address machines.
-   Significant code generation improvements for Fortran code on Alphas.
-   Various optimizations from the g77 project as well as improved loop optimizations.
-   Dwarf2 debug format support for some targets.
-   egcs libstdc++ includes the SGI STL implementation without changes.
-   As a result of these and other changes, egcs libstc++ is not binary compatible with previous releases of libstdc++.
-   Various new ports -- UltraSPARC, Irix6.2 & Irix6.3 support, The SCO Openserver 5 family (5.0.{0,2,4} and Internet FastStart 1.0 and 1.1), Support for RTEMS on several embedded targets, Support for arm-linux, Mitsubishi M32R, Hitachi H8/S, Matsushita MN102 and MN103, NEC V850, Sparclet, Solaris & GNU/Linux on PowerPCs, etc.
-   Integrated testsuites for gcc, g++, g77, libstdc++ and libio.
-   RS6000/PowerPC ports generate code which can run on all RS6000/PowerPC variants by default.
-   -mcpu= and -march= switches for the x86 port to allow better control over how the x86 port generates code.
-   Includes the template repository patch (aka repo patch); note the new template code makes repo obsolete for ELF systems using gnu-ld such as GNU/Linux.
-   Plus the usual assortment of bugfixes and improvements.

