# EGCS 1.1 test strategy

-   [No testsuite regressions on major platforms](regress.html). DONE.
-   libg++ addon builds and passes on same platforms as we do testsuite regressions on. DONE.
-   Builds glibc and passes glibc testsuite on major supported platforms (x86, alpha, SPARC). Testing on powerpc and m68k would also be appreciated.
    We are testing a development version of glibc. The sources for testing can be found on the egcs ftp server.
    x86 has successfully passed the glibc test.
    ppc has passed using different versions of the glibc sources
    alpha "passed" (failures were glibc problems, not compiler problems).
    SPARC builds and passes all glibc testsuites.
-   Linux kernel builds and runs (and 2.1.115). Platforms will be x86, alpha. SPARC & PowerPC kernel tests would also be greatly appreciated.
    x86 has passed the kernel test (2.1.115)
    PowerPC has passed using different versions of the kernel
    Alpha has had problems with 2.1.115, 2.1.116. Some success with 2.1.117, but it still locks up. Could be a kernel problem. A different person has reported success with 2.1.115 through 2.1.118.
    SPARC and SPARC64 linux kernels successfully build and work for non-SMP configurations.
-   Red Hat 5.1 builds without errors on x86, Alpha.
    -   x86 failures
        -   There are minor C++ violations in guavac, sgml-tools and linuxconf. These are not EGCS failures.
        -   We've built all the appropriate SRPMS and installed the resulting binaries, then rebuilt the SRPMS again.
    -   Alpha failures I've been told this was about 90% done. Not sure if there are any compiler problems to deal with.
    -   SPARC failures
    -   PowerPC failures
-   RTEMS builds on the following RTEMS targets:
    -   hppa1.1
    -   i386, i386-elf, i386-go32
    -   i960
    -   m68k
    -   mips64orion
    -   powerpc
    -   sh, sh-elf
    -   SPARC

