# EGCS 1.1 branch

We have made a branch in the source tree for EGCS 1.1. Depending on your situation you have three choices for handling your CVS tree.

1.  Do nothing. Your existing CVS tree will continue to track the mainline EGCS sources.
2.  Track just the EGCS 1.1 release. You can convert your existing CVS tree to track the EGCS 1.1 release with the command:
    `cvs update -regcs_1_1_branch`
3.  Track both the EGCS 1.1 release and the development sources. You will need to check out the EGCS 1.1 release in a separate directory using the command:
    `cvs co -regcs_1_1_branch egcs`

Considering that the primary focus of the project is on the egc-1.1 release, option \#2 or \#3 is recommended.

FTP snapshots will track the EGCS 1.1 release only. After EGCS 1.1 is released, snapshots will return to tracking the mainline sources.

* * * * *

All patches for the EGCS 1.1 release branch must be approved by either someone with global write privs or that maintains the specific file being patched. At some point in the near future, all patches will need to go through the release coordinator. A message will be sent to the EGCS list at that time.

We are only accepting bugfix patches for the EGCS 1.1 release.

You can still submit patches for the mainline EGCS sources. However, responses to such patches may be delayed.
