# EGCS 1.1

The EGCS project is rapidly approaching its second major release (EGCS 1.1).

This page is meant to provide information about the current schedule for EGCS 1.1, the [testing/release criteria](egcs-1.1-test.html) for EGCS 1.1, [new features](features.html) to be found in EGCS 1.1, etc.

August 30, 1998. Regression, libg++, glibc, linux kernel, rtems testing all look good. Red Hat 5.1 testing looks good on the x86 and I've been told that Red Hat 5.1 testing on the Alpha looks good too. No word on the SPARC port.

August 14, 1998. The EGCS 1.1 sources are frozen. Only critical patches are being accepted. All checkins to the EGCS 1.1 tree must be approved by the release manager (law@cygnus.com). We are shooting for an end of the month release.

July 14, 1998. We have created the [EGCS 1.1 branch](egcs-1.1-branch.html).

July 7, 1998. The [testing plan](egcs-1.1-test.html) is not ready yet, but we are actively working on solidifying the testing plan.

The tentative schedule for the EGCS 1.1 release is:

-   **July 3, 1998. Feature freeze date**.

    All new features, optimizations, etc need to be *submitted* by this date. It is very likely that not all submissions will be accepted for EGCS 1.1. But all submissions need to be made available for evaluation by July 3, 1998.

    It is also possible some patches which have been submitted and are accepted for EGCS 1.1 will not have been installed by this date due to EOQ commitments for many of the Cygnus volunteers.

    After July 3, 1998, only bugfix submissions will be considered for the EGCS 1.1 release.

    By July 3, 1998, we also want to have a testing plan and release criteria in place.

-   **July 11, 1998. Branch Creation & Testing**.

    the EGCS 1.1 branch will be created on July 11, 1998 and snapshots will occur from the EGCS 1.1 branch instead of the mainline sources.

    By July 11, 1998 it is hoped that all new features, optimizations, etc slated for the EGCS 1.1 release have been installed into the source tree.

    Wide scale, focused testing should start on or before July 11th.

-   **July 31, 1998. Critical Bug Freeze Date**.

    July 31, 1998 is the target date for freezing the sources. After this date only critical bugfixes will be accepted for inclusion in EGCS 1.1. This may be overly optimistic, but it's important to have a goal to shoot for.

The release should go out as soon as possible in early August.

Note that we have the ability to install changes into the mainline sources during this process. However, the main focus will be on the EGCS 1.1 specific issues.

It is highly likely that some minor releases will follow EGCS 1.1 (ie EGCS 1.1.1, EGCS 1.1.2, etc). So if a critical bug does not get fixed for EGCS 1.1, it may be fixed in one of the minor releases.

We plan to do a post-release review of the proposed schedule vs the actual schedule used for EGCS 1.1 so that we can improve the process in the future.
