# EGCS 1.1 new features

-   Integrated GNU Fortran (`g77`) compiler and runtime library with improvements, based on `g77` version 0.5.23.
-   Vast improvements in the C++ compiler; so many they have [page of their own!](c++features.html)
-   Compiler implements [global common subexpression elimination and global copy/constant propagation.](../news/gcse.html)
-   More major improvements in the [alias analysis code](../news/alias.html).
-   More major improvements in the exception handling code to improve performance, lower static overhead and provide the infrastructure for future improvements.
-   The infamous /tmp symlink race security problems have been fixed.
-   The regmove optimization pass has been nearly completely rewritten to improve performance of generated code.
-   The compiler now recomputes register usage information before local register allocation. By providing more accurate information to the priority based allocator, we get better register allocation.
-   The register reloading phase of the compiler optimizes spill code much better than in previous releases.
-   Some bad interactions between the register allocator and instruction scheduler have been fixed, resulting in much better code for certain programs. Additionally, we have tuned the scheduler in various ways to improve performance of generated code for some architectures.
-   The compiler's branch shortening algorithms have been significantly improved to work better on targets which align jump targets.
-   The compiler now supports -Os to prefer optimizing for code space over optimizing for code speed.
-   The compiler will now totally eliminate library calls which compute constant values. This primarily helps targets with no integer div/mul support and targets without floating point support.
-   The compiler now supports an extensive "--help" option.
-   cpplib has been greatly improved and may be suitable for limited use.
-   Memory footprint for the compiler has been significantly reduced for some pathological cases.
-   The time to build EGCS has been improved for certain targets (particularly the alpha and mips platforms).
-   Many infrastructure improvements throughout the compiler, plus the usual mountain of bugfixes and minor improvements.
-   Target dependent improvements:
    -   SPARC port now includes V8 plus and V9 support as well as performance tuning for Ultra class machines. The SPARC port now uses the Haifa scheduler.
    -   Alpha port has been tuned for the EV6 processor and has an optimized expansion of memcpy/bzero. The Alpha port now uses the Haifa scheduler.
    -   RS6000/PowerPC: support for the Power64 architecture and AIX 4.3. The RS6000/PowerPC port now uses the Haifa scheduler.
    -   x86: Alignment of static store data and jump targets is per Intel recommendations now. Various improvements throughout the x86 port to improve performance on Pentium processors (including improved epilogue sequences for Pentium chips and backend improvements which should help register allocation on all x86 variants. Conditional move support has been fixed and enabled for PPro processors. The x86 port also better supports 64bit operations now. Unixware 7, a System V Release 5 target, is now supported and SCO OpenServer targets can support GAS.
    -   MIPS has improved multiply/multiply-add support and now includes mips16 ISA support.
    -   M68k has many micro-optimizations and Coldfire fixes.
-   Core compiler is based on the GCC development tree from June 9, 1998, so we have all of the [features found in GCC 2.8](../egcs-1.0/features-2.8.html).

