# EGCS 1.1 regression test strategy

One of the critical steps in making releases is regression testing. For EGCS 1.1 we want to perform regression tests against GCC 2.8.1 and EGCS 1.0.3a.

Since it is impossible to test every platform, we have selected a list of popular platforms for which EGCS 1.1 should have no regressions relative to EGCS 1.0.3a or GCC 2.8.1.

We encourage testing of platforms not on the official regression test list. However, the "no regressions" policy does not necessarily apply to targets not on the official regression test list.

Contact law@cygnus.com directly if you want to help with the regression testing phase of the EGCS 1.1 release cycle.

## How to run the regression tests

First, you must get out of the EGCS object directory. So change directories into your home directory, /tmp or somewhere else.

Second, you need an appropriate site.exp. Here is a template:

    # snapdir should point to the top-level directory of an unpacked EGCS 1.1
    # snapshot
    set snapdir "/some/place/EGCS 19980715"

    # set old_release_prefix to the prefix directory of an installed EGCS 1.0.3a.
    set old_release_prefix /prefix/for/EGCS 1.0.3a

    set rootme "."
    set target_triplet [exec "$snapdir/config.guess"]
    set srcdir "$snapdir/gcc/testsuite"
    set GCC_UNDER_TEST "$old_release_prefix/bin/gcc"
    set GXX_UNDER_TEST "$old_release_prefix/bin/g++"
    set G77_UNDER_TEST "$old_release_prefix/bin/g77"

Obviously you have to change "snapdir" and "old\_release\_prefix" to appropriate values.

Then run the tests with the following commands:

      runtest --tool gcc
      runtest --tool g++
      runtest --tool g77 (Only necessary if you have installed g77)

This will create "gcc.sum", "g++.sum" and "g77.sum". You should send those files to the testing coordinator law@cygnus.com.

Note that you will receive many failures from GCC 2.8.1 and EGCS 1.0.3a when running the gcc testsuites because those versions of the compiler do not support -Os.

* * * * *

## Current Regression Testing Status

Platform

GCC 2.8.1

EGCS 1.0.3a

EGCS 1.1

regressions

libg++ tests

alpha-dec-osf4

Jeff Law

Kaveh Ghazi

Kaveh Ghazi

NONE

OK

alphaev5-unknown-linux-gnu

Craig Burley

Craig Burley

Alexandre Oliva

NONE

OK

hppa1.1-hp-hpux10.20

Jeff Law

Jeff Law

Jeff Law

NONE

OK

i386-pc-freebsd2.1.6

Kate Hedstrom

Kate Hedstrom

Kate Hedstrom

NONE

i386-pc-freebsd2.2.6

Jeff Law

Jeff Law

Jeff Law

NONE

curses.h botch Otherwise OK

i586-pc-linux-gnu

Jeff Law

Jeff Law

Manfred Hollstein

NONE

OK

i586-pc-linux-gnulibc1

Jeff Law

Jeff Law

Alexandre Oliva

NONE

OK

i686-pc-linux-gnulibc1

Craig Burley

Craig Burley

Jeff Law

NONE

OK

i686-pc-linux-gnu

Jeff Law

Jeff Law

Jeff Law

NONE

OK

i686-pc-sco3.2v5.0.5

Robert Lipe

Robert Lipe

Robert Lipe

NONE

curses.h botch Otherwise OK

m68k-sun-sunos4.1

Kate Hedstrom

Kate Hedstrom

Kate Hedstrom

NONE

mips-sgi-irix5.2

Alexandre Oliva

Alexandre Oliva

Alexandre Oliva

NONE

mips-sgi-irix5.3

Jeff Law

Jeff Law

Jeff Law

NONE

curses.h botch Otherwise OK

mips-sgi-irix6.2

David Billinghurst

David Billinghurst

David Billinghurst

NONE

curses.h botch Otherwise OK

mips-sgi-irix6.{3,4}

Dave Love irix6.4

Dave Love irix6.4

Alexandre Oliva irix6.3

NONE

curses.h botch Otherwise OK

{rs6000,powerpc}-ibm-aix4.2

David Edelsohn

David Edelsohn

Rodney Brown

NONE

curses.h botch Otherwise OK

powerpc-ibm-linux-gnu

Alexandre Oliva

Alexandre Oliva

sparc-sun-solaris2.5

Alexandre Oliva

Joe Buck

Manfred Hollstein

NONE

OK

sparc-sun-solaris2.6

Alexandre Oliva

Alexandre Oliva

Manfred Hollstein

NONE

OK

sparc-sun-sunos4.1

Alexandre Oliva

Alexandre Oliva

Manfred Hollstein

NONE

OK

sparc-sun-linux-gnu

David Miller

David Miller

David Miller

NONE

\*Note Everything is failing gcc.dg/980626-1.c. It's a bogus warning test, so I don't consider it critical, but I have asked for a volunteer to look into the problem.
