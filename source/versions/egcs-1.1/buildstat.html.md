# Build and Test status for EGCS 1.1

**alpha-dec-osf3.2**
 Successful

**alphaev56-dec-osf4.0b**
 Successful

**alphaev56-dec-osf4.0d**
 Successful

**alphaev56-unknown-linux-gnu**
 Successful

**alphaev5-unknown-linux-gnu**
 Successful

**alphapca56-unknown-linux-gnu**
 Successful

**hppa1.1-hp-hpux9.05**
 Successful

**hppa1.1-hp-hpux9.07**
 Successful

**hppa1.1-hp-hpux10.20**
 Successful

**hppa1.1-hp-hpux11.00**
 Successful

**i386-pc-solaris2.6**
 Successful

**i386-pc-solaris2.7**
 Successful

**i386-unknown-freebsd3.1**
 Successful

**i386-unknown-openbsd2.3**
 Had to hack gcc/configure.in. Troubles with texinfo, but a successful build was eventually possible.

**i586-unknown-linux-gnulibc1**
 Successful

**i586-unknown-linux-gnu Debian**
 Successful

**i686-unknown-linux-gnu**
 Successful

**i686-pc-cygwin32**
 Successful

**i686-UnixWare7-sysv5**
 Successful

**i686-pc-sco3.2v5.0.4**
 Successful

**i686-pc-sco3.2v5.0.5**
 Successful

**x86-unknown-linux-gnu Red Hat 5.1**
 Successful

**mips-sgi-irix5.2**
 Successful

**mips-sgi-irix5.3**
 Successful

**mips-sgi-irix6.2**
 Successful

**mips-sgi-irix6.3**
 Successful

**mips-sgi-irix6.4**
 Successful

**mips-sgi-irix6.5**
 Successful

**m68k-next-nextstep3**
 Successful

**powerpc-unknown-linux-gnulibc1**
 Successful

**rs6000-ibm-aix3.2**
 Successful

**rs6000-ibm-aix4.1.4.0**
 Successful

**rs6000-ibm-aix4.1.5.0**
 Successful

**rs6000-ibm-aix4.2.0.0**
 Successful

**rs6000-ibm-aix4.2.1.0**
 Successful

**sparc-unknown-linux-gnu (sun4m)**
 Successful

**sparc-sun-solaris2.3**
 Successful

**sparc-sun-solaris2.5**
 Successful

**sparc-sun-solaris2.5.1**
 Successful

**sparc-sun-solaris2.6**
 Successful

**sparc-sun-solaris2.7**
 Successful

**sparc-sun-sunos4.1.4**
 Successful
