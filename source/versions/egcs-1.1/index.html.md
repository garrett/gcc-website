# EGCS 1.1

September 3, 1998: We are pleased to announce the release of EGCS 1.1.
 December 1, 1998: We are pleased to announce the release of EGCS 1.1.1.
 March 15, 1999: We are pleased to announce the release of EGCS 1.1.2.

EGCS is a free software project to further the development of the GNU compilers using an open development environment.

EGCS 1.1 is a major new release of the EGCS compiler system. It has been [extensively tested](egcs-1.1-test.html) and is believed to be stable and suitable for widespread use.

EGCS 1.1 is based on an June 6, 1998 snapshot of the GCC 2.8 development sources; it contains all of the new features found in GCC 2.8.1 as well as all new development from GCC up to June 6, 1998.

EGCS 1.1 also contains many improvements and features not found in GCC or in older versions of EGCS:

-   Global common subexpression elimination and global constant/copy propagation (aka [gcse](../news/gcse.html))
-   Ongoing improvements to the [alias analysis](../news/alias.html) support to allow for better optimizations throughout the compiler.
-   Vastly improved [C++ compiler](c++features.html) and integrated C++ runtime libraries.
-   Fixes for the /tmp symlink race security problems.
-   New targets including mips16, arm-thumb and 64 bit PowerPC.
-   Improvements to GNU Fortran (`g77`) compiler and runtime library made since `g77` version 0.5.23.

See the [new features](features.html) page for a more complete list of new features found in EGCS 1.1 releases.

EGCS 1.1.1 is a minor update to fix several serious problems in EGCS 1.1:

-   General improvements and fixes
    -   Avoid some stack overflows when compiling large functions.
    -   Avoid incorrect loop invariant code motions.
    -   Fix some core dumps on Linux kernel code.
    -   Bring back the imake -Di386 and friends fix from EGCS 1.0.2.
    -   Fix code generation problem in gcse.
    -   Various documentation related fixes.
-   g++/libstdc++ improvements and fixes
    -   MT safe EH fix for setjmp/longjmp based exception handling.
    -   Fix a few bad interactions between optimization and exception handling.
    -   Fixes for demangling of template names starting with "\_\_".
    -   Fix a bug that would fail to run destructors in some cases with -O2.
    -   Fix 'new' of classes with virtual bases.
    -   Fix crash building Qt on the Alpha.
    -   Fix failure compiling WIFEXITED macro on GNU/Linux.
    -   Fix some -frepo failures.
-   g77 and libf2c improvements and fixes
    -   Various documentation fixes.
    -   Avoid compiler crash on RAND intrinsic.
    -   Fix minor bugs in makefiles exposed by BSD make programs.
    -   Define \_XOPEN\_SOURCE for libI77 build to avoid potential problems on some 64-bit systems.
    -   Fix problem with implicit endfile on rewind.
    -   Fix spurious recursive I/O errors.
-   platform specific improvements and fixes
    -   Match all versions of UnixWare7.
    -   Do not assume x86 SVR4 or UnixWare targets can handle stabs.
    -   Fix PPC/RS6000 LEGITIMIZE\_ADDRESS macro and bug in conversion from unsigned ints to double precision floats.
    -   Fix ARM ABI issue with NetBSD.
    -   Fix a few arm code generation bugs.
    -   Fixincludes will fix additional broken SCO OpenServer header files.
    -   Fix a m68k backend bug which caused invalid offsets in reg+d addresses.
    -   Fix problems with 64bit AIX 4.3 support.
    -   Fix handling of long longs for varargs/stdarg functions on the ppc.
    -   Minor fixes to CPP predefines for Windows.
    -   Fix code generation problems with gpr\<-\>fpr copies for 64bit ppc.
    -   Fix a few coldfire code generation bugs.
    -   Fix some more header file problems on SunOS 4.x.
    -   Fix assert.h handling for RTEMS.
    -   Fix Windows handling of TREE\_SYMBOL\_REFERENCED.
    -   Fix x86 compiler abort in reg-stack pass.
    -   Fix cygwin/windows problem with section attributes.
    -   Fix Alpha code generation problem exposed by SMP Linux kernels.
    -   Fix typo in m68k 32-\>64bit integer conversion.
    -   Make sure target libraries build with -fPIC for PPC & Alpha targets.

EGCS 1.1.2 is a minor update to fix several serious problems in EGCS 1.1.1:

-   General improvements and fixes
    -   Fix bug in loop optimizer which caused the SPARC (and potentially other) ports to segfault.
    -   Fix infinite recursion in alias analysis and combiner code.
    -   Fix bug in regclass preferencing.
    -   Fix incorrect loop reversal which caused incorrect code to be generated for several targets.
    -   Fix return value for builtin memcpy.
    -   Reduce compile time for certain loops which exposed quadratic behavior in the loop optimizer.
    -   Fix bug which caused volatile memory to be written multiple times when only one write was needed/desired.
    -   Fix compiler abort in caller-save.c
    -   Fix combiner bug which caused incorrect code generation for certain division by constant operations.
    -   Fix incorrect code generation due to a bug in range check optimizations.
    -   Fix incorrect code generation due to mis-handling of clobbered values in CSE.
    -   Fix compiler abort/segfault due to incorrect register splitting when unrolling loops.
    -   Fix code generation involving autoincremented addresses with ternary operators.
    -   Work around bug in the scheduler which caused qt to be mis-compiled on some platforms.
    -   Fix code generation problems with -fshort-enums.
    -   Tighten security for temporary files.
    -   Improve compile time for codes which make heavy use of overloaded functions.
    -   Fix multiply defined constructor/destructor symbol problems.
    -   Avoid setting bogus RPATH environment variable during bootstrap.
    -   Avoid GNU-make dependencies in the texinfo subdir.
    -   Install CPP wrapper script in \$(prefix)/bin if --enable-cpp. --enable-cpp=\<dirname\> can be used to specify an additional install directory for the cpp wrapper script.
    -   Fix CSE bug which caused incorrect label-label refs to appear on some platforms.
    -   Avoid linking in EH routines from libgcc if they are not needed.
    -   Avoid obscure bug in aliasing code.
    -   Fix bug in weak symbol handling.
-   Platform-specific improvements and fixes
    -   Fix detection of PPro/PII on Unixware 7.
    -   Fix compiler segfault when building spec99 and other programs for SPARC targets.
    -   Fix code-generation bugs for integer and floating point conditional move instructions on the PPro/PII.
    -   Use fixincludes to fix byteorder problems on i?86-\*-sysv.
    -   Fix build failure for the arc port.
    -   Fix floating point format configuration for i?86-gnu port.
    -   Fix problems with hppa1.0-hp-hpux10.20 configuration when threads are enabled.
    -   Fix coldfire code generation bugs.
    -   Fix "unrecognized insn" problems for Alpha and PPC ports.
    -   Fix h8/300 code generation problem with floating point values in memory.
    -   Fix unrecognized insn problems for the m68k port.
    -   Fix namespace-pollution problem for the x86 port.
    -   Fix problems with old assembler on x86 NeXT systems.
    -   Fix PIC code-generation problems for the SPARC port.
    -   Fix minor bug with LONG\_CALLS in PowerPC SVR4 support.
    -   Fix minor ISO namespace violation in Alpha varargs/stdarg support.
    -   Fix incorrect "braf" instruction usage for the SH port.
    -   Fix minor bug in va-sh which prevented its use with -ansi.
    -   Fix problems recognizing and supporting FreeBSD.
    -   Handle OpenBSD systems correctly.
    -   Minor fixincludes fix for Digital UNIX 4.0B.
    -   Fix problems with ctors/dtors in SCO shared libraries.
    -   Abort instead of generating incorrect code for PPro/PII floating point conditional moves.
    -   Avoid multiply defined symbols on GNU/Linux systems using libc-5.4.xx.
    -   Fix abort in alpha compiler.
-   Fortran-specific fixes
    -   Fix the IDate intrinsic (VXT) (in `libg2c`) so the returned year is in the documented, non-Y2K-compliant range of 0-99, instead of being returned as 100 in the year 2000.
    -   Fix the \`Date\_and\_Time' intrinsic (in `libg2c`) to return the milliseconds value properly in Values(8).
    -   Fix the \`LStat' intrinsic (in `libg2c`) to return device-ID information properly in SArray(7).

Each release includes installation instructions in both HTML and plaintext forms (see the INSTALL directory in the toplevel directory of the distribution). However, we also keep the most up to date [installation instructions](http://gcc.gnu.org/install/) and [build/test status](buildstat.html) on our web page. We will update those pages as new information becomes available.

The EGCS project would like to thank the numerous people that have contributed new features, test results, bugfixes, etc. This [amazing group of volunteers](http://gcc.gnu.org/onlinedocs/gcc/Contributors.html) is what makes EGCS successful.

And finally, we can't in good conscience fail to mention some [caveats](caveats.html) to using EGCS 1.1.

Download EGCS from egcs.cygnus.com (USA California).

The EGCS 1.1 release is also available on many mirror sites.
 [Goto mirror list to find a closer site](../mirrors.html).
