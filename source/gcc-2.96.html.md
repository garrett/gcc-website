# GCC 2.96

October 6th, 2000

It has come to our attention that some GNU/Linux distributions are currently shipping with \`\`GCC 2.96''.

We would like to point out that GCC 2.96 is not a formal GCC release nor will there ever be such a release. Rather, GCC 2.96 has been the code- name for our development branch that will eventually become GCC 3.0.

Current snapshots of GCC, and any version labeled 2.96, produce object files that are not compatible with those produced by either GCC 2.95.2 or the forthcoming GCC 3.0. Therefore, programs built with these snapshots will not be compatible with any official GCC release. Actually, C and Fortran code will probably be compatible, but code in other languages, most notably C++ due to incompatibilities in symbol encoding (\`\`mangling''), the standard library and the application binary interface (ABI), is likely to fail in some way. Static linking against C++ libraries may make a binary more portable, at the cost of increasing file size and memory use.

To avoid any confusion, we have bumped the version of our current development branch to GCC 2.97.

Please note that both GCC 2.96 and 2.97 are development versions; we do not recommend using them for production purposes. Binaries built using any version of GCC 2.96 or 2.97 will not be portable to systems based on one of our regular releases.

If you encounter a bug in a compiler labeled 2.96, we suggest you contact whoever supplied the compiler as we can not support 2.96 versions that were not issued by the GCC team.

Please see [http://gcc.gnu.org/snapshots.html](snapshots.html) if you want to use our latest snapshots. We suggest you use 2.95.2 if you are uncertain.

[The GCC Steering Committee](steering.html)
