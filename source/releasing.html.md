# Rolling a GCC Release

## Preparations

1.  Before rolling the release, update the release notes web pages (for example `gcc-4.5.0/index.html`) and ensure that they are all listed in `contrib/gennews`. On the announcement page for that release series, note the new release without removing information about any previous minor releases. The `changes.html` page for that release series should have details added of the changes in each minor release.
2.  Verify that displayed copyright years include the current year. Check at least `java/jcf-dump.c`, `gcc.c`, `gcov.c`, `fortran/gfortranspec.c` and `gcov-dump.c` (grep for `"Copyright`). Also check documentation. See SVN revision 194838 for an incomplete example.
3.  Verify [PR other/43620](http://gcc.gnu.org/PR43620) isn't a problem on the branch, otherwise ftp upload to ftp.gnu.org might fail. `find . -name Makefile.in | xargs grep a+w` or `find . -name Makefile.in | xargs grep distdir:` might do the job.
4.  Warn developers to abstain from checking in any code on the release branch.
5.  [Check for new message translations](translation.html#collect) and commit them to the release branch (and to mainline if it is the most recent release branch). [Regenerate `gcc.pot`](translation.html#regen) and `cpplib.pot`.

## Generation and Upload

1.  Roll the release using the `maintainer-scripts/gcc_release` script from the same branch as the release. You must pass the `-f` option, to indicate a final release, the `-r` option (for example, `-r 4.2.3`), to give the release version, and, if diffs against a previous release are to be generated, the `-p` option, whose argument must name the `.tar.bz2` file for a previous release, in a directory containing all the `.tar.bz2` files for that previous release (for example, `-p /some/where/gcc-4.2.2/gcc-4.2.2.tar.bz2`, where there are also files such as `/some/where/gcc-4.2.2/gcc-core-4.2.2.tar.bz2`).
2.  Upload the release to ftp.gnu.org.
3.  Increment the version number in `gcc/BASE-VER`. Restore the word "prerelease" (without the quotation marks) to `gcc/DEV-PHASE`. Check these files in.
4.  Notify developers that checkins to the branch are once again allowed.
5.  Reenable the generation of snapshots off the respective release branch in the crontab of gccadmin on gcc.gnu.org.

## Web Site Updates

1.  Update the `develop.html` and `releases.html` web pages.
2.  Update the version numbers of the current and future releases on the main web page, and add a proper news item there as well.
3.  For a new major release, ensure that the build status page is present and add a link from the main `buildstat.html` page.
4.  Generate online documentation for the new release with `update_web_docs_svn`. The appropriate command to run (as gccadmin) to generate the documentation would be `scripts/update_web_docs_svn -rgcc_3_0_2_release -dgcc-3.0.2` (with the current version number inserted). Link to it from `onlinedocs/index.html` (but don't break URLs to documentation for previous releases even if you remove the links to it). Create additionally `onlinedocs/version-number/index.html` by copying it from a previous release and adjust it.
5.  Generate online libstdc++ documentation with the `maintainer-scripts/generate_libstdcxx_web_docs` script on trunk. That currently can't be done on sourceware.org due to dependencies on a number of DocBook and LaTeX packages. The script takes two arguments, the root of the GCC sources (for the new release) and the output directory for the generated files. All the output that gets created should be placed in the same `onlinedocs/version-number/` directory as the other documentation for the release.
6.  Update the online-documentation links in `changes.html` to point to the online-documentation for the branch.
7.  Update the main web page (`index.html`).

## Announcements

1.  Announce the release to the gcc-announce@gcc.gnu.org, gcc@gcc.gnu.org and info-gnu@gnu.org mailing lists.
    -   Make sure that most mirrors have retrieved the new release already.
    -   In the case of a major release, coordinate announcements with the FSF.

## Bugzilla Updates

1.  Create a new version in Bugzilla corresponding to the new branch version. This can be accomplished by choosing products, choosing gcc, and editing the versions. Please do **not** delete old versions.
2.  Create a new target milestone in Bugzilla corresponding to the dot release after the immediate next (for example create a milestone for 3.3.3 after releasing 3.3.1, so we can slip the milestone for PRs that can't be fixed in time for 3.3.2). This can be accomplished by choosing products, choosing gcc, and editing the target milestones. Please do **not** delete old target milestones.
3.  Change all open bugs targeted at the just released milestone to be targeted at either the mainline version milestone, or the next point-release milestone. This can be accomplished by querying for all bugs targeted at the old milestone, choosing "Change several bugs at once" at the bottom of the buglist screen, choosing "check all" to select all bugs to change, choosing the new target milestone from the drop down, entering a useful comment, and clicking commit.
4.  Update the bug searches on the main web page for the milestone changes.

